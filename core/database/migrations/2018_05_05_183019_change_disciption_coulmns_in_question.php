<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeDisciptionCoulmnsInQuestion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('questions', function (Blueprint $table) {
            $table->text('si_description');
            $table->text('en_description');
            $table->text('ta_description');
            $table->dropColumn('description');
            $table->dropColumn('title');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('questions', function (Blueprint $table) {
          $table->dropColumn('si_description');
          $table->dropColumn('en_description');
          $table->dropColumn('ta_description');
          $table->text('description');
          $table->text('title');
        });
    }
}
