<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeExpireDate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('questions', function (Blueprint $table) {
            $table->date('expire_date');
        });

        Schema::table('events', function (Blueprint $table) {
            $table->dropColumn('expire_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('questions', function (Blueprint $table) {
          $table->dropColumn('expire_date');
        });

        Schema::table('events', function (Blueprint $table) {
          $table->date('expire_date');
        });
    }
}
