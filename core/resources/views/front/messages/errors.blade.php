@if ($errors->any())
<div class="container text-center" style="padding-top: 5px;">
    <div class="white-text">
        @foreach ($errors->all() as $error)
        {!! $error !!}<br/>
        @endforeach
    </div>
</div>
@elseif (session()->get('flash_success'))
<div class="container text-center" style="padding-top: 5px;">
    <div class="white-text">
        @if(is_array(json_decode(session()->get('flash_success'), true)))
        {!! implode('', session()->get('flash_success')->all(':message<br/>')) !!}
        @else
        {!! session()->get('flash_success') !!}
        @endif
    </div>
</div>
@endif