<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
    <style>
    a{
        text-decoration: none;
    }
    .btn{
        color: #fff!important;
        background-color: #d64646;
        -webkit-box-shadow: 0px 0px 5px 2px rgba(145,39,39,1);
        -moz-box-shadow: 0px 0px 5px 2px rgba(145,39,39,1);
        box-shadow: 0px 0px 5px 2px rgba(145,39,39,1);
        /*border: none;*/
        border-radius: 2px;
        display: inline-block;
        height: 36px;
        line-height: 36px;
        padding: 0 16px;
        text-transform: uppercase;
        vertical-align: middle;
        width: 160px;
        float: left;
        margin-top: 0px;
    }


</style>
</head>
<body>
    <h2>{{ trans('auth.email_verify') }}</h2>

    <div>
        <h5>{{ trans('auth.email_verify_detail') }}</h5>
        
        <a href="{{ $url}}" class="btn">{{ trans('auth.confirm_email') }}</a><br><br>

    </div>

    <h6>All Rights Reserved, <br> People Power</h6>
</body>
</html>