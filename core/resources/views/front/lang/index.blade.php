@extends('front.layouts.app')

@section('content')

  <div class="container">
     <div class="row">
      <div class="section">
      	  <div class="col s12">
          	<a href="{{ route('frontend.language.set', 'english') }}"  class="waves-effect white btn btn1">ENGLISH</a>
          </div>
          <div class="col s12">
          	<a href="{{ route('frontend.language.set', 'sinhala') }}" class="waves-effect white btn btn1">&#x0DC3;&#x0DD2;&#x0D82;&#x0DC4;&#x0DBD;</a>
          </div>
          <div class="col s12">
          	<a href="{{ route('frontend.language.set', 'tamil') }}"  class="waves-effect white btn btn1">&#x0BA4;&#x0BAE;&#x0BBF;&#x0BB4;&#x0BCD;</a>
          </div>
      </div>
     </div>
  </div>
@endsection