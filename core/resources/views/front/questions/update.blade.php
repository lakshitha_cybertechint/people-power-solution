@extends('front.layouts.app')

@section('content')

<style type="text/css">
body {
    background-color: #eeeeee!important;
}
.container{
    text-align: left;
}
#date {
    padding-left: 20px;
    font-size: 13px;
    font-weight: 600;
}
.test_div{
    height: 50px;
    width: 50px;
    background: #204d75 !important;
    top: 10px;
    position: relative;
    display: block;
}
.toggle-container{
    position: relative;
    top:0px;
}
.range-field{
    display: inline-block;
    width:200px;
    margin: 0px;
    border-radius: 2px;
}
input[type=range] {
    -webkit-appearance: none;
    margin: 0;
    width: 100%;
    padding: 0px;
    outline: none;
    border: none;
}
.toggle-false-msg{
    display: none;
    opacity: .2;
    transition: .5s opacity;
    display: inline-block;
    position: relative;
    top: 5px;
}
.toggle-true-msg{
    display: none;
    opacity: .2;
    transition: .5s opacity;
    display: inline-block;
    position: relative;
    top:5px;
}
.toggle-neutral-msg{
    display: none;
    opacity: .2;
    transition: .5s opacity;
    position: relative;
    top: -8px;
}
#rangeActive {
    background-color: blue;
}
#textActivefalse{
    opacity: 1;
    color:red;
    animation: shake 1.0s;
}
#textActive{
    opacity: 1;
    color:black;
}
#textActivetrue{
    opacity: 1;
    color:green;
    animation: shake 1.0s;
}
.mystyle {
    opacity: 1;
    animation: shake 1.0s;
}
.grascale{
    -webkit-filter: grayscale(100%); /* Safari 6.0 - 9.0 */
    -ms-filter: grayscale(100%);
    -moz-filter: grayscale(100%);
    filter: grayscale(100%);
    opacity: 0.5;
    fill-opacity: 0.5;
}

@keyframes shake {
    0% { transform: translate(1px, 1px) rotate(0deg); }
    10% { transform: translate(-1px, -2px) rotate(-2deg); }
    20% { transform: translate(-3px, 0px) rotate(2deg); }
    30% { transform: translate(3px, 2px) rotate(0deg); }
    40% { transform: translate(1px, -1px) rotate(2deg); }
    50% { transform: translate(-1px, 2px) rotate(-2deg); }
    60% { transform: translate(-3px, 1px) rotate(0deg); }
    70% { transform: translate(3px, 1px) rotate(-2deg); }
    80% { transform: translate(-1px, -1px) rotate(2deg); }
    90% { transform: translate(1px, 2px) rotate(0deg); }
    100% { transform: translate(1px, -2px) rotate(-2deg); }
}
#share {
    top:5px;
    background-color:#5D7DAE;
    height:24px;
    width: 80px;
}
#share1 {
    top:5px;
    background-color:#5D7DAE;
    height:24px;
    width: 80px;
    left: 10px;
    color: white;
}
#share a.click{
    font-size:13px;
    font-weight:bold;
    text-align:center;
    color:#fff;
    border:1px solid #FFF;
    background-color:#5D7DAE;
    padding: 2px 10px;
    cursor: pointer;
    text-decoration:none;
    width:80px;
    display:block;
}
input[type=range]:focus {
    outline: none;
}
input[type=range]::-webkit-slider-runnable-track {
    width: 100%;
    height: 10px;
    cursor: pointer;
    animate: 0.2s;
    box-shadow: 0px 0px 0px #000000;
    /*background: #8BBF54;*/
    border-radius: 10px;
    border: 0px solid #000000;
}
input[type=range]::-webkit-slider-thumb {
    -webkit-appearance: none;
    border: 1px solid #fff;
    height: 30px;
    width: 30px;
    border-radius: 50px;
    background: #F11820;
    cursor: pointer;
    box-shadow: 0px 0px 2px #000000;
    margin-top: -10px; /* You need to specify a margin in Chrome, but in Firefox and IE it is automatic */
    box-shadow: 1px 1px 1px #000000, 0px 0px 1px #0d0d0d; /* Add cool effects to your sliders! */
    background-image: url('data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4gPHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PGRlZnM+PGxpbmVhckdyYWRpZW50IGlkPSJncmFkIiBncmFkaWVudFVuaXRzPSJvYmplY3RCb3VuZGluZ0JveCIgeDE9IjAuNSIgeTE9IjAuMCIgeDI9IjAuNSIgeTI9IjEuMCI+PHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwLjEzIi8+PHN0b3Agb2Zmc2V0PSIxMDAlIiBzdG9wLWNvbG9yPSIjZmZmZmZmIiBzdG9wLW9wYWNpdHk9IjAuMCIvPjwvbGluZWFyR3JhZGllbnQ+PC9kZWZzPjxyZWN0IHg9IjAiIHk9IjAiIHdpZHRoPSIxMDAlIiBoZWlnaHQ9IjEwMCUiIGZpbGw9InVybCgjZ3JhZCkiIC8+PC9zdmc+IA==');
    background-size: 100%;
    background-image: -webkit-gradient(linear, 50% 0%, 50% 100%, color-stop(0%, rgba(0, 0, 0, 0.13)), color-stop(100%, rgba(255, 255, 255, 0)));
    background-image: -moz-linear-gradient(rgba(0, 0, 0, 0.13), rgba(255, 255, 255, 0));
    background-image: -webkit-linear-gradient(rgba(0, 0, 0, 0.13), rgba(255, 255, 255, 0));
    background-image: linear-gradient(rgba(0, 0, 0, 0.13), rgba(255, 255, 255, 0));
    -moz-border-radius: 50%;
    -webkit-border-radius: 50%;
    border-radius: 50%;
}
            /*input[type=range]:focus::-webkit-slider-runnable-track {
            background: #3071A9;
            }*/
            input[type=range]::-moz-range-track {
                width: 100%;
                height: 10px;
                cursor: pointer;
                animate: 0.2s;
                box-shadow: 0px 0px 0px #000000;
                background-color: #B3B5B3;
                border-radius: 15px;
                border: 0px solid #000000;
                /*background-image: url('{{asset('assets/pps-assets/images/06.png')}}');*/
            }
            input[type=range]{
                width: 111%;
                height: 10px;
                cursor: pointer;
                animate: 0.2s;
                box-shadow: 0px 0px 0px #000000;
                background-color: #1CFF00;
                border-radius: 15px;
                border: 0px solid #000000;
                background-image: url('{{asset('assets/pps-assets/images/06.png')}}');
            }
            
            input[type=range]::-moz-range-thumb {
                box-shadow: 0px 0px 2px #000000;
                border: 1px solid #fff;
                height: 30px;
                width: 30px;
                border-radius: 50px;
                background-color: #F11820;
                cursor: pointer;
            }
            input[type=range]::-ms-track {
                width: 100%;
                height: 30px;
                cursor: pointer;
                animate: 0.2s;
                background: transparent;
                border-color: transparent;
                color: transparent;
            }
            input[type=range]::-ms-fill-lower {
                background: #3071A9;
                border: 0px solid #000000;
                border-radius: 0px;
                box-shadow: 0px 0px 0px #000000;
            }
            input[type=range]::-ms-fill-upper {
                background: #3071A9;
                border: 0px solid #000000;
                border-radius: 0px;
                box-shadow: 0px 0px 0px #000000;
            }
            input[type=range]::-ms-thumb {
                box-shadow: 0px 0px 0px #000000;
                border: 0px solid #000000;
                height: 30px;
                width: 19px;
                border-radius: 20px;
                background: #FFFFFF;
                cursor: pointer;
            }
            input[type=range]:focus::-ms-fill-lower {
                background: #3071A9;
            }
            input[type=range]:focus::-ms-fill-upper {
                background: #3071A9;
            }
            input[type="range"] + .thumb.active{
                display: none!important;
                visibility: hidden!important;
            }
            .rangeFalse::-webkit-slider-runnable-track {
                background: #5d0a0a !important;
            }
            .rangeFalse::-webkit-slider-thumb {
                background: white !important;
            }
            .rangeNeutral::-webkit-slider-runnable-track {
                background: #204d75 !important;
            }
            .rangeNeutral::-webkit-slider-thumb {
                background: white !important;
            }
            .rangeTrue::-webkit-slider-runnable-track {
                background: #0e4e1f !important;
            }
            .rangeTrue::-webkit-slider-thumb {
                background: white !important;
            }
            .ibox {
                clear: both;
                margin-bottom: 25px;
                margin-top: 0;
                padding: 0;
                box-shadow: 2px 5px 5px #888888;
                margin-left: 15%;
                margin-right: 15%;
            }
            .ibox-title {
                -moz-border-bottom-colors: none;
                -moz-border-left-colors: none;
                -moz-border-right-colors: none;
                -moz-border-top-colors: none;
                background-color: #ffffff;
                border-color: #e7eaec;
                border-image: none;
                border-style: solid solid none;
                border-width: 2px 0 0;
                color: inherit;
                margin-bottom: 0;
                padding: 7px 5px 7px 5px;
                font-weight: 900;
                min-height: 40px;
                font-size: 1.1em;
            }
            .ibox-content {
                background-color: #ffffff;
                color: inherit;
                padding: 15px 10px 5px 10px;
                border-color: #e7eaec;
                border-image: none;
                border-style: solid solid none;
                border-width: 1px 0;
            }
            .ibox-footer {
                color: inherit;
                border-top: 1px solid #e7eaec;
                font-size: 90%;
                background: #ffffff;
                padding: 10px 15px;
            }
            .gray-bg{
                background-color: #f3f3f4;
            }
            .p-lg {
                padding: 10px;

            }
            .progress {
                margin-top: 50px;
                height: 15px!important;
                background-color: #23E70A;
                background-image: url('{{asset('assets/pps-assets/images/06.png')}}');
                opacity: 0.75;
                fill-opacity: 0.75;
            }
            .progress .determinate{
                background-image: url('{{asset('assets/pps-assets/images/06.png')}}');
                background-color: #f00;
                height: 15px!important;
            }
            .emoji{
             /* max-width: 100%;*/
         }
         .facebook{
            color: #3B5998;
        }
    </style>
    <style type="text/css">
    @media screen and (max-width: 320px) {
        .ibox {
            clear: both;
            margin-bottom: 5px;
            margin-top: 0;
            padding: 0;
            box-shadow: 2px 5px 5px #888888;
            margin-left: 5px;
            margin-right: 5px;
        }
        .range-field{
            display: inline-block;
            width:102%!important;
            margin: 0px;
            border-radius: 2px;
        }
        input[type="range"] {
            width: 80%;
            height: 10px;
        }
        .row .col.s10{
            margin-top: 0px!important; 
            text-align: center;
        }
        input[type="range"]::-moz-range-track{
           height: 10px;
       }
       .emoji{
        /*max-width: 30px;*/
    }
    .progress{
        margin-top: 20px;
    }
    .row{
        margin-bottom: 0px;
    }
    .container{
        width: 100%;
        margin-top: 5px;
    }
    .btnYes, .btnNo{
        font-size: 11px;
        width: 30px;
        height: 30px;
        margin-top: -5px;
    }

    .sharing-text-a {
        font-size: 12px;
    }

}

@media screen and (min-width: 321px) and (max-width: 360px) {
    .ibox {
        clear: both;
        margin-bottom: 5px;
        margin-top: 0;
        padding: 0;
        box-shadow: 2px 5px 5px #888888;
        margin-left: 5px;
        margin-right: 5px;
    }
    .range-field{
        display: inline-block;
        width:102%!important;
        margin: 0px;
        border-radius: 2px;
    }
    input[type="range"]{
        width: 80%;
        height: 10px;
    }
    .row .col.s10{
        margin-top: 0px!important;
        text-align: center; 
    }
    input[type="range"]::-moz-range-track{
       height: 10px;
   }
   .emoji{
    /*max-width: 30px;*/
}
.progress{
    margin-top: 20px;
}
.row{
    margin-bottom: 0px;
}
.container{
    width: 100%;
    margin-top: 5px;
}
.btnYes, .btnNo{
    font-size: 11px;
    width: 30px;
    height: 30px;
    margin-top: -5px;
}
}

@media screen and (min-width: 361px) and (max-width: 768px) {
    .ibox {
        clear: both;
        margin-bottom: 5px;
        margin-top: 0;
        padding: 0;
        box-shadow: 2px 5px 5px #888888;
        margin-left: 5px;
        margin-right: 5px;
    }
    .range-field{
        display: inline-block;
        width:125% !important;
        margin: 0px;
        border-radius: 2px;
    }
    input[type="range"]{
        width: 80%;
        height: 10px;
    }
    .row .col.s10{
        margin-top: 0px!important;
    }
    input[type="range"]::-moz-range-track{
       height: 10px;
   }
   .emoji{
     /* max-width: 30px;*/
 }
 .progress{
    margin-top: 20px;
}
.row{
    margin-bottom: 0px;
}
.container{
    width: 100%;
    margin-top: 10px;
}
.btnYes, .btnNo{
    font-size: 11px;
    width: 30px;
    height: 30px;
    margin-top: -5px;
}

}

@media screen and (min-width: 769px) and (max-width: 800px) {
    .ibox {
        clear: both;
        margin-bottom: 15px;
        margin-top: 0;
        padding: 0;
        box-shadow: 2px 5px 5px #888888;
        margin-left: 10px;
        margin-right: 10px;
    }
    .range-field{
        display: inline-block;
        width:125% !important;
        margin: 0px;
        border-radius: 2px;
    }
    input[type="range"]{
        width: 80%;
        height: 10px;
    }
    .row .col.s10{
        margin-top: 0px!important;
    }
    input[type="range"]::-moz-range-track{
       height: 10px;
   }
   .emoji{
    /*max-width: 30px;*/
}
.progress{
    margin-top: 20px;
}
.row{
    margin-bottom: 0px;
}
.container{
    width: 100%;
    margin-top: 10px;
}
.btnYes, .btnNo{
    font-size: 12px;
    width: 35px;
    height: 35px;
    margin-top: -5px;
}
}

@media screen and (min-width: 801px) and (max-width: 980px) {
    .ibox {
        clear: both;
        margin-bottom: 10px;
        margin-top: 0;
        padding: 0;
        box-shadow: 2px 5px 5px #888888;
        margin-left: 20px;
        margin-right: 20px;
    }
    .range-field{
        display: inline-block;
        width:102%!important;
        margin: 0px;
        border-radius: 2px;
    }
    input[type="range"]{
        width: 80%;
        height: 10px;
    }
    .row .col.s10{
        margin-top: 0px!important;
        text-align: center; 
    }
    input[type="range"]::-moz-range-track{
       height: 10px;
   }
   .emoji{
    /*max-width: 30px;*/
}
.progress{
    margin-top: 20px;
}
.row{
    margin-bottom: 0px;
}
.container{
    width: 100%;
    margin-top: 10px;
}
.btnYes, .btnNo{
    border-radius: 100%;
    font-size: 15px;
    width: 40px;
    height: 40px;
    margin-top: -5px;
    padding: 5px;
}
}

@media screen and (min-width: 981px) {
    .ibox {
        clear: both;
        margin-bottom: 15px;
        margin-top: 0;
        padding: 0;
        box-shadow: 2px 5px 5px #888888;
        margin-left: 2%;
        margin-right: 2%;
    }
    .range-field{
        display: inline-block;
        width:90%!important;
        margin: 0px;
        border-radius: 2px;
    }
    .row {
        margin-bottom: 0px;
    }
    .container{
        margin-top: 15px;
    }
}
</style>

<div class="spinner-wrapper">
    <div class="spinner"></div>
</div>

<div class="container questions">

    @if($questions['count'] == [])

    <div class="text-center">
        <p>{{ trans('questions.no_votes') }}</p>
    </div>

    @else

    @foreach($questions['count'] as $key => $question)
    <!-- date -->
    @if(isset($questions['publish_date'][$key]))
    <div id="date">{{ $questions['publish_date'][$key] }}</div>
    @endif
    
    {{-- {{dd($question)}} --}}
    <!-- question start -->
    <div class="row">
        <div class="col-sm-4"></div>
        <div class="col-sm-4 ">
            <div class="ibox text-center">
                <div class="ibox-title">
                    <div class="questiontext" id="{{ $questions['data'][$key]['id'] }}">{{ $questions['data'][$key]['question'] }}</div>
                </div>
                <div class="ibox-content">
                    <div class="toggle-container row s12">
                        <div class="col s1" style="padding: 0; text-align: center;vertical-align: middle;">
                                <!-- <div id="aa15_bmsg" style="display: none;">
                                    <button id="sadface"><i class="fa fa-frown-o  fa-2x" aria-hidden="true"></i></button>
                                </div> -->
                                <div id="aa1{{ $questions['data'][$key]['id'] }}_bmsg">
                                    <button id="no{{ $questions['data'][$key]['id'] }}" class="btnNo emoji" onclick="btnNo(this);" value="-1" data-value="{{ $questions['data'][$key]['id'] }}">NO<!-- {{ trans('questions.no') }} -->
                                        <!-- <img class="emoji" src="{{asset('assets/pps-assets/images/sad.png')}}"/> -->
                                    </button>
                                </div>
                            </div>
                            <div class="col s10" style="padding: 0; margin-top: 10px;">
                                <div class="range-field" style="width: 100%;">
                                    <input type="range" id="aa1{{ $questions['data'][$key]['id'] }}" name="points" onchange="togglebutton(this);" min="-1" class="" max="1" value="0" data-value="{{ $questions['data'][$key]['id'] }}">
                                </div>
                            </div>
                            <div class="col s1" style="padding: 0; text-align: center;vertical-align: middle;">
                                <!-- <div id="aa15_hmsg" style="display: none;">Neutral</div> -->
                                <!-- <div id="aa15_hmsg1" style="display: none;">
                                    <button id="happyface"><i class="fa fa-smile-o fa-2x" aria-hidden="true"></i></button>
                                </div> -->
                                <div id="aa1{{ $questions['data'][$key]['id'] }}_hmsg1">
                                    <button id="yes{{ $questions['data'][$key]['id'] }}" class="btnYes emoji" onclick="btnYes(this);" value="1" data-value="{{ $questions['data'][$key]['id'] }}">YES<!-- {{ trans('questions.yes') }} -->
                                        <!-- <img class="emoji" src="{{asset('assets/pps-assets/images/happy.png')}}"/> -->
                                    </button>
                                </div>
                            </div>
                            <div class="p-lg"></div>
                            @if($questions['data'][$key]['percent'] !== null)
                            <div class="progress">
                                <div class="determinate" style="width: {{ $questions['data'][$key]['percent'] }}%"></div>
                            </div>
                            @else
                            <br>
                            <p>{{ trans('questions.no_votes_casted') }}</p>
                            @endif
                            <div class="sharing-text-a">
                                <a class="click facebook" href="https://www.facebook.com/sharer/sharer.php?u={{ $questions['data'][$key]['url'] }}&display=popup" target="_blank">
                                    <span style="float: left;"><i class="fa fa-facebook-official"></i> {{ trans('questions.fb_share') }}</span></a>  
                                    
                                    @if($questions['data'][$key]['percent'] !== null)
                                    <a class="click" href="https://twitter.com/intent/tweet?text={{ $questions['data'][$key]['question'] }}, {{ $meta_percent['yes_percent'] }}% peoples says Yes, {{ $meta_percent['no_percent'] }}% peoples says No&url={{ $questions['data'][$key]['url'] }}" target="_blank"><span style="float: right;"><i class="fa fa-twitter"></i> {{ trans('questions.tw_share') }}</span></a>
                                    @else
                                    <a class="click" href="https://twitter.com/intent/tweet?text={{ $questions['data'][$key]['question'] }}, {{ trans('questions.no_votes_casted') }}&url={{ $questions['data'][$key]['url'] }}" target="_blank"><span style="float: right;"><i class="fa fa-twitter"></i> {{ trans('questions.tw_share') }}</span></a>
                                    @endif
                                </div>
                                <br>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4"></div>
            </div>
            <!-- question end -->
            @endforeach
            @endif

        </div>

        <script type="text/javascript">
            var exportUrl = 'http://export.highcharts.com/';
            
            function togglebutton(range) {

                var val = range.value;
                var thidID= range.id;
                
                $("#"+thidID).prop('disabled', true);
                var val1 = range.value;
                var thidID1 = range.id;
                var returnIDVal = document.getElementById(thidID1).value;
                var q_id = $("#"+thidID).attr('data-value');

                /*Disable buttons*/
                $("#no"+q_id).prop('disabled', true);
                $("#yes"+q_id).prop('disabled', true);
                document.getElementById("no"+q_id).classList.add("nohover");
                document.getElementById("yes"+q_id).classList.add("nohover");

                if(returnIDVal == -1){  

                   document.getElementById(thidID1+"_bmsg").classList.add("mystyle");
                   document.getElementById(thidID1+"_hmsg1").classList.add("grascale");
               }

               if(returnIDVal == 0){

               }

               if(returnIDVal == 1){

                document.getElementById(thidID1+"_hmsg1").classList.add("mystyle");
                document.getElementById(thidID1+"_bmsg").classList.add("grascale");
            }



            $.ajax({
                url: '{{ route('frontend.question.answer.update') }}',
                type: 'POST',
                dataType: 'json', 
                data: {'_token': '{{ csrf_token() }}', 'value': returnIDVal, 'q_id': q_id},
                success: function(data){
                    // alert(data);
                    console.log(data);
                    if(data['create_graph'] == false){
                        var optionsStr = JSON.stringify({
                            "chart": {
                                "plotBackgroundColor": null,
                                "plotBorderWidth": null,
                                "plotShadow": false,
                                "type": 'pie',
                                "height": "500px",
                                "backgroundColor": 'transparent',
                                "useHTML": "enabled",
                                "events": {
                                    load: function() {
                                        this.renderer.text("<i style='font-size:24px; color:#000;' class='fa'>&#xf2b9;</i>", 220, 250, true).add();
                                    }
                                },
                            },
                            "title": {
                                "style": {
                                    "fontSize": '30px'
                                },
                                "text": ''
                            },
                            "credits": {
                                "enabled": false
                            },
                            "tooltip": {
                                "pointFormat": '{series.name}: <b>{point.percentage:.1f}%</b>'
                            },
                            "plotOptions": {
                                "pie": {
                                    "allowPointSelect": true,
                                    "cursor": 'pointer',
                                    "colors": ['green', 'red'],
                                    "dataLabels": {
                                        "enabled": true,
                                        "format": '<b>{point.name}</b>: {point.percentage:.0f} %',
                                        "style": {
                                            "fontSize": '22px',
                                            "color": 'black'
                                        }
                                    }
                                }
                            },
                            "series": [{
                                "name": 'Votes',
                                "colorByPoint": true,
                                "data": [{
                                    "name": 'Yes',
                                    "y": data['yes'],
                                    "sliced": true
                                }, {
                                    "name": 'No',
                                    "y": data['no']
                                }]
                            }]
                        }),
                        dataString = encodeURI('async=true&type=png&width=360px&options=' + optionsStr);

                        $.ajax({
                            type: 'POST',
                            data: dataString,
                            url: 'http://export.highcharts.com/',
                            success: function (imageData) {
                                console.log('get the file from relative url: ', imageData);
                                $.ajax({
                                    url: '{{ route('frontend.question.chart.image') }}',
                                    type: 'POST',
                                    dataType: 'json', 
                                    data: {'_token': '{{ csrf_token() }}', 'value': returnIDVal, 'image': exportUrl + imageData, 'q_id': q_id, db_image: data['old_question']['image']},
                                    success: function (saveData) {
                                        // console.log(saveData);
                                    },
                                    error: function (err) {
                                        console.log('error', err.statusText)
                                    }
                                });
                            },
                            error: function (err) {
                                console.log('error', err.statusText)
                            }
                        });
                    }
                },
                error:function(data){ 
                    // console.log(data);
                }
            }); 
}

            // click on NO button
            function btnNo(elm) 
            { 
                var val = elm.value;
                var thidID= elm.id;
                
                var q_id = $("#"+thidID).attr('data-value');
                var questionID ="aa1"+q_id;
                $("#"+questionID).prop('disabled', true);

                /*Disable buttons*/
                $("#no"+q_id).prop('disabled', true);
                $("#yes"+q_id).prop('disabled', true);
                

                var dtValue = document.getElementById(questionID).value = "-1";

                /*Disable buttons*/
                document.getElementById("no"+q_id).classList.add("nohover");
                document.getElementById("yes"+q_id).classList.add("nohover");

                document.getElementById(questionID+"_bmsg").classList.add("mystyle");
                document.getElementById(questionID+"_hmsg1").classList.add("grascale");

                
                $.ajax({
                    url: '{{ route('frontend.question.answer.update') }}',
                    type: 'POST',
                    dataType: 'json', 
                    data: {'_token': '{{ csrf_token() }}', 'value': dtValue, 'q_id': q_id},
                    success: function(data){
                    // alert(data);
                    // console.log(data);
                    if(data['create_graph'] == false){
                        var optionsStr = JSON.stringify({
                            "chart": {
                                "plotBackgroundColor": null,
                                "plotBorderWidth": null,
                                "plotShadow": false,
                                "type": 'pie',
                                "height": "500px",
                                "backgroundColor": 'transparent',
                                "useHTML": "enabled",
                                "events": {
                                    load: function() {
                                        this.renderer.text("<i style='font-size:24px; color:#000;' class='fa'>&#xf2b9;</i>", 220, 250, true).add();
                                    }
                                },
                            },
                            "title": {
                                "style": {
                                    "fontSize": '30px'
                                },
                                "text": ''
                            },
                            "credits": {
                                "enabled": false
                            },
                            "tooltip": {
                                "pointFormat": '{series.name}: <b>{point.percentage:.1f}%</b>'
                            },
                            "plotOptions": {
                                "pie": {
                                    "allowPointSelect": true,
                                    "cursor": 'pointer',
                                    "colors": ['green', 'red'],
                                    "dataLabels": {
                                        "enabled": true,
                                        "format": '<b>{point.name}</b>: {point.percentage:.0f} %',
                                        "style": {
                                            "fontSize": '22px',
                                            "color": 'black'
                                        }
                                    }
                                }
                            },
                            "series": [{
                                "name": 'Votes',
                                "colorByPoint": true,
                                "data": [{
                                    "name": 'Yes',
                                    "y": data['yes'],
                                    "sliced": true
                                }, {
                                    "name": 'No',
                                    "y": data['no']
                                }]
                            }]
                        }),
                        dataString = encodeURI('async=true&type=png&width=360px&options=' + optionsStr);

                        $.ajax({
                            type: 'POST',
                            data: dataString,
                            url: 'http://export.highcharts.com/',
                            success: function (imageData) {
                            // console.log('get the file from relative url: ', imageData);
                            $.ajax({
                                url: '{{ route('frontend.question.chart.image') }}',
                                type: 'POST',
                                dataType: 'json', 
                                data: {'_token': '{{ csrf_token() }}', 'value': dtValue, 'image': exportUrl + imageData, 'q_id': q_id},
                                success: function (saveData) {
                                    // console.log(saveData);
                                },
                                error: function (err) {
                                    console.log('error', err.statusText)
                                }
                            });
                        },
                        error: function (err) {
                            console.log('error', err.statusText)
                        }
                    });
                    }
                },
                error:function(data){ 
                    // console.log(data);
                }
            });
}

             // click on YES button
             function btnYes(elm) 
             { 
                var val = elm.value;
                var thidID= elm.id;
                
                var q_id = $("#"+thidID).attr('data-value');
                var questionID ="aa1"+q_id;
                $("#"+questionID).prop('disabled', true);
                $("#no"+q_id).prop('disabled', true);
                $("#yes"+q_id).prop('disabled', true);

                var dtValue = document.getElementById(questionID).value = "1";
                
                /*Disable buttons*/
                document.getElementById("no"+q_id).classList.add("nohover");
                document.getElementById("yes"+q_id).classList.add("nohover");

                document.getElementById(questionID+"_hmsg1").classList.add("mystyle");
                document.getElementById(questionID+"_bmsg").classList.add("grascale");

                

                $.ajax({
                    url: '{{ route('frontend.question.answer.update') }}',
                    type: 'POST',
                    dataType: 'json', 
                    data: {'_token': '{{ csrf_token() }}', 'value': dtValue, 'q_id': q_id},
                    success: function(data){
                    // alert(data);
                    // console.log(data);
                    if(data['create_graph'] == false){
                        var optionsStr = JSON.stringify({
                            "chart": {
                                "plotBackgroundColor": null,
                                "plotBorderWidth": null,
                                "plotShadow": false,
                                "type": 'pie',
                                "height": "500px",
                                "backgroundColor": 'transparent',
                                "useHTML": "enabled",
                                "events": {
                                    load: function() {
                                        this.renderer.text("<i style='font-size:24px; color:#000;' class='fa'>&#xf2b9;</i>", 220, 250, true).add();
                                    }
                                },
                            },
                            "title": {
                                "style": {
                                    "fontSize": '30px'
                                },
                                "text": ''
                            },
                            "credits": {
                                "enabled": false
                            },
                            "tooltip": {
                                "pointFormat": '{series.name}: <b>{point.percentage:.1f}%</b>'
                            },
                            "plotOptions": {
                                "pie": {
                                    "allowPointSelect": true,
                                    "cursor": 'pointer',
                                    "colors": ['green', 'red'],
                                    "dataLabels": {
                                        "enabled": true,
                                        "format": '<b>{point.name}</b>: {point.percentage:.0f} %',
                                        "style": {
                                            "fontSize": '22px',
                                            "color": 'black'
                                        }
                                    }
                                }
                            },
                            "series": [{
                                "name": 'Votes',
                                "colorByPoint": true,
                                "data": [{
                                    "name": 'Yes',
                                    "y": data['yes'],
                                    "sliced": true
                                }, {
                                    "name": 'No',
                                    "y": data['no']
                                }]
                            }]
                        }),
                        dataString = encodeURI('async=true&type=png&width=360px&options=' + optionsStr);

                        $.ajax({
                            type: 'POST',
                            data: dataString,
                            url: 'http://export.highcharts.com/',
                            success: function (imageData) {
                            // console.log('get the file from relative url: ', imageData);
                            $.ajax({
                                url: '{{ route('frontend.question.chart.image') }}',
                                type: 'POST',
                                dataType: 'json', 
                                data: {'_token': '{{ csrf_token() }}', 'value': dtValue, 'image': exportUrl + imageData, 'q_id': q_id},
                                success: function (saveData) {
                                    // console.log(saveData);
                                },
                                error: function (err) {
                                    console.log('error', err.statusText)
                                }
                            });
                        },
                        error: function (err) {
                            console.log('error', err.statusText)
                        }
                    });
                    }
                },
                error:function(data){ 
                    // console.log(data);
                }
            });
}  
</script> 
@endsection