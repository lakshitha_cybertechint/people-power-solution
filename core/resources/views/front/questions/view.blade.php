@extends('front.layouts.app')
@section('meta')
<meta property="fb:app_id" content="116930891982664" />
@if($language == 'si')
<meta property="og:title" content="{{ $question['si_description'] }}"/>
@elseif($language == 'ta')

<meta property="og:title" content="{{ $question['ta_description'] }}"/>
@else

<meta property="og:title" content="{{ $question['en_description'] }}"/>
@endif

<meta property="og:site_name" content="People Power - Direct Democracy"/>
<meta property="og:type" content="article"/>

<meta property="og:url" content="{{ $url }}"/>
@if($meta_percent !== [])

<meta property="og:description" content="{{ round($meta_percent['yes_percent']) }}% peoples says Yes, {{ round($meta_percent['no_percent']) }}% peoples says No">
@else

<meta property="og:description" content="{{ trans('questions.no_votes_casted') }}">
@endif

@if($question['image'] !== null)
<meta property="og:image" content="{{ $question['image'] }}" />
@else
<meta property="og:image" content="{{ asset('assets/pps-assets/images/peoplepowerlogo-thumbnail.png') }}" />
@endif
<meta property="og:image:type" content="image/png">
<meta property="og:image:width" content="400">
<meta property="og:image:height" content="400">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style type="text/css">
body {
    background-color: #eeeeee!important;
}
</style>

@endsection


@section('content')



<div class="spinner-wrapper">
    <div class="spinner"></div>
</div>

<div class="container questions">

    <!-- date -->
    <br>
    <div id="date">{{ trans('questions.published_on') }} {{ $date }}</div>
    @if($language == 'si')
    <h3 style="color: red;">{{ $question['si_description'] }}</h3>
    @elseif($language == 'ta')
    <h3 style="color: red;">{{ $question['ta_description'] }}</h3>
    @else
    <h3 style="color: red;">{{ $question['en_description'] }}</h3>
    @endif
    
    @if($question == null)

    <div class="text-center">
        <p>Not Found!</p>
    </div>

    @else

    {{-- {{dd($question)}} --}}
    <!-- question start -->
    <div class="row">
        <div class="col-sm-4">
            @if($question['image'] !== null)
            <img class="img-responsive" src="{{ $question['image'] }}">
            @else
            {{ trans('questions.no_votes_casted') }} <br><br>
            <a href="{{ route('frontend.auth.login') }}" class="waves-effect white btn btn1 white-btn">{{ trans('auth.login') }}</a>

            @endif

        </div>
    </div>
</div>

@endif

@endsection