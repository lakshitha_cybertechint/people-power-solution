@extends('front.layouts.app')

@section('content')

    <div class="container">
		<div class="row">
		 	<div class="section">
		 		<div class="col s12">
		 			<a class="waves-effect white btn btn1 white-btn" href="{{ route('frontend.language') }}">REGISTER</a>
		 		</div>
			 	<div class="col s12">
			 		<a class="waves-effect white btn btn1 white-btn" href="{{ route('frontend.auth.login') }}">LOGIN</a>
			 	</div>
		 	</div>
		</div>
	</div>

@endsection