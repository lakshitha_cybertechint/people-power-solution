<style type="text/css">

@media screen and (max-width: 600px) and (min-width: 0px) {
	.nav-wrapper a {
		padding-top: 10px;
		align-content: center;
	}

	.logo-custom a img {
		padding-right: 15px;
		padding-left: 15px;
	}

}
</style>

<nav>
	<div class="nav-wrapper">
		<div class="row">
			<div class="col c4">
				<a href="{{ route('frontend.settings') }}"><i class="fa fa-cog waves-effect icon nav-user-left"></i></a>
			</div>
			<div class="col c4 center logo-custom">
				@if(Auth::check())
				<a href="{{ route('frontend.questions') }}" class="brand-logo">
					<img class="header-image" src="{{asset('assets/pps-assets/images/peoplepowerlogo.svg')}}"/>
				</a>
				@else
				<a href="{{ route('frontend.index') }}" class="brand-logo">
					<img class="header-image" src="{{asset('assets/pps-assets/images/peoplepowerlogo.svg')}}"/>
				</a>
				@endif
			</div>
			<div class="col c4 right profile-i">
				@if(Auth::check())
				<a href="{{ route('frontend.profile') }}"><i class="fa fa-user waves-effect icon nav-user-right"></i></a>
				@else
				<a href="#"><i class="fa fa-user waves-effect icon nav-user-right"></i></a>
				@endif
			</div>
		</div>
	</div>
</nav>
