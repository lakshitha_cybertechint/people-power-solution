@extends('front.layouts.app')

@section('content')


<div class="container" style="margin-top: 100px;">
    <div class="row">
        <div class="col s8 offset-s2">
            <h5 class="object">{{ trans('auth.reset') }}</h5>
            <h6 style="font-size: 14px;">{{ trans('auth.password_reset_details') }}</h6>
        </div>
    </div>
    <div class="row">
        <form action="{{ route('frontend.password.email.post') }}" method="post" class="col s8 offset-s2">
            {!! csrf_field() !!}
            <div class="input-field">
                <label class="em" for="email">{{ trans('auth.form.email') }}</label>
                <input id="email" name="email" type="email" class="validate em" required>
            </div>
            <div class="row">
                <button type="submit" class="waves-effect white btn btn1 white-btn">{{ trans('auth.send_passsword') }}</button>
            </div>
        </form>
    </div>
</div>

@endsection