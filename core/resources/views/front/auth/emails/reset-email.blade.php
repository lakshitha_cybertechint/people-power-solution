<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
    <style>
    a{
        text-decoration: none;
    }
    .btn{
        color: #fff!important;
        background-color: #d64646;
        -webkit-box-shadow: 0px 0px 5px 2px rgba(145,39,39,1);
        -moz-box-shadow: 0px 0px 5px 2px rgba(145,39,39,1);
        box-shadow: 0px 0px 5px 2px rgba(145,39,39,1);
        /*border: none;*/
        border-radius: 2px;
        display: inline-block;
        height: 36px;
        line-height: 36px;
        padding: 0 16px;
        text-transform: uppercase;
        vertical-align: middle;
        width: 160px;
        float: left;
        margin-top: 0px;
    }
    </style>
</head>
<body>
    <h2>{{ trans('auth.reset') }}</h2>

    <div>
        <h5>{{ trans('auth.password_reset_email_header') }}</h5>
        
        <a href="{{ $url}}" class="btn">{{ trans('auth.password_reset_email_link') }}</a><br><br>
    </div>

    <h5>{{ trans('auth.password_reset_email_footer') }}</h5>
    <h6>All Rights Reserved, <br> People Power</h6>

    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/js/materialize.min.js"></script>
</body>
</html>