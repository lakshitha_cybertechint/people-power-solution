@extends('front.layouts.app')

@section('content')


<div class="container" style="margin-top: 100px;">
    <div class="row">
        <div class="col s8 offset-s2">
            <h5 class="object">{{ trans('auth.forget') }}</h5>
            <h6 style="font-size: 14px;">{{ trans('auth.confirmation_details') }}</h6>
        </div>
    </div>
    <div class="row">
        <form action="{{ route('frontend.forget.password.post') }}" method="post" class="col s8 offset-s2">
            {!! csrf_field() !!}
            <div class="input-field">
                <label class="em" for="user_name">{{ trans('auth.form.user_name') }}</label>
                <input id="user_name" name="user_name" type="text" class="validate em" required>
            </div>
            <div class="input-field">
                <label class="active" for="mobile">{{ trans('auth.form.mobile') }}</label>
                <input  id="mobile" type="text" name="mobile" class="validate" required>
            </div>
            <div class="row">
                <button type="submit" class="waves-effect white btn btn1 white-btn">{{ trans('auth.verification_code') }}</button>
            </div>
        </form>
    </div>
</div>

@endsection