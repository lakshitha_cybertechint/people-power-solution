@extends('front.layouts.app')

@section('content')

<div class="container" style="margin-top: 50px;">
    <div class="row">
        <div class="">
            <form action="{{ route('frontend.email.resend.post') }}" method="post">
                {!! csrf_field() !!}
                <h5 class="object">{{ trans('auth.resend_email') }}</h5>
                {{-- <h6>{{ trans('auth.code_details') }}</h6> --}}
                <div class="input-field col s8 offset-s2">
                    <label class="em" for="email">{{ trans('auth.form.email') }}</label>
                    <input id="email" name="email" type="email" class="validate em" required>
                </div>
                <button type="submit" class="waves-effect white btn btn1 white-btn">{{ trans('auth.resend') }}</button>
                
            </form>
        </div>
    </div>
</div>

@endsection