@extends('front.layouts.app')

@section('content')


<div class="container" style="margin-top: 100px;">
    <div class="row">
        <div class="col s8 offset-s2">
            <h5 class="object">{{ trans('auth.reset') }}</h5>
            <h6 style="font-size: 14px;">{{ trans('auth.code_details') }}</h6>
        </div>
    </div>
    <div class="row">
        <form action="{{ route('frontend.password.reset.post') }}" method="post" class="col s8 offset-s2">
            {!! csrf_field() !!}
            <div class="input-field s8 m4 l2">
                <input id="code" name="code" type="number" class="validate" required>
                <label for="code">{{ trans('auth.form.code') }}</label>
            </div>
            <div class="row">
                <button type="submit" class="waves-effect white btn btn1 white-btn">{{ trans('auth.confirm') }}</button>
            </div>
        </form>
    </div>
</div>

@endsection