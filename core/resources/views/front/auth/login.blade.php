@extends('front.layouts.app')

@section('content')


<div class="container">
    <div class="row">
        <div class="col s12">
            <h5 class="object">{{ trans('auth.login') }}</h5>
            <div id="regbtn" class="row">
                <div>
                    <a href="{{ route('frontend.auth.socal', 'facebook') }}" class="waves-effect waves-light btn facebook-login-btn"><i class="fa fa-facebook-square font"></i>&nbsp;&nbsp;&nbsp;{{ trans('auth.social.facebook') }}</a>
                    <a href="{{ route('frontend.auth.socal', 'twitter') }}" class="waves-effect waves-light btn twitter-login-btn"><i class="fa fa-twitter-square font"></i>&nbsp;&nbsp;&nbsp;{{ trans('auth.social.twitter') }}</a>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col s12 m8 offset-m2 l6 offset-l3">
            <form action="{{ route('frontend.auth.login.post') }}" method="post">
                {!! csrf_field() !!}
                <h5 class="object">{{ trans('auth.or') }}</h5>
                <div class="input-field s8 m4 l2">
                    <label class="em" for="user_name">{{ trans('auth.form.user_name') }}</label>
                    <input id="user_name" name="user_name" type="text" class="validate em" required>
                </div>
                <div class="input-field s8 m4 l2">
                    <label class="active" for="pw">{{ trans('auth.form.password') }}</label>
                    <input  id="pw" type="Password" name="password" class="validate" required>
                </div>
                <div class="row">
                    <button type="submit" class="waves-effect white btn btn1 white-btn">{{ trans('auth.form.login') }}</button>

                    <button type="reset" class="waves-effect white btn btn1 white-btn">{{ trans('auth.form.reset') }}</button>

                    <!-- <a href="{{ route('frontend.language') }}" class="waves-effect white btn btn1 white-btn">{{ trans('auth.form.register') }}</a> -->
                </div>
                
                <h5><a href="{{ route('frontend.password.email') }}" style="color: #ccc;">{{ trans('auth.forget_password') }}</a></h5>

                <hr class="style-four"><br>

                <h5><a href="{{ route('frontend.language') }}" style="color: #ccc;">{{ trans('auth.havent_an_account') }}</a></h5>


            </form>
        </div>
    </div>
</div>




@endsection