@extends('front.layouts.app')
@section('content')


<div class="container" style="margin-top: 50px;">
    <div class="row">
        <div class="">
            <form action="{{ route('frontend.user.confirm.resend.post') }}" method="post">
                {!! csrf_field() !!}
                <h5 class="object">{{ trans('auth.resend_code') }}</h5>
                <div class="input-field col s8 offset-s2">
                    <label class="em" for="code">{{ trans('auth.form.user_name') }}</label>
                    <input id="code" name="user_name" type="text" class="validate em" required>
                </div>
                <div class="input-field col s8 offset-s2">
                    <label class="em" for="code">{{ trans('auth.form.mobile') }}</label>
                    <input id="code" name="mobile" type="text" class="validate em" required>
                </div>
                <button type="submit" class="waves-effect white btn btn1 white-btn">{{ trans('auth.send_code') }}</button>
                
            </form>            
        </div>
    </div>
</div>




@endsection