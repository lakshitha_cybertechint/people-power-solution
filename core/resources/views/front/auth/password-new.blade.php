@extends('front.layouts.app')

@section('content')


<div class="container" style="margin-top: 100px;">
    <div class="row">
        <div class="col s8 offset-s2">
            <h5 class="object">{{ trans('auth.new_password_header') }}</h5>
            <h6 style="font-size: 14px;">{{ trans('auth.new_password_details') }}</h6>
        </div>
    </div>
    <div class="row">
        <form action="{{ route('frontend.password.reset.post') }}" method="post" class="col s8 offset-s2">
            {!! csrf_field() !!}
            <input type="hidden" name="token" value="{{ $token }}">
            <div class="input-field s8 m4 l2">
                <input id="user_password" name="password" type="password" class="validate" required>
                <label for="user_password">{{ trans('auth.form.new_password') }}</label>
            </div>
            <div class="input-field s8 m4 l2">
                <input id="password_confirmation" name="password_confirmation" type="password" class="validate" required>
                <label for="password_confirmation">{{ trans('auth.form.confirm_password') }}</label>
            </div>
            <div class="row">
                <button type="submit" class="waves-effect white btn btn1 white-btn">{{ trans('auth.confirm') }}</button>
            </div>
        </form>
    </div>
</div>

@endsection