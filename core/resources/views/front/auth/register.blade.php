@extends('front.layouts.app')
@section('content')

<style type="text/css">
    a {
        color: #f35252;
    }
</style>

<div class="container">
    <!-- Page Content goes here -->
    <div class="col s12 m8 l6 offset-l3">
        <div id="regbtn" class="row">
            <div>
                <a href="{{ route('frontend.auth.socal', 'facebook') }}" class="waves-effect waves-light btn facebook-login-btn"><i class="fa fa-facebook-square font"></i>&nbsp;&nbsp;&nbsp;{{ trans('auth.social.facebook') }}</a>
                <a href="{{ route('frontend.auth.socal', 'twitter') }}" class="waves-effect waves-light btn twitter-login-btn"><i class="fa fa-twitter-square font"></i>&nbsp;&nbsp;&nbsp;{{ trans('auth.social.twitter') }}</a>
            </div>
        </div>
        <h5 class="object">{{ trans('auth.register') }}</h5>
    </div>
    <div class="row">
        <div class="col s12 m8 offset-m2 l6 offset-l3">
        <form class="" action="{{ route('frontend.user.create') }}" method="post">
            {!! csrf_field() !!}
            <div class="input-field s8 m4 l2">
                <input id="name" name="name" value="{{ old('name') }}" type="text" class="validate" required>
                <label for="name">{{ trans('auth.form.name') }}</label>
            </div>
            <div class="input-field s8 m4 l2">
                <input id="user_name" name="user_name"  value="{{ old('user_name') }}" type="text" class="validate" required>
                <label for="user_name">{{ trans('auth.form.user_name') }}</label>
            </div>
            <div class="input-field s8 m4 l2">
                <input id="email" name="email"  value="{{ old('email') }}" type="email" class="validate" required>
                <label for="email">{{ trans('auth.form.email') }}</label>
            </div>
            <div class="input-field s8 m4 l2">
                <input id="mobile_number" name="mobile"  value="{{ old('mobile') }}" type="text" class="validate">
                <label for="mobile_number">{{ trans('auth.form.mobile') }}</label>
            </div>
            <div class="input-field s8 m4 l2">
                <input id="user_password" name="password" type="password" class="validate" required>
                <label for="user_password">{{ trans('auth.form.password') }}</label>
            </div>
            <div class="input-field s8 m4 l2">
                <input id="password_confirmation" name="password_confirmation" type="password" class="validate" required>
                <label for="password_confirmation">{{ trans('auth.form.confirm_password') }}</label>
            </div>

            <div class="row">
            <div class="row mtop">

            <div class="row mtop">

                <div class="col s10 offset-s1 m8 l12">
                <button type="submit" class="waves-effect white btn btn1 white-btn">{{ trans('auth.form.register') }}</button>
                {{-- <a href="{{ route('frontend.questions') }}" class="waves-effect white btn btn1 white-btn">Register</a> --}}
                
                <button type="reset" class="waves-effect white btn btn1 white-btn">{{ trans('auth.form.reset') }}</button>
                {{-- Reset --}}
                </div>

            </div>
            
            <hr class="style-four"><br>
            
            <h5>
                {{ trans('auth.already_have_account') }} <a href="{{ route('frontend.questions') }}">{{ trans('auth.login') }}</a>
            </h5>
            
        </form>
    </div>
    </div>
</div>
@endsection