@extends('front.layouts.app')

@section('content')

<div class="container" style="margin-top: 50px;">
    <div class="row">
        <div class="">
            <form action="{{ route('frontend.user.confirm.activate') }}" method="post">
                {!! csrf_field() !!}
                <h5 class="object">{{ trans('auth.confirm_register') }}</h5>
                <h6>{{ trans('auth.code_details') }}</h6>
                <div class="input-field col s8 offset-s2">
                    <label class="em" for="code">{{ trans('auth.form.code') }}</label>
                    <input id="code" name="code" type="number" class="validate em" required>
                </div>
                <button type="submit" class="waves-effect white btn btn1 white-btn">{{ trans('auth.confirm_account') }}</button>
                
            </form>


            <h5>{{ trans('auth.receive_code') }}<a href="{{ route('frontend.user.confirm.resend') }}" style="color: #ccc;"> {{ trans('auth.send_code') }}</a></h5>
        </div>
    </div>
</div>

@endsection