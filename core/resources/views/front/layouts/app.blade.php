<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta name="google" content="notranslate" />
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @yield('meta')
    <link rel="stylesheet" href="{{asset('assets/pps-assets/css/materialize.min.css')}}">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('assets/pps-assets/css/normalize.css')}}">
    <link rel="stylesheet" href="{{asset('assets/pps-assets/css/main.css')}}">
    <link rel="stylesheet" href="{{asset('assets/pps-assets/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('assets/pps-assets/css/font-awesome.css')}}">
    <link rel="stylesheet" href="{{asset('assets/pps-assets/css/docs.css')}}">
    <style type="text/css">
    .section{
        margin-top: 30%;
    }
    #loader img {
        width: 100%!important;
        left: -15%!important;
    }
    .btn1{
        margin-top: 0px;
        text-align: center;
        width: 50%;
        color: #c62828!important;
        font-weight: bold;
    }
    .header-image {
        width: 200px!important;
    }
    body{
        background-color: #C62828;
    }

    .white-btn {
        color: #c62828;
    }

    .facebook-login-btn {
        background-color: #4267b2;
        font-weight: 600;
    }

    .twitter-login-btn {
        background-color: #1da1f2;
        font-weight: 600;
    }
    .btn i {
        font-size: 15px;
        line-height: inherit;
    }





    /*responsive*/
    @media screen and (min-width: 0px) and (max-width: 360px) {
        .section{
           margin-top: 30%;
       }
       .btn1{
        margin-bottom: 10px;
        width: 90%;
        text-align: center;
        color: #c62828!important;
    }
    #loader img {
        width: 100%!important;
        left: 0%!important;
    }
    #loader {
        display: block;
        position: relative;
        left: 25%;
        top: 20%;
        width: 50%;
        margin: 0 0 0 0;
    }

    .nav-wrapper {
        padding: 0px 0px 5px 0px;
        position: absolute;
    }

    .row .col {
        padding: 0 0!important;
    }

            /*.nav-wrapper {
                padding: 0px 10px 5px 10px;
                position: absolute;
                }*/

                .nav-wrapper i {
                    line-height: 25px!important;
                    height: 45px!important;
                }

                .nav-wrapper a {
                    padding-top: 6px;
                    align-content: center;
                }

                nav {
                    background-color: #a72222;
                    height: 65px;
                }

                .nav-user-right {
                    float: right;
                    padding-top: 10px;
                    margin-top: 10px!important;
                }

                .nav-user-left {
                    float: left;
                    padding-top: 10px;
                    margin-top: 10px!important;
                }

                .header-image {
                    width: 240px!important;
                }



                #regbtn {
                    margin-top: 25px;
                }
                h5.object{
                    font-size: 20px;
                    margin: 0 0 0 0;
                }
                .input-field>label {
                    font-size: 16px;
                }
                input:not([type]), input[type=text]:not(.browser-default), input[type=password]:not(.browser-default), input[type=email]:not(.browser-default), input[type=url]:not(.browser-default), input[type=time]:not(.browser-default), input[type=date]:not(.browser-default), input[type=datetime]:not(.browser-default), input[type=datetime-local]:not(.browser-default), input[type=tel]:not(.browser-default), input[type=number]:not(.browser-default), input[type=search]:not(.browser-default), textarea.materialize-textarea {
                    height: 4rem;
                }
                .btn, .btn-large, .btn-small, .btn-floating, .btn-large, .btn-small, .btn-flat{
                    font-size: 14px;
                    height: 36px;
                    padding: 0px 20px 0px 20px;
                    width: 90%;
                    margin-bottom: 10px;
                }

            }





            @media screen and (min-width: 361px) and (max-width: 550px) {
                .section{
                   margin-top: 30%;
               }
               .btn1{
                margin-bottom: 10px;
                width: 90%;
                text-align: center;
                color: #c62828!important;
            }
            #loader img {
                width: 100%!important;
                left: 0%!important;
            }
            #loader {
                display: block;
                position: relative;
                left: 25%;
                top: 20%;
                width: 50%;
                margin: 0 0 0 0;
            }

            .nav-wrapper {
                padding: 0px 0px 5px 0px;
                position: absolute;
            }

            .row .col {
                padding: 0 0!important;
            }

            /*.nav-wrapper {
                padding: 0px 10px 5px 10px;
                position: absolute;
                }*/

                .nav-wrapper i {
                    line-height: 25px!important;
                    height: 45px!important;
                }

                .nav-wrapper a {
                    padding-top: 6px;
                    align-content: center;
                }

                nav {
                    background-color: #a72222;
                    height: 65px;
                }

                .nav-user-right {
                    float: right;
                    padding-top: 10px;
                    margin-top: 10px!important;
                }

                .nav-user-left {
                    float: left;
                    padding-top: 10px;
                    margin-top: 10px!important;
                }

                .header-image {
                    width: 240px!important;
                }


                #regbtn {
                    margin-top: 25px;
                }
                h5.object{
                    font-size: 25px;
                    margin: 0 0 0 0;
                }
                .input-field>label {
                    font-size: 18px;
                }
                input:not([type]), input[type=text]:not(.browser-default), input[type=password]:not(.browser-default), input[type=email]:not(.browser-default), input[type=url]:not(.browser-default), input[type=time]:not(.browser-default), input[type=date]:not(.browser-default), input[type=datetime]:not(.browser-default), input[type=datetime-local]:not(.browser-default), input[type=tel]:not(.browser-default), input[type=number]:not(.browser-default), input[type=search]:not(.browser-default), textarea.materialize-textarea {
                    height: 4rem;
                }
                .btn, .btn-large, .btn-small, .btn-floating, .btn-large, .btn-small, .btn-flat{
                    font-size: 15px;
                    height: 36px;
                    padding: 0px 20px 0px 20px;
                    width: 90%;
                    margin-bottom: 10px;
                }



            }





            @media screen and (min-width: 551px) and (max-width: 768px) {
                .section{
                   margin-top: 30%;
               }
               .btn1{
                margin-bottom: 10px;
                width: 90%;
                text-align: center;
                color: #c62828!important;
            }
            #loader img {
                width: 100%!important;
                left: 0%!important;
            }
            #loader {
                display: block;
                position: relative;
                left: 25%;
                top: 20%;
                width: 50%;
                margin: 0 0 0 0;
            }

            .nav-wrapper {
                padding: 0px 0px 5px 0px;
                position: absolute;
            }

            .row .col {
                padding: 0 0!important;
            }

            /*.nav-wrapper {
                padding: 0px 10px 5px 10px;
                position: absolute;
                }*/

                .nav-wrapper i {
                    line-height: 25px!important;
                    height: 45px!important;
                }

                .nav-wrapper a {
                    padding-top: 6px;
                    align-content: center;
                }

                nav {
                    background-color: #a72222;
                    height: 65px;
                }

                .nav-user-right {
                    float: right;
                    padding-top: 10px;
                    margin-top: 10px!important;
                }

                .nav-user-left {
                    float: left;
                    padding-top: 10px;
                    margin-top: 10px!important;
                }

                .header-image {
                    width: 240px!important;
                }



                #regbtn {
                    margin-top: 25px;
                }
                h5.object{
                    font-size: 25px;
                    margin: 0 0 0 0;
                }
                .input-field>label {
                    font-size: 18px;
                }
                input:not([type]), input[type=text]:not(.browser-default), input[type=password]:not(.browser-default), input[type=email]:not(.browser-default), input[type=url]:not(.browser-default), input[type=time]:not(.browser-default), input[type=date]:not(.browser-default), input[type=datetime]:not(.browser-default), input[type=datetime-local]:not(.browser-default), input[type=tel]:not(.browser-default), input[type=number]:not(.browser-default), input[type=search]:not(.browser-default), textarea.materialize-textarea {
                    height: 4rem;
                }
                .btn, .btn-large, .btn-small, .btn-floating, .btn-large, .btn-small, .btn-flat{
                    font-size: 15px;
                    height: 36px;
                    padding: 0px 20px 0px 20px;
                    width: 90%;
                    margin-bottom: 10px;
                }

            }





            @media screen and (min-width: 769px) and (max-width: 800px) {
                .section{
                    margin-top: 30%;
                }
                .btn1{
                    margin-bottom: 10px;
                    width: 90%;
                    text-align: center;
                    color: #c62828!important;
                }
                #loader img {
                    width: 100%!important;
                    left: 0%!important;
                }
                #loader {
                    display: block;
                    position: relative;
                    left: 25%;
                    top: 20%;
                    width: 50%;
                    margin: 0 0 0 0;
                }

                .nav-wrapper {
                    padding: 0px 0px 5px 0px;
                    position: absolute;
                }

                .row .col {
                    padding: 0 0!important;
                }

            /*.nav-wrapper {
                padding: 0px 10px 5px 10px;
                position: absolute;
                }*/

                .nav-wrapper i {
                    line-height: 25px!important;
                    height: 45px!important;
                }

                .nav-wrapper a {
                    padding-top: 6px;
                    align-content: center;
                }

                nav {
                    background-color: #a72222;
                    height: 65px;
                }

                .nav-user-right {
                    float: right;
                    padding-top: 10px;
                    margin-top: 10px!important;
                }

                .nav-user-left {
                    float: left;
                    padding-top: 10px;
                    margin-top: 10px!important;
                }

                .header-image {
                    width: 240px!important;
                }



                #regbtn {
                    margin-top: 25px;
                }
                h5.object{
                    font-size: 25px;
                    margin: 0 0 0 0;
                }
                .input-field>label {
                    font-size: 16px;
                }
                input:not([type]), input[type=text]:not(.browser-default), input[type=password]:not(.browser-default), input[type=email]:not(.browser-default), input[type=url]:not(.browser-default), input[type=time]:not(.browser-default), input[type=date]:not(.browser-default), input[type=datetime]:not(.browser-default), input[type=datetime-local]:not(.browser-default), input[type=tel]:not(.browser-default), input[type=number]:not(.browser-default), input[type=search]:not(.browser-default), textarea.materialize-textarea {
                    height: 3rem;
                }
                .btn, .btn-large, .btn-small, .btn-floating, .btn-large, .btn-small, .btn-flat{
                    font-size: 15px;
                    height: 36px;
                    padding: 0px 20px 0px 20px;
                    width: 60%;
                    margin-bottom: 10px;
                }
            }





            @media screen and (min-width: 801px) and (max-width: 980px) {
                .section{
                    margin-top: 30%;
                }
                .btn1{
                    margin-bottom: 10px;
                    width: 90%;
                    text-align: center;
                    color: #c62828!important;
                }
                #loader img {
                    width: 100%!important;
                    left: 0%!important;
                }
                #loader {
                    display: block;
                    position: relative;
                    left: 25%;
                    top: 20%;
                    width: 50%;
                    margin: 0 0 0 0;
                }

                .nav-wrapper {
                    padding: 0px 0px 5px 0px;
                    position: absolute;
                }

                .row .col {
                    padding: 0 0!important;
                }

            /*.nav-wrapper {
                padding: 0px 10px 5px 10px;
                position: absolute;
                }*/

                .nav-wrapper i {
                    line-height: 25px!important;
                    height: 45px!important;
                }

                .nav-wrapper a {
                    padding-top: 6px;
                    align-content: center;
                }

                nav {
                    background-color: #a72222;
                    height: 65px;
                }

                .nav-user-right {
                    float: right;
                    padding-top: 10px;
                    margin-top: 10px!important;
                }

                .nav-user-left {
                    float: left;
                    padding-top: 10px;
                    margin-top: 10px!important;
                }

                .header-image {
                    width: 240px!important;
                }



                #regbtn {
                    margin-top: 25px;
                }
                h5.object{
                    font-size: 25px;
                    margin: 0 0 0 0;
                }
                .input-field>label {
                    font-size: 16px;
                }
                input:not([type]), input[type=text]:not(.browser-default), input[type=password]:not(.browser-default), input[type=email]:not(.browser-default), input[type=url]:not(.browser-default), input[type=time]:not(.browser-default), input[type=date]:not(.browser-default), input[type=datetime]:not(.browser-default), input[type=datetime-local]:not(.browser-default), input[type=tel]:not(.browser-default), input[type=number]:not(.browser-default), input[type=search]:not(.browser-default), textarea.materialize-textarea {
                    height: 3rem;
                }
                .btn, .btn-large, .btn-small, .btn-floating, .btn-large, .btn-small, .btn-flat{
                    font-size: 15px;
                    height: 36px;
                    padding: 0px 20px 0px 20px;
                    width: 40%;
                    margin-bottom: 10px;
                }
            }





            @media screen and (min-width: 981px) and (max-width: 2000px) {
                .section{
                    margin-top: 30%;
                }
                .btn1{
                    margin-bottom: 10px;
                    width: 30%;
                    text-align: center;
                    color: #c62828!important;
                }
                #loader img {
                    width: 100%!important;
                    left: 0%!important;
                }
                #loader {
                    display: block;
                    position: relative;
                    left: 40%;
                    top: 25%;
                    width: 20%;
                    margin: 0 0 0 0;
                }

                .nav-wrapper {
                    padding: 0px 0px 5px 0px;
                    position: absolute;
                }

                .row .col {
                    padding: 0 0!important;
                }

            /*.nav-wrapper {
                padding: 0px 10px 5px 10px;
                position: absolute;
                }*/

                .nav-wrapper i {
                    line-height: 25px!important;
                    height: 45px!important;
                }

                .nav-wrapper a {
                    padding-top: 6px;
                    align-content: center;
                }

                nav {
                    background-color: #a72222;
                    height: 65px;
                }

                .nav-user-right {
                    float: right;
                    padding-top: 10px;
                    margin-top: 10px!important;
                }

                .nav-user-left {
                    float: left;
                    padding-top: 10px;
                    margin-top: 10px!important;
                }

                .header-image {
                    width: 240px!important;
                }



                #regbtn {
                    margin-top: 45px;
                }
                h5.object{
                    font-size: 25px;
                    margin: 0 0 0 0;
                }
                .input-field>label {
                    font-size: 16px;
                }
                input:not([type]), input[type=text]:not(.browser-default), input[type=password]:not(.browser-default), input[type=email]:not(.browser-default), input[type=url]:not(.browser-default), input[type=time]:not(.browser-default), input[type=date]:not(.browser-default), input[type=datetime]:not(.browser-default), input[type=datetime-local]:not(.browser-default), input[type=tel]:not(.browser-default), input[type=number]:not(.browser-default), input[type=search]:not(.browser-default), textarea.materialize-textarea {
                    height: 3rem;
                }
                .btn, .btn-large, .btn-small, .btn-floating, .btn-large, .btn-small, .btn-flat{
                    font-size: 15px;
                    height: 36px;
                    padding: 0px 20px 0px 20px;
                    width: 40%;
                    margin-bottom: 10px;
                }
            }
        </style>


        @include('front.includes.header')

    </head>

    <body class="demo">

        <div id="content">
            @include('front.messages.errors')
            @yield('content')
        </div>
        @include('front.includes.footer')
        @yield('after-footer')

    </body>
    </html>
