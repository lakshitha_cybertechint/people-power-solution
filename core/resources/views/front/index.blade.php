@extends('front.layouts.app')

<meta http-equiv="refresh" content="4;url=select" />

@section('content')

<style type="text/css">
	@media screen and (min-width: 321px) and (max-width: 360px) {
		.header-image {
    		width: 200px!important;
		}
	}

	@media screen and (min-width: 361px) and (max-width: 768px) {
		.header-image {
    		width: 190px!important;
		}
	}

	@media screen and (min-width: 769px) and (max-width: 800px) {
		.header-image {
    		width: 250px!important;
		}

		.btn {
			font-size: 18px;
		}
	}

	@media screen and (min-width: 801px) and (max-width: 980px) {
		.header-image {
    		width: 250px!important;
		}

		.btn {
			font-size: 24px;
		}		
	}

	@media screen and (min-width: 981px) {
			.btn {
			font-size: 16px;
		}		
	}

</style>

<!-- start logo blink -->
<div id="demo-content">
    <div id="loader-wrapper">
        <div id="loader">
        	<img class="blink-image" src="{{asset('assets/pps-assets/images/peoplepowerlogo-thumbnail.svg')}}">
        </div>
        <div class="loader-section section-left"></div>
        <div class="loader-section section-right"></div>
    </div>
</div>
<!-- start logo blink -->


    <!-- <div class="container">
		<div class="row">
		 	<div class="section">
		 		<div class="col s12">
		 			<a class="waves-effect white btn btn1 white-btn" href="{{ route('frontend.language') }}">REGISTER</a>
		 		</div>
			 	<div class="col s12">
			 		<a class="waves-effect white btn btn1 white-btn" href="{{ route('frontend.auth.login') }}">LOGIN</a>
			 	</div>
		 	</div>
		</div>
	</div> -->

@endsection