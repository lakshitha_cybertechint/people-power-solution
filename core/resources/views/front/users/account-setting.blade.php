@extends('front.layouts.app')

@section('content')
<div class="container">
  <div class="spinner-wrapper">
    <div class="spinner"></div>
  </div>
  <!-- Page Content goes here -->
  <div id="object" class="col s12">
    <h5>{{ trans('auth.settings.account_settings') }}</h5>
  </div>

  <div class="row">
    <form action="{{ route('frontend.member.update') }}" method="POST">
      {{ csrf_field() }}
      <div class="col-sm-4"><br></div>
      <div class="input-field s8 l6">
        <input id="name" name="name" type="text" class="validate" value="{{ $member->name }}" required>
        <label for="name">{{ trans('auth.form.name') }}</label>
      </div>
      <div class="input-field s8 l6">
        <input id="user_name" type="text" name="user_name" class="validate"  value="{{ $member->user_name }}" required>
        <label for="user_name">{{ trans('auth.form.user_name') }}</label>
      </div>
      <div class="input-field s8 m4 l2">
        <input id="email" name="email" type="email" class="validate" required value="{{ $member->email }}">
        <label for="email">{{ trans('auth.form.email') }}</label>
      </div>
      <div class="input-field s8 m4 l2">
        <input id="mobile" name="mobile" type="text" class="validate"  value="{{ $member->mobile }}" required>
        <label for="mobile">{{ trans('auth.form.mobile') }}</label>
      </div>
      <div class="col-sm-4"><br></div>
      <div class="col s12">
        <button type="submit" class="waves-effect white btn btn1 white-btn">{{ trans('auth.settings.update_settings') }}</button>
      </div>
    </form>
  </div>

  <div class="row">
    <form action="{{ route('frontend.member.change.password') }}" method="POST">
      {{ csrf_field() }}
      @if(($member->social == null && $member->password !== null) || ($member->social !== null && $member->password !== null))
      <div class="input-field s8 m4 l2">
        <input id="old_password" name="old_password" type="password" class="validate" required>
        <label for="old_password">{{ trans('auth.settings.form.current_password') }}</label>
      </div>
      @endif
      <div class="input-field s8 m4 l2">
        <input id="password" name="password" type="password" class="validate" required>
        <label for="password">{{ trans('auth.form.password') }}</label>
      </div>
      <div class="input-field s8 m4 l2">
        <input id="password_confirmation" name="password_confirmation" type="password" class="validate" required>
        <label for="password_confirmation">{{ trans('auth.settings.form.confirm_password') }}</label>
      </div>
      <div class="col s12">
        <button type="submit" class="waves-effect white btn btn1 white-btn">{{ trans('auth.settings.change_password') }}</button>
      </div>
    </form>
  </div>

</div>



@endsection