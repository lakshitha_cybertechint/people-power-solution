@extends('front.layouts.app')



@section('content')

<div class="container">
	<div class="row">
		<div class="col s10 offset-s1 m8 offset-m2 l10 offset-l1">
			<div class="row">
				<div class="col s12">
					<a href="{{ route('frontend.language.change', 'english') }}"  class="waves-effect white btn btn1 @if(\App::getLocale() == 'en') disabled @endif">{{ trans('profile.english') }}</a>
				</div>
				<div class="col s12">
					<a href="{{ route('frontend.language.change', 'sinhala') }}" class="waves-effect white btn btn1 @if(\App::getLocale() == 'si') disabled @endif">{{ trans('profile.sinhala') }}</a>
				</div>
				<div class="col s12">
					<a href="{{ route('frontend.language.change', 'tamil') }}"  class="waves-effect white btn btn1 @if(\App::getLocale() == 'ta') disabled @endif">{{ trans('profile.tamil') }}</a>
				</div>
			</div>
			<hr class="style-four"><br>
			<div class="row">
				<div class="col s12">
					<a href="{{ route('frontend.questions') }}" class="waves-effect white btn btn1 white-btn">{{ trans('profile.back_to_quetsions') }}</a>
				</div>
				<div class="col s12">
					<a href="{{ route('frontend.questions.update') }}" class="waves-effect white btn btn1 white-btn">{{ trans('profile.vote_update') }}</a>
				</div>
			</div>
			<hr class="style-four"><br>
			<div class="row">
				<a href="{{ route('frontend.auth.logout') }}" class="waves-effect white btn btn1 white-btn">{{ trans('profile.logout') }}</a>
			</div>
		</div>
	</div>
</div>
@endsection