@extends('layouts.back.master') @section('current_title','WELLCOME tO SAMBOLE ADMIN WEB PORTAL')
@section('css')
<style type="text/css">
    .clr-yellow div {
    background-color: #f8ac59;
    color: #ffffff;
    }
    .clr-blue div {
    background-color: #1c84c6;
    color: #FFFFFF;
    }
    .clr-red div {
    background-color: #ed5565;
    color: #ffffff;
    }
    .clr-green div {
    background-color: #1ab394;
    color: #ffffff;
    }
    .clr-ibox
    {
    background-color: #595959;
    }
    #alertmod_table_list_2 {
    top: 900px!important;
    }
    .wrapper-content {
    padding-top: 0;
    }
    .no-margins {
    /*text-align: center;*/
    font-weight: bold;
    }
    .margins {
    margin-top: 20px;
    }
</style>

@stop
@section('current_path')

@stop
@section('page_header')
 <div class="col-lg-9">
    <h2>Dashboard</h2>
    <ol class="breadcrumb">
        <li>
            <a href="{{url('/')}}">Home</a>
        </li>
        <li class="active">
            <strong>Dashboard</strong>
        </li>
    </ol>
</div>
@stop
@section('content')
 <div class="row">
    <div class="col-lg-3 margins">
        <div class="ibox float-e-margins clr-green">
            <div class="ibox-title ">
                {{-- <span class="label label-danger pull-right clr-ibox">Monthly</span> --}}
                <h5>Question Expire Today</h5>
            </div>
            <div class="ibox-content">
                <!-- <h2 class="no-margins"></h2> -->
                <h2 class="no-margins" > {{ $expire->count() }} <i class="fa fa-calendar pull-right"></i></h2>
                <!-- <div class="stat-percent font-bold text-danger">00% <i class="fa fa-level-down"></i></div> -->
                <small>question that are expiring today</small>
            </div>
        </div>
    </div>
    <div class="col-lg-3 margins">
        <div class="ibox float-e-margins clr-blue" >
            <div class="ibox-title ">
                <h5>Total No. of Questions</h5>
            </div>
            <div class="ibox-content ">
                <h2 class="no-margins" > {{ $question->count() }}<i class="fa fa-question pull-right"></i></h2>
                <!-- <div class="stat-percent font-bold text-info">20% <i class="fa fa-level-up"></i></div> -->
                <small>total number of question </small>
            </div>
        </div>
    </div>
    <div class="col-lg-3 margins">
        <div class="ibox float-e-margins clr-yellow" >
            <div class="ibox-title" >
                <h5>Total Yes Votes</h5>
            </div>
            <div class="ibox-content" >
                <h2 class="no-margins" >{{ $yes }} <i class="fa fa-thumbs-up pull-right"></i></h2>
                <!-- <div class="stat-percent font-bold text-success">98% <i class="fa fa-bolt"></i></div>  -->
                <small>total number of yes votes</small>
            </div>
        </div>
    </div>
    <div class="col-lg-3 margins">
        <div class="ibox float-e-margins clr-red">
            <div class="ibox-title ">
                <h5>Total No Votes</h5>
            </div>
            <div class="ibox-content">
                <!-- <h1 class="no-margins">000000000</h1> -->
                <h2 class="no-margins" > {{ $no }} <i class="fa fa-thumbs-down pull-right"></i></h2>
                <!-- <div class="stat-percent font-bold text-navy">00% <i class="fa fa-level-up"></i></div> -->
                <small>total number of no vots</small>
            </div>
        </div>
    </div>
</div>
  <div class="row">
    <div class="col-lg-12">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>Recently Added Questions</h5>
            <div class="ibox-tools">
                <a class="collapse-link">
                    <i class="fa fa-chevron-up"></i>
                </a>
            </div>
        </div>
        <div class="ibox-content">

            <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover dataTables-example" >
        <thead>
        <tr>
            <th>Question</th>
            <th>Event</th>
            <th>Category</th>
            <th>Yes</th>
            <th>No</th>
            <th>Created</th>
            <th>View</th>
        </tr>
        </thead>
        <tbody>
          @foreach ($questions as $key => $value)
            <tr>
              <td>
                {{substr(strip_tags($value->en_description), 0 , 32).".."}}
              </td>
              <td>
                {{ $value->event->name }}
              </td>
              <td>
                {{ $value->questionCategory->name }}
              </td>
              <td>
                {{ $value->yes }}
              </td>
              <td>
                {{ $value->no }}
              </td>
              <td>
                {{ $value->created_at }}
              </td>
              <td>
                <a href="{{ url('admin/question/edit/'.$value->id) }}">
                  <i class="fa fa-eye"></i>
                </a>
              </td>
            </tr>
          @endforeach
        </tbody>
        </table>
            </div>

        </div>
    </div>
</div>
</div>

@stop
@section('js')

<script type="text/javascript">
  $(document).ready(function(){

  });


</script>
@stop
