
<!DOCTYPE html>
<html lang="en-US" prefix="og: http://ogp.me/ns#">
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <script src='../www.google.com/recaptcha/api.js'></script>
        <title>Sambole.lk - Classifieds in Sri Lanka</title>
        <meta name="description" content="Sambole enables you to search or buy anything from used to new products as well as services offered. Advertising is completely FREE for all categories."/>
        <link rel="canonical" href={{url('/')}}" />
        <meta property="og:locale" content="en_US" />
        <meta property="og:type" content="website" />
        <meta property="og:title" content="Sambole.lk - Classifieds in Sri Lanka" />
        <meta property="og:description" content="Sambole enables you to search or buy anything from used to new products as well as services offered. Advertising is completely FREE for all categories." />
        <meta property="og:url" content="http://www.sambole.lk/" />
        <meta property="og:site_name" content="Sambole" />
        <meta name="twitter:card" content="summary" />
        <meta name="twitter:description" content="Sambole enables you to search or buy anything from used to new products as well as services offered. Advertising is completely FREE for all categories." />
        <meta name="twitter:title" content="Sambole.lk - Classifieds in Sri Lanka" />
        <meta name="twitter:creator" content="@com" />
        <script type='application/ld+json'>{"@context":"http:\/\/schema.org","@type":"WebSite","@id":"#website","url":"http:\/\/www.sambole.lk\/","name":"Sambole","potentialAction":{"@type":"SearchAction","target":"http:\/\/www.sambole.lk\/?s={search_term_string}","query-input":"required name=search_term_string"}}</script>
        <!-- / Yoast SEO plugin. -->
        <link rel='dns-prefetch' href='http://maps.googleapis.com/' />
        <link rel='dns-prefetch' href="{{url('/')}}" />
        <link rel='dns-prefetch' href='http://cdnjs.cloudflare.com/' />
        <link rel='dns-prefetch' href='http://fonts.googleapis.com/' />
        <link rel='dns-prefetch' href='http://s.w.org/' />
        <link rel="alternate" type="application/rss+xml" title="Sambole &raquo; Home Page Comments Feed" href="home-page/feed/index.html" />
        <script src="{{asset('assets/front/js/emojiSettings.js')}}"></script>
        <style type="text/css">
            img.wp-smiley,
            img.emoji {
            display: inline !important;
            border: none !important;
            box-shadow: none !important;
            height: 1em !important;
            width: 1em !important;
            margin: 0 .07em !important;
            vertical-align: -0.1em !important;
            background: none !important;
            padding: 0 !important;
            }
        </style>
        <link rel='stylesheet' id='classifieds-awesome-css'  href="{{asset('assets/front/css/font-awesome.mine100.css?ver=4.7.2')}}" type='text/css' media='all' />
        <link rel='stylesheet' id='classifieds-bootstrap-css'  href="{{asset('assets/front/css/bootstrap.mine100.css?ver=4.7.2')}}" type='text/css' media='all' />
        <link rel='stylesheet' id='classifieds-carousel-css'  href="{{asset('assets/front/css/owl.carousele100.css?ver=4.7.2')}}" type='text/css' media='all' />
        <link rel='stylesheet' id='classifieds-navigation-font-css'  href='http://fonts.googleapis.com/css?family=Muli%3A100%2C300%2C400%2C700%2C900%2C100italic%2C300italic%2C400italic%2C700italic%2C900italic&amp;ver=1.0.0' type='text/css' media='all' />
        <!-- <link rel='stylesheet' id='buttons-css'  href='wp-includes/css/buttons.mine100.css?ver=4.7.2' type='text/css' media='all' /> -->
        <!-- <link rel='stylesheet' id='dashicons-css'  href='wp-includes/css/dashicons.mine100.css?ver=4.7.2' type='text/css' media='all' /> -->
        <link rel='stylesheet' id='mediaelement-css'  href="{{asset('assets/front/js/mediaelement/mediaelementplayer.min51cd.css?ver=2.22.0')}}" type='text/css' media='all' />
        <link rel='stylesheet' id='wp-mediaelement-css'  href="{{asset('assets/front/js/mediaelement/wp-mediaelement.mine100.css?ver=4.7.2')}}" type='text/css' media='all' />
        <!-- <link rel='stylesheet' id='media-views-css'  href='wp-includes/css/media-views.mine100.css?ver=4.7.2' type='text/css' media='all' /> -->
        <link rel='stylesheet' id='imgareaselect-css'  href="{{asset('assets/front/js/imgareaselect/imgareaselect3bf4.css?ver=0.9.8')}}" type='text/css' media='all' />
        <link rel='stylesheet' id='classifieds-select2-css'  href="{{asset('assets/front/js/select2/select2e100.css?ver=4.7.2')}}" type='text/css' media='all' />
        <link rel='stylesheet' id='classifieds-magnific-popup-css'  href="{{asset('assets/front/css/magnific-popupe100.css?ver=4.7.2')}}" type='text/css' media='all' />
        <!-- <link rel='stylesheet' id='dropzonecss-css'  href="{{asset('assets/front/ajax/libs/dropzone/4.2.0/min/dropzone.mine100.css?ver=4.7.2')}}" type='text/css' media='all' /> -->
        <link rel='stylesheet' href="{{asset('core/vendor/dropzone/css/dropzone.css')}}" type='text/css' media='all' />
        <link rel='stylesheet' href="{{asset('core/vendor/dropzone/css/basic.css')}}" type='text/css' media='all' />
        <link rel='stylesheet' id='classifieds-style-css'  href="{{asset('assets/front/stylee100.css?ver=4.7.2')}}" type='text/css' media='all' />
         <link rel='stylesheet' id='classifieds-style-css'  href="{{asset('assets/front/css/classifieds-style-inline-css.css?ver=4.7.2')}}" type='text/css' media='all' />
        <script type='text/javascript' src="{{asset('assets/front/js/jquery/jqueryb8ff.js?ver=1.12.4')}}"></script>
        <script type='text/javascript' src="{{asset('assets/front/js/jquery/jquery-migrate.min330a.js?ver=1.4.1')}}"></script>
        <script type='text/javascript'>
            /* <![CDATA[ */
            var userSettings = {"url":"\/","uid":"0","time":"1518542940","secure":""};
            /* ]]> */
        </script>
        <script type='text/javascript' src="{{asset('assets/front/js/utils.mine100.js?ver=4.7.2')}}"></script>
        <script type='text/javascript' src="{{asset('assets/front/js/plupload/plupload.full.mincc91.js?ver=2.1.8')}}"></script>
        <!--[if lt IE 8]>
        <script type='text/javascript' src="{{asset('assets/front/js/json2.min.js?ver=2015-05-03')}}"></script>
        <![endif]-->
        <script type="text/javascript" src="{{asset('core/vendor/dropzone/js/dropzone.js')}}"></script>
        <!-- <script type='text/javascript' src="{{asset('assets/front/ajax/libs/dropzone/4.2.0/min/dropzone.mine100.js?ver=4.7.2')}}"></script> -->
        <link rel='https://api.w.org/' href="{{asset('assets/front/json/index.html')}}" />
        <link rel="EditURI" type="application/rsd+xml" title="RSD" href="xmlrpc0db0.html?rsd" />
        <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="{{asset('assets/front/xml/wlwmanifest.xml')}}" />
        <meta name="generator" content="WordPress 4.7.2" />
        <link rel='shortlink' href="{{url('/')}}" />
        <link rel="alternate" type="application/json+oembed" href="{{asset('assets/front/json/oembed/1.0/embed2e4e.json?url=http%3A%2F%2Fwww.sambole.lk%2F')}}" />
        <link rel="alternate" type="text/xml+oembed" href="{{asset('assets/front/json/oembed/1.0/embed7d96?url=http%3A%2F%2Fwww.sambole.lk%2F&amp;format=xml')}}" />
        <script type="text/javascript">var ajaxurl = 'wp-admin/admin-ajax.html';</script>       
        <style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-108362078-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', 'UA-108362078-1');
        </script>
    @yield('css')
    </head>
<body class="page-template page-template-page-tpl_search_page page-template-page-tpl_search_page-php page page-id-342">
    <section class="navigation">
            <div class="container">
                <nav class="navbar navbar-default" role="navigation">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <a href="{{url('/ad/post-ad')}}"  class="btn submit-add visible-xs">Post Your Ad</a>
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        </button>
                    </div>
                    <a href="{{url('/')}}" class="navbar-brand">
                    <img src="{{asset('assets/front/uploads/2017/07/logo-1.png')}}" title="" alt=""
                        width="488"
                        height="488">
                    </a>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="navbar-collapse-1">
                        <!-- <ul class="nav navbar-nav navbar-left">
                            <li><a href="#">Menu</a></li>
                            <li><a href="#">Menu</a></li>
                            </ul>
                            -->
                        <ul class="nav navbar-nav navbar-left">
                            <li id="menu-item-3376" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3376"><a href="{{url('/ad/all-ads')}}">All Ads</a></li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="#login" data-toggle="modal" class="login-action">Sign In</a></li>
                            <li><a href="#register-new" data-toggle="modal" class="login-action">Register</a></li>
                            <li class="submit-add hidden-xs"><a href="{{url('/ad/post-ad')}}"  class="btn">Post Your Ad</a></li>
                        </ul>
                    </div>
                    <!-- /.navbar-collapse -->
                </nav>
            </div>
        </section>
    <section class="search-bar clearfix">
        <div class="container">
            <form method="get" class="search-form advanced-search" action="{{url('ad/search')}}">
                <input type="hidden" class="view" name="view" value="grid">
                <input type="hidden" class="sortby" name="sortby" value="{{$sortby}}">
                <ul class="list-unstyled list-inline">
                    <li>
                        <input type="text" class="form-control keyword" name="keyword" value="{{$keyword}}" placeholder="Find by keyword or short code">
                        <i class="fa fa-search"></i>
                    </li>
                    <li>
                        <select class="form-control select2 location" name="location" data-placeholder="Location">
                            <option value="">&nbsp;</option>
                            @foreach ($location_select_options as $option)
                                @if ($option->district_id)
                                    <option value="{{$option->id}}" class="subitem" @if ($location == $option->id) selected="selected" @endif>&nbsp;&nbsp; {{$option->name}}</option>
                                @else
                                    <option value="{{$option->id}}" @if ($location == $option->id) selected="selected" @endif> {{$option->name}}</option>
                                @endif
                            @endforeach

                        </select>
                        <i class="fa fa-bars"></i>
                    </li>
                    <li>
                        <select class="form-control select2 category" name="category" data-placeholder="Category">
                            <option value="">&nbsp;</option>
                            @foreach ($category_select_options as $option)
                                @if ($option->main_category_id)
                                    <option value="{{$option->id}}" class="subitem" @if ($category == $option->id) selected="selected" @endif>&nbsp;&nbsp; {{$option->name}}</option>
                                @else
                                    <option value="{{$option->id}}" @if ($category == $option->id) selected="selected" @endif> {{$option->name}}</option>
                                @endif
                            @endforeach
                            
                        </select>
                        <i class="fa fa-bars"></i>
                    </li>
                    <!-- <li>
                            <select class="form-control select2 radius" name="radius" data-placeholder="Radius (300km)">
                                <option value="">&nbsp;</option>
                                                        </select>
                            <i class="fa fa-bullseye"></i>
                            </li> -->
                    <li>
                        <a href="javascript:;" class="btn submit-form">
                            Search </a>
                    </li>
                </ul>
                <div class="filters-holder hidden"></div>
            </form>
        </div>
    </section>


            <div class="modal fade in" id="filters" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
                        <div class="filters-modal-holder">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fadein" id="payUAdditional" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content showCode-content">
                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
                        <div class="payu-content-modal">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        
        <!--[if lte IE 8]>
        <style>
            .attachment:focus {
            outline: #1e8cbe solid;
            }
            .selected.attachment {
            outline: #1e8cbe solid;
            }
        </style>
        <![endif]-->
        <script type="text/html" id="tmpl-media-frame">
            <div class="media-frame-menu"></div>
            <div class="media-frame-title"></div>
            <div class="media-frame-router"></div>
            <div class="media-frame-content"></div>
            <div class="media-frame-toolbar"></div>
            <div class="media-frame-uploader"></div>
        </script>
        <script type="text/html" id="tmpl-media-modal">
            <div class="media-modal wp-core-ui">
                <button type="button" class="button-link media-modal-close"><span class="media-modal-icon"><span class="screen-reader-text">Close media panel</span></span></button>
                <div class="media-modal-content"></div>
            </div>
            <div class="media-modal-backdrop"></div>
        </script>
        <script type="text/html" id="tmpl-uploader-window">
            <div class="uploader-window-content">
                <h1>Drop files to upload</h1>
            </div>
        </script>
        <script type="text/html" id="tmpl-uploader-editor">
            <div class="uploader-editor-content">
                <div class="uploader-editor-title">Drop files to upload</div>
            </div>
        </script>


                    <!-- header-two.php 541 - 570 -->


        <script type="text/html" id="tmpl-media-library-view-switcher">
            <a href="/?mode=list" class="view-list">
                <span class="screen-reader-text">List View</span>
            </a>
            <a href="/?mode=grid" class="view-grid current">
                <span class="screen-reader-text">Grid View</span>
            </a>
        </script>
        <script type="text/html" id="tmpl-uploader-status">
            <h2>Uploading</h2>
            <button type="button" class="button-link upload-dismiss-errors"><span class="screen-reader-text">Dismiss Errors</span></button>
            
            <div class="media-progress-bar"><div></div></div>
            <div class="upload-details">
                <span class="upload-count">
                    <span class="upload-index"></span> / <span class="upload-total"></span>
                </span>
                <span class="upload-detail-separator">&ndash;</span>
                <span class="upload-filename"></span>
            </div>
            <div class="upload-errors"></div>
        </script>



                    <!-- header-two.php 593 - 596 -->


        <script type="text/html" id="tmpl-edit-attachment-frame">
            <div class="edit-media-header">
                <button class="left dashicons <# if ( ! data.hasPrevious ) { #> disabled <# } #>"><span class="screen-reader-text">Edit previous media item</span></button>
                <button class="right dashicons <# if ( ! data.hasNext ) { #> disabled <# } #>"><span class="screen-reader-text">Edit next media item</span></button>
            </div>
            <div class="media-frame-title"></div>
            <div class="media-frame-content"></div>
        </script>



                    <!-- header-two.php 605 - 749 -->
                    <!-- header-two.php 650 - 798 -->
                    <!-- header-two.php 799 - 880 -->


        <script type="text/html" id="tmpl-media-selection">
            <div class="selection-info">
                <span class="count"></span>
                <# if ( data.editable ) { #>
                    <button type="button" class="button-link edit-selection">Edit Selection</button>
                <# } #>
                <# if ( data.clearable ) { #>
                    <button type="button" class="button-link clear-selection">Clear</button>
                <# } #>
            </div>
            <div class="selection-view"></div>
        </script>



                    <!-- header-two.php 893 - 997 -->


        <script type="text/html" id="tmpl-gallery-settings">
            <h2>Gallery Settings</h2>
            
            <label class="setting">
                <span>Link To</span>
                <select class="link-to"
                    data-setting="link"
                    <# if ( data.userSettings ) { #>
                        data-user-setting="urlbutton"
                    <# } #>>
            
                    <option value="post" <# if ( ! wp.media.galleryDefaults.link || 'post' == wp.media.galleryDefaults.link ) {
                        #>selected="selected"<# }
                    #>>
                        Attachment Page             </option>
                    <option value="file" <# if ( 'file' == wp.media.galleryDefaults.link ) { #>selected="selected"<# } #>>
                        Media File              </option>
                    <option value="none" <# if ( 'none' == wp.media.galleryDefaults.link ) { #>selected="selected"<# } #>>
                        None                </option>
                </select>
            </label>
            
            <label class="setting">
                <span>Columns</span>
                <select class="columns" name="columns"
                    data-setting="columns">
                                        <option value="1" <#
                            if ( 1 == wp.media.galleryDefaults.columns ) { #>selected="selected"<# }
                        #>>
                            1                   </option>
                                        <option value="2" <#
                            if ( 2 == wp.media.galleryDefaults.columns ) { #>selected="selected"<# }
                        #>>
                            2                   </option>
                                        <option value="3" <#
                            if ( 3 == wp.media.galleryDefaults.columns ) { #>selected="selected"<# }
                        #>>
                            3                   </option>
                                        <option value="4" <#
                            if ( 4 == wp.media.galleryDefaults.columns ) { #>selected="selected"<# }
                        #>>
                            4                   </option>
                                        <option value="5" <#
                            if ( 5 == wp.media.galleryDefaults.columns ) { #>selected="selected"<# }
                        #>>
                            5                   </option>
                                        <option value="6" <#
                            if ( 6 == wp.media.galleryDefaults.columns ) { #>selected="selected"<# }
                        #>>
                            6                   </option>
                                        <option value="7" <#
                            if ( 7 == wp.media.galleryDefaults.columns ) { #>selected="selected"<# }
                        #>>
                            7                   </option>
                                        <option value="8" <#
                            if ( 8 == wp.media.galleryDefaults.columns ) { #>selected="selected"<# }
                        #>>
                            8                   </option>
                                        <option value="9" <#
                            if ( 9 == wp.media.galleryDefaults.columns ) { #>selected="selected"<# }
                        #>>
                            9                   </option>
                                </select>
            </label>
            
            <label class="setting">
                <span>Random Order</span>
                <input type="checkbox" data-setting="_orderbyRandom" />
            </label>
            
            <label class="setting size">
                <span>Size</span>
                <select class="size" name="size"
                    data-setting="size"
                    <# if ( data.userSettings ) { #>
                        data-user-setting="imgsize"
                    <# } #>
                    >
                                        <option value="thumbnail">
                            Thumbnail                   </option>
                                        <option value="medium">
                            Medium                  </option>
                                        <option value="large">
                            Large                   </option>
                                        <option value="full">
                            Full Size                   </option>
                                </select>
            </label>
        </script>
        <script type="text/html" id="tmpl-playlist-settings">
            <h2>Playlist Settings</h2>
            
            <# var emptyModel = _.isEmpty( data.model ),
                isVideo = 'video' === data.controller.get('library').props.get('type'); #>
            
            <label class="setting">
                <input type="checkbox" data-setting="tracklist" <# if ( emptyModel ) { #>
                    checked="checked"
                <# } #> />
                <# if ( isVideo ) { #>
                <span>Show Video List</span>
                <# } else { #>
                <span>Show Tracklist</span>
                <# } #>
            </label>
            
            <# if ( ! isVideo ) { #>
            <label class="setting">
                <input type="checkbox" data-setting="artists" <# if ( emptyModel ) { #>
                    checked="checked"
                <# } #> />
                <span>Show Artist Name in Tracklist</span>
            </label>
            <# } #>
            
            <label class="setting">
                <input type="checkbox" data-setting="images" <# if ( emptyModel ) { #>
                    checked="checked"
                <# } #> />
                <span>Show Images</span>
            </label>
        </script>
        <script type="text/html" id="tmpl-embed-link-settings">
            <label class="setting link-text">
                <span>Link Text</span>
                <input type="text" class="alignment" data-setting="linkText" />
            </label>
            <div class="embed-container" style="display: none;">
                <div class="embed-preview"></div>
            </div>
        </script>



                    <!-- header-two.php 1129 - 1170 -->
                    <!-- header-two.php 1171 - 1309 -->
                    <!-- header-two.php 1310 - 1313 -->
                    <!-- header-two.php 1314 - 1456 -->
                    <!-- header-two.php 1457 - 1697 -->
                    <!-- header-two.php 1698 - 1726 -->
                    <!-- header-two.php 1727 - 1730 -->
                    <!-- header-two.php 1731 - 1747 -->



        <script type='text/javascript' src="{{asset('assets/front/js/underscore.min4511.js?ver=1.8.3')}}"></script>
        <script type='text/javascript' src="{{asset('assets/front/js/backbone.min9632.js?ver=1.2.3')}}"></script>
        <script type='text/javascript'>
            /* <![CDATA[ */
            var wpApiSettings = {"root":"http:\/\/www.sambole.lk\/assets/json\/","nonce":"6481a30370","versionString":"wp\/v2\/"};
            var WP_API_Settings = {"root":"http:\/\/www.sambole.lk\/assets/json\/","nonce":"6481a30370","title":"Media Title","description":"Media Description","alt_text":"Media Alt Text","caption":"Media Caption"};
            /* ]]> */
        </script>
        <script type='text/javascript' src="{{asset('assets/front/js/wp-api.mine100.js?ver=4.7.2')}}"></script>
        <script type='text/javascript' src='http://maps.googleapis.com/maps/api/js?key=AIzaSyDTkWWz97piXi43nvxfdFkdppZz3aeY7rc&amp;libraries=places&amp;ver=4.7.2'></script>
        <script type='text/javascript' src="{{asset('assets/front/js/bootstrap.mine100.js?ver=4.7.2')}}"></script>
        <script type='text/javascript' src="{{asset('assets/front/js/bootstrap-dropdown-multilevele100.js?ver=4.7.2')}}"></script>
        <script type='text/javascript' src="{{asset('assets/front/js/shortcode.mine100.js?ver=4.7.2')}}"></script>
        <script type='text/javascript'>
            /* <![CDATA[ */
            var _wpUtilSettings = {"ajax":{"url":"\/wp-admin\/admin-ajax.php"}};
            /* ]]> */
        </script>
        <script type='text/javascript' src="{{asset('assets/front/js/wp-util.mine100.js?ver=4.7.2')}}"></script>
        <script type='text/javascript' src="{{asset('assets/front/js/wp-backbone.mine100.js?ver=4.7.2')}}"></script>
        <script type='text/javascript'>
            /* <![CDATA[ */
            var _wpMediaModelsL10n = {"settings":{"ajaxurl":"\/wp-admin\/admin-ajax.php","post":{"id":0}}};
            /* ]]> */
        </script>
        <script type='text/javascript' src="{{asset('assets/front/js/media-models.mine100.js?ver=4.7.2')}}"></script>
        <script type='text/javascript'>
            /* <![CDATA[ */
            var pluploadL10n = {"queue_limit_exceeded":"You have attempted to queue too many files.","file_exceeds_size_limit":"%s exceeds the maximum upload size for this site.","zero_byte_file":"This file is empty. Please try another.","invalid_filetype":"This file type is not allowed. Please try another.","not_an_image":"This file is not an image. Please try another.","image_memory_exceeded":"Memory exceeded. Please try another smaller file.","image_dimensions_exceeded":"This is larger than the maximum size. Please try another.","default_error":"An error occurred in the upload. Please try again later.","missing_upload_url":"There was a configuration error. Please contact the server administrator.","upload_limit_exceeded":"You may only upload 1 file.","http_error":"HTTP error.","upload_failed":"Upload failed.","big_upload_failed":"Please try uploading this file with the %1$sbrowser uploader%2$s.","big_upload_queued":"%s exceeds the maximum upload size for the multi-file uploader when used in your browser.","io_error":"IO error.","security_error":"Security error.","file_cancelled":"File canceled.","upload_stopped":"Upload stopped.","dismiss":"Dismiss","crunching":"Crunching\u2026","deleted":"moved to the trash.","error_uploading":"\u201c%s\u201d has failed to upload."};
            var _wpPluploadSettings = {"defaults":{"runtimes":"html5,flash,silverlight,html4","file_data_name":"async-upload","url":"\/wp-admin\/async-upload.php","flash_swf_url":"http:\/\/www.sambole.lk\/wp-includes\/js\/plupload\/plupload.flash.swf","silverlight_xap_url":"http:\/\/www.sambole.lk\/wp-includes\/js\/plupload\/plupload.silverlight.xap","filters":{"max_file_size":"67108864b","mime_types":[{"extensions":"jpg,jpeg,jpe,gif,png,bmp,tiff,tif,ico,asf,asx,wmv,wmx,wm,avi,divx,flv,mov,qt,mpeg,mpg,mpe,mp4,m4v,ogv,webm,mkv,3gp,3gpp,3g2,3gp2,txt,asc,c,cc,h,srt,csv,tsv,ics,rtx,css,vtt,dfxp,mp3,m4a,m4b,ra,ram,wav,ogg,oga,mid,midi,wma,wax,mka,rtf,js,pdf,class,tar,zip,gz,gzip,rar,7z,psd,xcf,doc,pot,pps,ppt,wri,xla,xls,xlt,xlw,mdb,mpp,docx,docm,dotx,dotm,xlsx,xlsm,xlsb,xltx,xltm,xlam,pptx,pptm,ppsx,ppsm,potx,potm,ppam,sldx,sldm,onetoc,onetoc2,onetmp,onepkg,oxps,xps,odt,odp,ods,odg,odc,odb,odf,wp,wpd,key,numbers,pages,redux"}]},"multipart_params":{"action":"upload-attachment","_wpnonce":"002e5e128a"}},"browser":{"mobile":false,"supported":true},"limitExceeded":false};
            /* ]]> */
        </script>
        <script type='text/javascript' src="{{asset('assets/front/js/plupload/wp-plupload.mine100.js?ver=4.7.2')}}"></script>
        <script type='text/javascript' src="{{asset('assets/front/js/jquery/ui/core.mine899.js?ver=1.11.4')}}"></script>
        <script type='text/javascript' src="{{asset('assets/front/js/jquery/ui/widget.mine899.js?ver=1.11.4')}}"></script>
        <script type='text/javascript' src="{{asset('assets/front/js/jquery/ui/mouse.mine899.js?ver=1.11.4')}}"></script>
        <script type='text/javascript' src="{{asset('assets/front/js/jquery/ui/sortable.mine899.js?ver=1.11.4')}}"></script>
        <script type='text/javascript'>
            /* <![CDATA[ */
            var mejsL10n = {"language":"en-US","strings":{"Close":"Close","Fullscreen":"Fullscreen","Turn off Fullscreen":"Turn off Fullscreen","Go Fullscreen":"Go Fullscreen","Download File":"Download File","Download Video":"Download Video","Play":"Play","Pause":"Pause","Captions\/Subtitles":"Captions\/Subtitles","None":"None","Time Slider":"Time Slider","Skip back %1 seconds":"Skip back %1 seconds","Video Player":"Video Player","Audio Player":"Audio Player","Volume Slider":"Volume Slider","Mute Toggle":"Mute Toggle","Unmute":"Unmute","Mute":"Mute","Use Up\/Down Arrow keys to increase or decrease volume.":"Use Up\/Down Arrow keys to increase or decrease volume.","Use Left\/Right Arrow keys to advance one second, Up\/Down arrows to advance ten seconds.":"Use Left\/Right Arrow keys to advance one second, Up\/Down arrows to advance ten seconds."}};
            var _wpmejsSettings = {"pluginPath":"\/wp-includes\/js\/mediaelement\/"};
            /* ]]> */
        </script>
        <script type='text/javascript' src="{{asset('assets/front/js/mediaelement/mediaelement-and-player.min51cd.js?ver=2.22.0')}}"></script>
        <script type='text/javascript' src="{{asset('assets/front/js/mediaelement/wp-mediaelement.mine100.js?ver=4.7.2')}}"></script>
        <script type='text/javascript'>
            /* <![CDATA[ */
            var _wpMediaViewsL10n = {"url":"URL","addMedia":"Add Media","search":"Search","select":"Select","cancel":"Cancel","update":"Update","replace":"Replace","remove":"Remove","back":"Back","selected":"%d selected","dragInfo":"Drag and drop to reorder media files.","uploadFilesTitle":"Upload Files","uploadImagesTitle":"Upload Images","mediaLibraryTitle":"Media Library","insertMediaTitle":"Insert Media","createNewGallery":"Create a new gallery","createNewPlaylist":"Create a new playlist","createNewVideoPlaylist":"Create a new video playlist","returnToLibrary":"\u2190 Return to library","allMediaItems":"All media items","allDates":"All dates","noItemsFound":"No items found.","insertIntoPost":"Insert into post","unattached":"Unattached","trash":"Trash","uploadedToThisPost":"Uploaded to this post","warnDelete":"You are about to permanently delete this item.\n  'Cancel' to stop, 'OK' to delete.","warnBulkDelete":"You are about to permanently delete these items.\n  'Cancel' to stop, 'OK' to delete.","warnBulkTrash":"You are about to trash these items.\n  'Cancel' to stop, 'OK' to delete.","bulkSelect":"Bulk Select","cancelSelection":"Cancel Selection","trashSelected":"Trash Selected","untrashSelected":"Untrash Selected","deleteSelected":"Delete Selected","deletePermanently":"Delete Permanently","apply":"Apply","filterByDate":"Filter by date","filterByType":"Filter by type","searchMediaLabel":"Search Media","searchMediaPlaceholder":"Search media items...","noMedia":"No media files found.","attachmentDetails":"Attachment Details","insertFromUrlTitle":"Insert from URL","setFeaturedImageTitle":"Featured Image","setFeaturedImage":"Set featured image","createGalleryTitle":"Create Gallery","editGalleryTitle":"Edit Gallery","cancelGalleryTitle":"\u2190 Cancel Gallery","insertGallery":"Insert gallery","updateGallery":"Update gallery","addToGallery":"Add to gallery","addToGalleryTitle":"Add to Gallery","reverseOrder":"Reverse order","imageDetailsTitle":"Image Details","imageReplaceTitle":"Replace Image","imageDetailsCancel":"Cancel Edit","editImage":"Edit Image","chooseImage":"Choose Image","selectAndCrop":"Select and Crop","skipCropping":"Skip Cropping","cropImage":"Crop Image","cropYourImage":"Crop your image","cropping":"Cropping\u2026","suggestedDimensions":"Suggested image dimensions:","cropError":"There has been an error cropping your image.","audioDetailsTitle":"Audio Details","audioReplaceTitle":"Replace Audio","audioAddSourceTitle":"Add Audio Source","audioDetailsCancel":"Cancel Edit","videoDetailsTitle":"Video Details","videoReplaceTitle":"Replace Video","videoAddSourceTitle":"Add Video Source","videoDetailsCancel":"Cancel Edit","videoSelectPosterImageTitle":"Select Poster Image","videoAddTrackTitle":"Add Subtitles","playlistDragInfo":"Drag and drop to reorder tracks.","createPlaylistTitle":"Create Audio Playlist","editPlaylistTitle":"Edit Audio Playlist","cancelPlaylistTitle":"\u2190 Cancel Audio Playlist","insertPlaylist":"Insert audio playlist","updatePlaylist":"Update audio playlist","addToPlaylist":"Add to audio playlist","addToPlaylistTitle":"Add to Audio Playlist","videoPlaylistDragInfo":"Drag and drop to reorder videos.","createVideoPlaylistTitle":"Create Video Playlist","editVideoPlaylistTitle":"Edit Video Playlist","cancelVideoPlaylistTitle":"\u2190 Cancel Video Playlist","insertVideoPlaylist":"Insert video playlist","updateVideoPlaylist":"Update video playlist","addToVideoPlaylist":"Add to video playlist","addToVideoPlaylistTitle":"Add to Video Playlist","settings":{"tabs":[],"tabUrl":"http:\/\/www.sambole.lk\/wp-admin\/media-upload.php?chromeless=1","mimeTypes":{"image":"Images","audio":"Audio","video":"Video"},"captions":true,"nonce":{"sendToEditor":"0dc6239036"},"post":{"id":3274},"defaultProps":{"link":"none","align":"","size":""},"attachmentCounts":{"audio":0,"video":0},"embedExts":["mp3","ogg","wma","m4a","wav","mp4","m4v","webm","ogv","wmv","flv"],"embedMimes":{"mp3":"audio\/mpeg","ogg":"audio\/ogg","wma":"audio\/x-ms-wma","m4a":"audio\/mpeg","wav":"audio\/wav","mp4":"video\/mp4","m4v":"video\/mp4","webm":"video\/webm","ogv":"video\/ogg","wmv":"video\/x-ms-wmv","flv":"video\/x-flv"},"contentWidth":1920,"months":[{"year":"2018","month":"2","text":"February 2018"},{"year":"2018","month":"1","text":"January 2018"},{"year":"2017","month":"12","text":"December 2017"},{"year":"2017","month":"11","text":"November 2017"},{"year":"2017","month":"10","text":"October 2017"},{"year":"2017","month":"9","text":"September 2017"},{"year":"2017","month":"8","text":"August 2017"},{"year":"2017","month":"7","text":"July 2017"},{"year":"2017","month":"6","text":"June 2017"},{"year":"2017","month":"5","text":"May 2017"},{"year":"2017","month":"4","text":"April 2017"},{"year":"2017","month":"3","text":"March 2017"},{"year":"2017","month":"2","text":"February 2017"},{"year":"2017","month":"1","text":"January 2017"}],"mediaTrash":0}};
            /* ]]> */
        </script>
        <script type='text/javascript' src="{{asset('assets/front/js/media-views.mine100.js?ver=4.7.2')}}"></script>
        <script type='text/javascript' src="{{asset('assets/front/js/media-editor.mine100.js?ver=4.7.2')}}"></script>
        <script type='text/javascript' src="{{asset('assets/front/js/media-audiovideo.mine100.js?ver=4.7.2')}}"></script>
        <!-- <script type='text/javascript' src="{{asset('assets/front/js/image-uploadere100.js?ver=4.7.2')}}"></script> -->
        <script type='text/javascript' src="{{asset('assets/front/js/gmape100.js?ver=4.7.2')}}"></script>
        <script type='text/javascript' src="{{asset('assets/front/js/custom-fieldse100.js?ver=4.7.2')}}"></script>
        <script type='text/javascript' src="{{asset('assets/front/js/markerclusterer_compilede100.js?ver=4.7.2')}}"></script>
        <script type='text/javascript' src="{{asset('assets/front/js/infoboxe100.js?ver=4.7.2')}}"></script>
        <script type='text/javascript' src="{{asset('assets/front/js/comment-reply.mine100.js?ver=4.7.2')}}"></script>
        <script type='text/javascript' src="{{asset('assets/front/js/owl.carousel.mine100.js?ver=4.7.2')}}"></script>
        <script type='text/javascript' src="{{asset('assets/front/js/responsiveslides.mine100.js?ver=4.7.2')}}"></script>
        <script type='text/javascript' src="{{asset('assets/front/js/select2/select2.mine100.js?ver=4.7.2')}}"></script>
        <script type='text/javascript' src="{{asset('assets/front/js/imagesloadede100.js?ver=4.7.2')}}"></script>
        <script type='text/javascript' src="{{asset('assets/front/js/masonrye100.js?ver=4.7.2')}}"></script>
        <script type='text/javascript' src="{{asset('assets/front/js/cachee100.js?ver=4.7.2')}}"></script>
        <script type='text/javascript' src="{{asset('assets/front/js/jquery.magnific-popup.mine100.js?ver=4.7.2')}}"></script>
        <script type='text/javascript'>
            /* <![CDATA[ */
            var classifieds_data = {"url":"http:\/\/www.sambole.lk\/assets\/themes\/sambole","home_map_geolocation":"no","home_map_geo_zoom":"","contact_map_zoom":"5","restrict_country":"LK","home_map_zoom":"","map_price":"","empty_search_location":"","ads_max_videos":"10","ads_max_images":"10"};
            /* ]]> */
        </script>
        <script type='text/javascript' src="{{asset('assets/front/js/custome100.js?ver=4.7.2')}}"></script>
        <script type='text/javascript' src="{{asset('assets/front/js/wp-embed.mine100.js?ver=4.7.2')}}"></script>
    @yield('content')


<style type="text/css">
    .topic_login{
                text-align: center;
            }
            .submit-form-ajax1{
                width: 90%;
                background-color: #712b2f;
                margin-left: 5%;
                margin-bottom: 30px;
            }
            .submit-form-ajax1:hover{
                background-color:#cccccc;
                color: #712b2f;
            }
            .register-close-login1{
                width: 100%;
                margin-left: 20px;
            }
            .or{
                font-style: italic;
                color: #b8b894;
                margin-left: 10px;
                margin-top: 20px;
                text-align: center;
            }
            .login-remember{
                color: #333333;
            }
            .login-remember:hover{
                color: #a6a6a6;
            }
            .forgot-password1 {
                color:  #333333;
            }
            .forgot-password1:hover{
                color: #a6a6a6;
            }
            .register-close-login2{
                background-color: #ffad33;
                color: white;
                width: 95%;
                margin-left: 2%;
                margin-top: 20px;
            }
            .register-close-login2:hover{
                background-color: #cccccc;
                color:  #ffad33;
            }

            #btn1
            {
                text-align: center;
            }

            #submit
            {
                
               background-color: #712b2f; 
               margin-right: 10px;
            }

            #submit:hover
            {
                 background-color: #D2691E;
                        
            }

            #reset:hover
            {
                
                background-color: #D2691E;
            }

            .button-style1 button {
                font-size:20px;
                /*margin:0 20px 0 20px;*/
                width: 60%;
                border-radius: 5px;
                border: none;
                color: white;
                padding: 15px 32px;
                text-align: center;
                text-decoration: none;
                display: inline-block;
                font-size: 16px;
                margin: 4px 2px ;
                cursor: pointer;
            }

            .button-style1
            {
                text-align: center;
            }

            .member-reg {
                background-color: #4CAF50;
            }
            .business-reg {
                background-color: #ff9933;

            }
            .member-reg:hover{
                background-color: #eaeae1;
                color:#4CAF50;
            }
            .business-reg:hover {
                background-color: #eaeae1;
                color:#ff9933;
            }
           
            #h5{
                color: #737373;
            }
            .modal-content{
                width: 100%;
            }


            @media(min-width: 320px) and (max-width: 359px){
                
                 /************* User Type *****************/

                .topic {
                    font-size: 13px;
                }

                #h5 {
                    font-size: 12px;
                }

                .button-style1 button {
                    padding: 15px 10px;
                    font-size: 11px;
                    width: 100%;
                }

                .modal-content {
                    padding: 20px 10px;
                }

                .member-reg {
                    font-size: 12px;
                }

            }

            /************* Contact Popup Model *****************/

                 .contact {
                    font-size: 13px;
                 }

                 .label1 {
                    font-size: 11px;
                 }

                 #submit {
                    font-size: 10px;
                 }

                 #reset {
                    font-size: 10px;
                 }

            }


            @media(min-width: 360px) and (max-width: 767px){

                /************* User Type *****************/

                .topic {
                    font-size: 18px;
                }

                #h5 {
                    font-size: 16px;
                }

                .button-style1 button {
                    padding: 17px 20px;
                    font-size: 16px;}

            @media(max-width: 320px) and (max-height: 480px){
                .forgetPass-login {
                    text-align: left;
                }

                .rememberMe-login {
                    text-align: left;
                }

                /************* User Type *****************/

                .topic {
                    font-size: 22px;
                }

                #h5 {
                    font-size: 15px;
                }

                .button-style1 button {
                    padding: 15px 10px;
                    font-size: 15px;

                    width: 100%;
                }

                .modal-content {
                    padding: 30px 10px;
                }



                 /************* Contact Popup Model *****************/

                 .contact {
                    font-size: 28px;
                 }

                 .label1 {
                    font-size: 19px;
                 }

                 #submit {
                    font-size: 17px;
                 }

                 #reset {
                    font-size: 17px;
                 }

            }

            @media (min-width: 768px) and (max-width: 979px){

            }

            @media(min-width: 360px) and (max-width: 640px){


                /************* User Type *****************/

                .topic {

                    font-size: 22px;
                }

                #h5 {
                    font-size: 20px;

                    font-size: 24px;
                }

                #h5 {
                    font-size: 17px;

                }

                .button-style1 button {
                    padding: 17px 20px;

                    font-size: 19px;

                    font-size: 16px;

                    width: 100%;
                }

                .modal-content {
                    padding: 30px 10px;
                }




                 /************* Contact Popup Model *****************/

                 .contact {
                    font-size: 22px;
                 }

                 .label1 {
                    font-size: 20px;
                 }

                 #submit {
                    font-size: 16px;
                 }

                 #reset {
                    font-size: 16px;
                 }

            }


                 @media(min-width: 980px) and (max-width: 1279px){


                /************* Contact Popup Model *****************/

                 .contact {
                    font-size: 28px;
                 }

                 .label1 {
                    font-size: 26px;
                 }

                 #submit {
                    font-size: 22px;
                 }

                 #reset {
                    font-size: 22px;
                 }

                 input .one {
                    width: 100px;
                 }

           

                /************* User Type *****************/

                .topic {
                    font-size: 26px;
                }

                #h5 {
                    font-size: 24px;
                }

                .button-style1 button {
                    padding: 17px 20px !important;
                    font-size: 28px;
                    width: 100% !important;
                }

                .modal-content {
                    padding: 30px 10px !important;
                }          

 }

         @media(min-width: 768px) and (max-width: 980px){

            .model-dialog {
                width:700px;
                margin: 30px auto;
            }

            .button-style1 button {
                width: 80%;
            }
         }

           /* @media(min-width: 1280px) and (max-width: 1399px){*/


                /************* Contact Popup Model *****************/

               /*  .contact {
                    font-size: 30px;
                 }

                 .label1 {
                    font-size: 26px;
                 }

                 #submit {
                    font-size: 24px;
                    padding: 10px 10px 40px 12px;
                    text-align:center;
                 }

                 #reset {
                    font-size: 24px;
                    padding: 10px 10px 40px 12px;
                    text-align:center;
                 }
           */

   
</style> 


            



            
    
</style> 


    <div class="footer-social">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <a href="{{url('/')}}" class="logo">
                        <img src="{{asset('assets/front/uploads/2017/07/logo-1.png')}}" title="" alt=""
                            width="488"
                            height="488">
                        </a>
                    </div>
                    <div class="col-md-6">
                        <ul class="social-links list-inline">
                            <li><a href="https://www.facebook.com/sambole.lk/" target="_blank"><i class="fa fa-facebook"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>


        <section class="widget-footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="widget white-block widget_text">
                            <h4>About Us</h4>
                            <div class="textwidget">
                                <p>Sambole.lk enables you to search or buy anything from used/new Cars, Mobiles, Furniture and much more Products & Services. Adverting is completely FREE for all categories. Log on to <a href="{{url('/')}}"><u>sambole.lk</u></a> to start buying and selling. </p>
                                <p>We help, build and connect communities.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="widget white-block widget_text">
                            <h4>Get in Touch</h4>
                            <div class="textwidget">
                                <ul class="list-unstyled">
                                    <li><i class="fa fa-phone"></i> 011 4329753-8</li>
                                    <li><i class="fa fa-envelope"></i> <a href="#">info@sambole.lk</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <h4>Helpful Links</h4>
                        <div class="menu-footer-menu-container">
                            <ul id="menu-footer-menu" class="menu">

                                <li id="menu-item-3585" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3585"><a href="{{url('/termsOfService')}}">Terms &#038; Conditions</a></li>
                                <li id="menu-item-3586" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3586"><a href="{{url('/support')}}">Help &#038; Support</a></li>

                                <li id="menu-item-3585" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3585"><a href="termsOfService/index.html">Terms &#038; Conditions</a></li>
                                <li id="menu-item-3586" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3586"><a href="full-width-page/index.html">Help &#038; Support</a></li>

                            </ul>
                        </div>
                        <ul>
                            <li><a href="javascript:;" class="feedback" data-toggle="modal" data-target="#feedback">Contact</a></li>
                            <li><a href="javascript:;" class="feedback" data-toggle="modal" data-target="#register-type">Register Type</a></li>
                        </ul>
                    </div>
                    <!-- <div class="col-md-3">
                        <h4>Subscribe to Newsletter</h4>
                        
                        <form class="form-vertical">
                            <input type="text" name="email">
                            <button class="btn">Submit</button>
                        </form>
                        </div> -->
                </div>
            </div>
        </section>
        <div class="footer-copy">Copyright &copy; <strong>SAMBOLE.LK</strong> 2017 - 2018. All right reserved. version: VER1.2.1T</div>



        <div class="modal fade in" id="register-type">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
        <h2 class="topic">Register</h2>
        <h5 id="h5">Please select user type</h5>
      </div>
      <div class="modal-body">           
            <!-- <div class="ajax-response"></div> -->
            <div class="button-style1">
                <a href="{{url('/post-ad')}}">
                <button id="member-reg" class="member-reg">
                    <i class="fa fa-user"></i> &nbsp; Register as a Ad user
                </button></a>
                <a href="{{url('/businessReg')}}">
                <button id="business-reg" class="business-reg">
                    <i class="fa fa-users"></i>  &nbsp; Register as a business user
                </button></a>
            </div>
        </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->  
  
  
  
  <!-- #modal 2 -->
<div class="modal fade in" id="register-new">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
            <h4 class="regi">REGISTER</h4>
            <div class="ajax-response"></div>
            <form class="form-register">
                <div class="row">
                    <div class="col-md-12">
                        <input type="text" name="register-name" id="register-name1" class="form-control" reqiured placeholder="First Name" />
                    </div>
                     <div class="col-md-12">
                        <input type="text" name="register-name" id="register-name2" class="form-control" reqiured placeholder="Last Name" />
                    </div>
                    <div class="col-md-12">
                        <input type="text" name="register-email" id="register-email" class="form-control" reqiured placeholder="Email"/>
                    </div>
                    <div class="col-md-12">
                        <input type="text" name="register-phone" id="register-phone" class="form-control" reqiured placeholder="Contact Number"/>
                    </div>
                    <div class="col-md-12">
                        <input type="password" name="register-password" id="regi-password" class="form-control" reqiured placeholder="Password"/>
                    </div>
                    <div class="col-md-12">
                        <input type="password" name="register-password-repeat" id="regi-password-repeat" class="form-control" reqiured placeholder="Confirm Password"/>
                    </div>
                   
                </div>
                    
                <a href="#register-type" data-toggle="modal" data-dismiss="modal" class="login-action btn submit-form-ajax">Submit</a>
                or                  
                <a href="javascript:;" class="register-close-login" data-dismiss="modal">login here</a>
                <input type="hidden" name="action" value="register">
            </form>
            
        </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal --> 


        <!-- modal -->
        <div class="modal fade in" id="login" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
                        <h3 class="topic_login">Login</h3>
                        <div class="ajax-response"></div>
                        <form class="form-login">
                            <label for="login-email">Email</label>
                            <input type="text" name="login-email" id="login-email" class="form-control" placeholder="Email"/>
                            <label for="login-password">Password</label>
                            <input type="password" name="login-password" id="login-password" class="form-control"/ placeholder="Password">
                            <div class="row">
                                    <a href="admin/index.html" class="btn submit-form-ajax1 submit-btn">LOGIN</a>
                                    
                                    <input type="hidden" name="action" value="login">
                                    <div class="col-md-12">
                                    <div class="col-md-6 rememberMe-login">
                                        <div class="checkbox checkbox-inline">
                                            <input type="checkbox" name="login-remember" id="login-remember"/>
                                            <label for="login-remember" class="login-remember">Remember me</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 text-right forgetPass-login">
                                        <a href="javascript:;" class="forgot-password1" data-dismiss="modal">Forgot Password?</a>
                                    </div>
                                    </div>
                                    <div class="col-md-12">
                                         <p class="or">Don't you have an account yet?</p>
                                    </div>
                                    <div class="col-md-12">
                                        <!-- <a href="javascript:;" class="register-close-login1" 
                                         data-dismiss="modal">Register here</a>  --> 
                                         <a href="#register-new" data-toggle="modal" data-dismiss="modal" class="login-action btn register-close-login2">REGISTER</a>
                                    </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        

        
        <!-- .modal -->
        <div class="modal fade in" id="recover" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
                        <h4>Recover Password &amp; Username</h4>
                        <div class="ajax-response"></div>
                        <form class="form-login">
                            <label for="recover-email">Email</label>
                            <input type="text" name="recover-email" id="recover-email" class="form-control"/>
                            <a href="javascript:;"
                                class="btn submit-form-ajax">RECOVER</a>
                            <input type="hidden" name="action" value="recover">
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade in" id="resend-activation" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
                        <h4>Resend activation link</h4>
                        <div class="ajax-response"></div>
                        <form class="form-login">
                            <label for="resend-email">Email</label>
                            <input type="text" name="resend-email" id="resend-email" class="form-control"/>
                            <a href="javascript:;"
                                class="btn submit-form-ajax">RESEND</a>
                            <input type="hidden" name="action" value="resend_activation">
                        </form>
                    </div>
                </div>
            </div>
        </div>



         <div class="modal fade in" id="feedback" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
                        <h4 class="contact">Contact</h4>
                        <div class="ajax-response"></div>
                        <form class="form-ask">
                            <label for="name" class="label1">Name</label>
                            <input type="text" class="form-control one" name="name" id="name">

                            <label for="name" class="label1">Email</label>
                            <input type="text" class="form-control" name="email" id="text">

                            <label for="name" class="label1">Phone Number</label>
                            <input type="text" class="form-control" name="contactNo" id="text">

                            <label for="name" class="label1">Message</label>
                            <textarea class="form-control" name="message" id="message"></textarea>
                            <input type="hidden" name="action" value="contact">

                            <div class="g-recaptcha" data-sitekey="6LdXh0kUAAAAAENca_6dTrnAsOUP5SqhZpi0DVhY"></div>   
                            <br>

                            <div id="btn1">
                                <button type="button" class="btn btn-default" id="submit">SUBMIT</button>
                                <button type="button" class="btn btn-default" id="reset"> RESET</button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>

        

        <!-- modal -->
        <div class="modal fade in" id="login" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
                        <h4>Login</h4>
                        <div class="ajax-response"></div>
                        <form class="form-login">
                            <label for="login-email">Email</label>
                            <input type="text" name="login-email" id="login-email" class="form-control"/>
                            <label for="login-password">Password</label>
                            <input type="password" name="login-password" id="login-password" class="form-control"/>
                            <div class="row">
                                <div class="col-xs-6">
                                    <div class="checkbox checkbox-inline">
                                        <input type="checkbox" name="login-remember" id="login-remember"/>
                                        <label for="login-remember">Remember me</label>
                                    </div>
                                </div>
                                <div class="col-xs-6 text-right">
                                    <a href="javascript:;" class="forgot-password"
                                        data-dismiss="modal">Forgot Password?</a>
                                </div>
                            </div>
                            <a href="javascript:;"
                                class="btn submit-form-ajax">LOGIN</a>
                            or                  <a href="javascript:;" class="btn register-close-login"
                                data-dismiss="modal">register here</a>
                            <input type="hidden" name="action" value="login">
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- .modal -->
        <div class="modal fade in" id="register-type" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
                        <h2>Register</h2>
                        <h5 id="h5">Please select user type</h5>
                        <br>
                        
                        <div class="button-style1">
                            <a href="#register" data-toggle="modal" class="login-action">
                            <button id="member-reg" class="member-reg">
                                <i class="fa fa-user"></i> &nbsp; Register as a Ad user
                            </button></a>
                            <button id="business-reg" class="business-reg">
                                <i class="fa fa-users"></i>  &nbsp; Register as a business user
                            </button>
                        </div>
                     </div>
                </div>
            </div>
        </div>

    @yield('js')        

    </body>
    
</html>

  





