<!doctype html>
<html class="no-js" lang="">

<head>
  <meta charset="utf-8">
  <title>Admin Panel | Sweet Delight Cakery</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width">
  <link rel="shortcut icon" href="/favicon.ico">

    <link href="{{asset('assets/back/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/back/font-awesome/css/font-awesome.css')}}" rel="stylesheet">

    <!-- Toastr style -->
    <link href="{{asset('assets/back/css/plugins/toastr/toastr.min.css')}}" rel="stylesheet">

    <!-- Gritter -->
   
    <link href="{{asset('assets/back/css/animate.css')}}" rel="stylesheet">
    <link href="{{asset('assets/back/css/style.css')}}" rel="stylesheet">

</head>
<body class="gray-bg">

    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <div>
                 @if($errors->has('login'))
                                <div class="alert alert-danger">
                                  Oh snap! {{$errors->first('login')}}
                                </div>
                              @endif

                <h1 class="logo-name">&nbsp;</h1>

            </div>
            <h3>Welcome to PeoplePower</h3>
            <p>Perfectly designed and precisely prepared admin panel.
                <!--Continually expanded and constantly improved Inspinia Admin Them (IN+)-->
            </p>
            <p>Login in. To see it in action.</p>
            <form class="m-t" role="form" action="{{URL::to('user/login')}}" method="POST">
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Username" required="" name="username">
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" placeholder="Password" required="" name="password">
                </div>
                <button type="submit" class="btn btn-primary block full-width m-b">Login</button>

                <!-- <a href="#"><small>Forgot password?</small></a>
                <p class="text-muted text-center"><small>Do not have an account?</small></p>
                <a class="btn btn-sm btn-white btn-block" href="register.html">Create an account</a> -->
            </form>
            <p class="m-t"> <small>Copyright &copy; 2017 - <?php echo date("Y"); ?></small> </p> <p class="m-t"> <small>The Capital Maharaja Organisation Limited.</small> </p>
        </div>
    </div>

<script src="{{asset('assets/back/js/jquery-3.1.1.min.js')}}"></script>
<script src="{{asset('assets/back/js/bootstrap.min.js')}}"></script>   

</body>

</html>
