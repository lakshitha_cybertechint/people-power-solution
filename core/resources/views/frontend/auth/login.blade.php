@extends('frontend.layouts.app')
<meta name="viewport" content="width=device-width, initial-scale=1">
<style type="text/css">
	

		@media screen and (min-width: 0px) and (max-width: 360px) {

		a.btn1 {
		width: 100%;
		font-size: 1.1rem;
		}
    	
    	.input-field label {
    			font-size: 1.1rem!important;
    	}

    	.section{
    		margin-right: 10px!important;
    	}
 
		}

		@media screen and (min-width: 361px) and (max-width: 768px){

			a.btn1 {
		width: 100%;
		font-size: 1.2rem;
		}
    	
    	.input-field label {
    			font-size: 1.2rem!important;
    	}

    	.section{
    		margin-right: 10px!important;
    	}
 
		}

		@media screen and (min-width: 769px) and (max-width: 800px) {
			a.btn1 {
		width: 100%;
		font-size: 1.4rem;
		}
    	
    	.input-field label {
    			font-size: 1.4rem!important;
    	}

    	.section{
    		margin-right: 10px!important;
    	}
 	
		}

		@media screen and (min-width: 801px) and (max-width: 980px) {
		a.btn1 {
		width: 100%;
		font-size: 1.6rem;
		}
    	
    	.input-field label {
    			font-size: 1.6rem!important;
    	}

    	.section{
    		margin-right: 10px!important;
    	}
				
		}

		/*@media screen and (min-width: 981px) {
			a.btn1 {
		width: 100%;
		font-size: 1.1rem;
		}
    	
    	.input-field label {
    			font-size: 1.3rem!important;
    	}

    	.section{
    		margin-right: 10px!important;
    	}
				
		}*/

	</style>

@section('content')
<div class="spinner-wrapper">
	<div class="spinner"></div>
</div>
	<div class="container">
		 <div class="row">
		 	<div class="section">
		 		<div class="input-field col s12">
		 			<label class="em" for="email">Email</label>
		 			<input id="email" type="text" class="validate em">
		 		</div>
		 		<div class="input-field col s12">
		 			<label class="active" for="pw">Password</label>
		 			<input  id="pw" type="Password" class="validate">
		 		</div>

		 	<div class="col col s12">
		 		<a href="{{ route('frontend.questions') }}" class="waves-effect white btn btn1 white-btn">LOGIN</a>

		 	</div>
		 	</div>
		 </div>
	</div>

	


@endsection