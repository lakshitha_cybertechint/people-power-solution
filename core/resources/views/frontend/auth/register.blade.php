@extends('frontend.layouts.app')

@section('css')

<style type="text/css"></style>

@section('content')

<style type="text/css">

@media (max-width: 400px) {
    h5.object{
      font-size: 2.64rem !important;
    }

  .input-field>label {
    font-size: 35px;
  }

  input:not([type]), input[type=text]:not(.browser-default), input[type=password]:not(.browser-default), input[type=email]:not(.browser-default), input[type=url]:not(.browser-default), input[type=time]:not(.browser-default), input[type=date]:not(.browser-default), input[type=datetime]:not(.browser-default), input[type=datetime-local]:not(.browser-default), input[type=tel]:not(.browser-default), input[type=number]:not(.browser-default), input[type=search]:not(.browser-default), textarea.materialize-textarea {
    height: 8rem;
  }

  .btn, .btn-large, .btn-small, .btn-floating, .btn-large, .btn-small, .btn-flat{
      font-size: 30px; 
  }

  .btn, .btn-large, .btn-small, .btn-flat {
    height: 55px;
    padding: 5px 25px;
  }

  .header-image {
    width: 300px!important;
}
}

@media (min-width: 401px) and (max-width: 768px) {
        #object h5{
      font-size: 6.64rem;
    }
}

</style>

<div class="spinner-wrapper">
  <div class="spinner"></div>
</div>
    <div class="container">
        <!-- Page Content goes here -->
        <div class="col s12">
            <h5 class="object">Login with</h5>
            <div id="regbtn" class="row">
                <div>
                  <a class="waves-effect waves-light btn facebook-login-btn"><i class="material-icons left">fingerprint</i>Log in with facebook</a>
                  <a class="waves-effect waves-light btn twitter-login-btn"><i class="material-icons left">fingerprint</i>Log in with twitter</a>
                </div>
            </div>
            <br>
            <h5>Register Here</h5>
        </div>

        <div class="row">
                <form class="">
                    <div class="input-field s8 l6">
                      <input id="name" type="text" class="validate">
                      <label for="name">Name</label>
                    </div>
                    <div class="input-field s8 l2">
                      <input id="user_name" type="text" class="validate">
                      <label for="user_name">User Name</label>
                    </div>
                    <div class="input-field s8 m4 l2">
                      <input id="user_password" type="password" class="validate">
                      <label for="user_password">Password </label>
                    </div>
                    <div class="input-field s8 m4 l2">
                      <input id="confirm_password" type="password" class="validate">
                      <label for="confirm_password">Confirm Password</label>
                    </div>
                    <div class="input-field s8 m4 l2">
                      <input id="mobile_number" type="text" class="validate">
                      <label for="mobile_number">Mobile</label>
                    </div>
                    <div class="col s6">
                      <a href="{{ route('frontend.questions') }}" class="waves-effect white btn btn1 white-btn">Register</a>
                    </div>
                  </form>
        </div>

    </div>





@endsection


