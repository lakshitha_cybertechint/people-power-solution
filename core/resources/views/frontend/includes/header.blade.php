<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"/>
<!-- <link href="{{asset('assets/pps-assets/css/materialize.min.css')}}" rel="stylesheet" /> -->
<link rel="stylesheet" href="{{asset('assets/pps-assets/css/style.css')}}"/>
<link href="{{asset('assets/pps-assets/css/font-awesome.css')}}" rel="stylesheet"/>
<link href="{{asset('assets/pps-assets/css/docs.css')}}" rel="stylesheet"/>

<!-- <link href="{{asset('assets/pps-assets/register-assets/assets/css/materialize.min.css')}}" rel="stylesheet"/>
<link href="{{asset('assets/front/css/style.css')}}" rel="stylesheet"/>
<link href="{{asset('assets/pps-assets/register-assets/assets/css/font-awesome.css')}}" rel="stylesheet"/>
<link href="{{asset('assets/pps-assets/register-assets/assets/css/docs.css')}}" rel="stylesheet"/>
<link href="{{asset('assets/pps-assets/register-assets/assets/css/style.css')}}" rel="stylesheet"/>
<link href="{{asset('assets/pps-assets/register-assets/assets/css/docs.css')}}" rel="stylesheet"/> -->



<style type="text/css">


	/*start ui for 0px to 500px*/

	/*nav {
		height: 125px!important;
	}

	.header-image {
    	width: 585px!important;
	}

	input[type=text]:not(.browser-default), input[type=password]:not(.browser-default), input[type=email]:not(.browser-default), input[type=url]:not(.browser-default), input[type=time]:not(.browser-default), input[type=date]:not(.browser-default), input[type=datetime]:not(.browser-default), input[type=datetime-local]:not(.browser-default), input[type=tel]:not(.browser-default), input[type=number]:not(.browser-default), input[type=search]:not(.browser-default), textarea.materialize-textarea {
	    background-color: transparent;
	    border: none;
	    border-bottom: 1px solid #9e9e9e;
	    border-radius: 0;
	    outline: none;
	    height: 7rem;
	    width: 100%;
	    font-size: 40px;
	    margin: 0 0 8px 0;
	    line-height: 13rem;
	    padding: 0;
	    -webkit-box-shadow: none;
	    box-shadow: none;
	    -webkit-box-sizing: content-box;
	    box-sizing: content-box;
	    -webkit-transition: border .3s, -webkit-box-shadow .3s;
	    transition: border .3s, -webkit-box-shadow .3s;
	    transition: box-shadow .3s, border .3s;
	    transition: box-shadow .3s, border .3s, -webkit-box-shadow .3s;
	}

	h5 {
		font-size: 3.30rem;
    	line-height: 10rem;
	}

	.facebook-login-btn {
	    background-color: #4267b2;
	    font-weight: 600;
	    width: 90%;
	    height: 95px;
	    font-size: 43px;
	    padding-top: 25px;
	    margin: 0 0 25px 0;
	}

	.twitter-login-btn {
	    background-color: #4267b2;
	    font-weight: 600;
	    width: 90%;
	    height: 95px;
	    font-size: 43px;
	    padding-top: 25px;
	}

	.btn i {
	    font-size: 5.3rem!important;
	    line-height: 3rem!important;
	    padding-left: 45px!important;
	}*/

	/*end ui for 0px to 500px*/

	@media screen and (max-width: 359px) {

	#loader {
	    display: block;
	    position: relative;
	    left: 20%;
	    top: 20%;
	    width: 60%;
	    margin: 0 0 0 0;
	}

	.nav-wrapper {
    	padding: 0px 0px 5px 0px!important;
    }

    .row .col {
    	padding: 0 0!important;
    }

}

	.nav-wrapper {
		padding: 0px 10px 5px 10px;
		position: absolute;
	}

	.nav-wrapper i {
		line-height: 25px!important;
		height: 45px!important;
	}

	.nav-wrapper a {
		padding-top: 6px;
		align-content: center;
	}

	nav {
		background-color: #a72222;
		height: 65px;
	}

	.nav-user-right {
		float: right;
		padding-top: 10px;
    	margin-top: 10px!important;
	}

	.header-image {
		width: 270px!important;
	}

	.white-btn {
		color: #c62828;
	}

	.facebook-login-btn {
		background-color: #4267b2;
		font-weight: 600;
	}

	.twitter-login-btn {
		background-color: #1da1f2;
		font-weight: 600;
	}

</style>


<!-- start logo blink -->
<!-- start logo blink -->
<!-- <div id="demo-content">
    <div id="loader-wrapper">
        <div id="loader">
        	<img class="blink-image" src="{{asset('assets/pps-assets/images/peoplepowerlogo.png')}}">
        </div>

        <div class="loader-section section-left"></div>
        <div class="loader-section section-right"></div>

    </div>
</div> -->
<!-- start logo blink-->


	<!-- start navbar -->
	<!-- <nav>
	    <div class="nav-wrapper">
	    	<a href="#" class="brand-logo"><img style="width: 200px; margin-left: 20px;margin-top: 5px;" src="{{asset('assets/front/images/peoplepowerlogo.svg')}}"></a>
	    </div>
	</nav> -->
	<!-- end navbar -->
<a id="loadMore"></a>
	<nav>
		<div class="nav-wrapper">
			<div class="row">
				<div class="col c4">
					<i class="fa fa-cog waves-effect icon"></i>
				</div>
				<div class="col c4 center">
					<a href="{{ route('frontend.index') }}" class="brand-logo">
						<img class="header-image" src="{{asset('assets/pps-assets/images/peoplepowerlogo.png')}}"/>
					</a>
				</div>
				<div class="col c4 right">
					<i class="fa fa-user waves-effect icon nav-user-right"></i>
				</div>


			</div>

		</div>
  	</nav>


