<!DOCTYPE html>
<html>
<head>
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="assets/pps-assets/css/materialize.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('assets/pps-assets/css/normalize.css')}}">
    <link rel="stylesheet" href="{{asset('assets/pps-assets/css/main.css')}}">
    <link rel="stylesheet" href="{{asset('assets/pps-assets/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('assets/pps-assets/css/font-awesome.css')}}">
    <link rel="stylesheet" href="{{asset('assets/pps-assets/css/docs.css')}}">

    <style type="text/css">
        .section{
            margin-top: 30%;
            /*margin-right: 15%;*/
        }
        #loader img {
                width: 100%!important;
                left: -15%!important;
            }
        .btn1{
            margin-top: 20px;
            text-align: center;
            width: 50%;
            color: #c62828!important;
            font-weight: bold;
        }
        .header-image {
                width: 200px!important;
            }
        body{
            background-color: #C62828;
        }
        /*responsive*/
        @media screen and (min-width: 321px) and (max-width: 360px) {
            .section{
                 margin-top: 30%;
            }
            .btn1{
                width: 90%;
                text-align: center;
                padding: 0 10px 0 10px;
                color: #c62828!important;
            }
            #loader img {
                width: 100%!important;
                left: -15%!important;
            }

        }
        @media screen and (min-width: 361px) and (max-width: 768px) {
            .section{
                 margin-top: 30%;
            }
            .btn1{
                width: 90%;
                text-align: center;
                padding: 0 10px 0 10px;
                color: #c62828!important;
            }
            #loader img {
                width: 100%!important;
                left: -15%!important;
            }

        }
        @media screen and (min-width: 769px) and (max-width: 800px) {
            .section{
                margin-top: 35%;
            }
        }
        @media screen and (min-width: 801px) and (max-width: 980px) {
            .section{
                margin-top: 40%;
            }
            .btn1{
                width: 70%;
                text-align: center;
                padding: 0 10px 0 10px;
                color: #c62828!important;
            }
            #loader img {
                width: 100%!important;
                left: -15%!important;
            }
        }
    </style>
    <style type="text/css">
        .spinner-wrapper {
        position: fixed;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #79060C;
        z-index: 999999;
        }
    </style>
        <link href="{{asset('assets/pps-assets/nprogress/nprogress.css')}}" rel="stylesheet" />

        

        
        <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script src="{{asset('assets/pps-assets/nprogress/nprogress.js')}}"></script>
        <script src="{{asset('assets/pps-assets/js/script.js')}}"></script>

@include('frontend.includes.header')

</head>

<body class="demo">

<div id="content">
    @yield('content')
</div>
@include('frontend.includes.footer')


</body>
</html>
