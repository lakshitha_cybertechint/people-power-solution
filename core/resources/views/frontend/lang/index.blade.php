@extends('frontend.layouts.app')

@section('content')

<style type="text/css">
    .section{
      margin-top: 20%;
      /*margin-right: 15%;*/
    }
    .btn1{
      margin-top: 20px;
      text-align: center;
      width: 40%;
      color: #c62828;
      font-weight: bold;
    }
    body{
      background-color: #c62828;
    }
    /*responsive*/
    @media screen and (min-width: 321px) and (max-width: 360px) {
      .section{
        margin-top: 30%;
      }
      .btn1{
        width: 90%;
        text-align: center;
        padding: 0 10px 0 10px;
        font-size: 15px;
      } 
    }
    @media screen and (min-width: 361px) and (max-width: 768px) {
      .section{
         margin-top: 30%;
      } 
      .btn1{
        width: 90%;
        text-align: center;
        padding: 0 10px 0 10px;
      }
    }
    @media screen and (min-width: 769px) and (max-width: 800px) {
      .section{
        margin-top: 35%;
      }
      .btn1{
        width: 70%;
      }
    }
    @media screen and (min-width: 801px) and (max-width: 980px) {
      .section{
        margin-top: 40%;
      } 
      .btn1{
        width: 70%;
        text-align: center;
        padding: 0 10px 0 10px;
        font-size: 20px;
      }
    }
  </style>



</head>

<body>
<div class="spinner-wrapper">
  <div class="spinner"></div>
</div>
  <div class="container">
     <div class="row">
      <div class="section">
      	  <div class="col s12">
          	<a href="{{ route('frontend.register') }}"  class="waves-effect white btn btn1">ENGLISH</a>
          </div>
          <div class="col s12">
          	<a href="{{ route('frontend.register') }}" class="waves-effect white btn btn1">&#x0DC3;&#x0DD2;&#x0D82;&#x0DC4;&#x0DBD;</a>
          </div>
          <div class="col s12">
          	<a href="{{ route('frontend.register') }}"  class="waves-effect white btn btn1">&#x0BA4;&#x0BAE;&#x0BBF;&#x0BB4;&#x0BCD;</a>
          </div>
      </div>
     
     </div>
  </div>
@endsection