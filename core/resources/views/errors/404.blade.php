@extends('layouts.back.master') @section('title','Ops! You\'re in a Wrong Place.')
@section('content')
</div>

    <div class="container" 
    <div class="row" style="margin-bottom:4px;vertical-align:middle;padding-bottom:10%;">
      <div class="col-md-12 col-xs-12 text-center" style="vertical-align:middle;padding-bottom:10%;">
        
        <h3 style="margin-top: 8px;">The page you are looking is not found.</h3>
        <a href="{{url('/admin')}}" style="text-decoration:underline;font-size: 15px;color:#000"><i class="fa fa-angle-left"></i> Back to Home</a>
      </div>
    </div>
 
    </div>
@stop

