<?php $loggedUser=Sentinel::getUser();?>
<ul class="nav metismenu" id="side-menu">
	 <li class="nav-header">
        <div class="dropdown profile-element text-center"> <span>
					<a href="{{ url('admin') }}">
						<img alt="image" class="img-circle" src="{{asset('assets/back/img/profile_small.jpg')}}" />
					</a>
             </span>
            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">{{$loggedUser->first_name." ". $loggedUser->last_name}}</strong>
             </span> <span class="text-muted text-xs block">{{$loggedUser->username}} <b class="caret"></b></span> </span> </a>
            <ul class="dropdown-menu animated fadeInRight m-t-xs">
                <li><a href="{{ url('user/admin/profile') }}">Profile</a></li>
                <li class="divider"></li>
                <li><a href="{{ url('user/logout') }}">Logout</a></li>
            </ul>
        </div>
        <div class="logo-element">
            IN+
        </div>
    </li>

@if(isset($menu))
	{!!$menu!!}
@endif
</ul>
