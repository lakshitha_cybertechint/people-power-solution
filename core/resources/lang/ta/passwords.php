<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Password Reminder Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are the default lines which match reasons
	| that are given by the password broker for a password update attempt
	| has failed, such as for an invalid token or invalid new password.
	|
	*/

	"password" => "கடவுச்சொல் குறைந்தபட்சம் ஆறு எழுத்துக்கள் இருக்க வேண்டும் மற்றும் உறுதிப்படுத்தல் பொருந்த வேண்டும்.",
	"user" => "அந்த மின்னஞ்சல் முகவரியுடன் ஒரு பயனரை நாம் கண்டுபிடிக்க முடியவில்லை.",
	"token" => "இந்த கடவுச்சொல் மீட்டமை டோக்கன் தவறானது.",
	"sent" => "உங்கள் கடவுச்சொல் மீட்டமைத்த இணைப்பை நாங்கள் மின்னஞ்சல் செய்துள்ளோம்!",
	"reset" => "உங்களுடைய கடவுச்சொல் மாற்றப்பட்டது!",

];
