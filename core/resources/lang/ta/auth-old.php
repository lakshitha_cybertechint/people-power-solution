<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Pagination Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are used by the paginator library to build
	| the simple pagination links. You are free to change them to anything
	| you want to customize your views to better match your application.
	|
	*/

	'social' => [
		'facebook'	=>	'ෆේස්බුක් සමග පිවිසුම',
		'twitter'	=>	'ට්විටර් සමඟ පිවිසුම',
	],
	'login'		=>	'මෙතනින් පිවිසෙන්න',
	'register'	=>	'මෙතැන ලියාපදිංචි වන්න',
	'or'		=>	'හෝ',
	'form'		=>	[
		'name'				=>	'නම',
		'user_name'			=>	'පරිශීලක නාමය',
		'mobile'			=>	'ජංගම දූරකථන අංකය',
		'lang'				=>	'භාෂාව',
		'password'			=>	'මුරපදය',
		'confirm_password'	=>	'මුරපදය තහවුරු කරන්න',
		'login'				=>	'පිවිසෙන්න',
		'register'			=>	'ලියාපදිංචි වන්න',
	],
	'forget_password'		=>	'Forget Password? Click Here to Reset',
	'settings'	=>	[
		'account_settings'	=>	'Account Settings',
		'update_settings'	=>	'Update Settings',
		'change_password'	=>	'Change Password',
		'form'	=>	[
			'current_password'	=>	'Current Password',
			'new_password'		=>	'New Password',
			'confirm_password'	=>	'Confirm Password',
		],
	],
];
