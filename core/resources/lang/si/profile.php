<?php

return [


	"back_to_quetsions"    	=> "ප්‍රශ්නවලට නැවතතත්",
	"vote_update"          	=> "ඡන්දය වෙනස් කිරීම",
	"logout"               	=> "ඉවත්වන්න",
	"english"				=> "ENGLISH",
	"sinhala"				=> "සිංහල",
	"tamil"					=> "தமிழ்",
];
