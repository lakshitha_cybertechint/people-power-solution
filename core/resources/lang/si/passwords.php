<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Password Reminder Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are the default lines which match reasons
	| that are given by the password broker for a password update attempt
	| has failed, such as for an invalid token or invalid new password.
	|
	*/

	"password" => "මුරපද අවම වශයෙන් අක්ෂර හයකින් සමන්විත විය යුතු අතර තහවුරු කිරීම සනාථ කරයි.",
	"user" => "අපට එම විද්යුත් තැපැල් ලිපිනය සමඟ පරිශීලකයෙකු සොයා ගත නොහැක.",
	"token" => "මෙම මුරපදය යළි පිහිටුවීමේ ටෝකනය වලංගු නොවේ.",
	"sent" => "ඔබගේ රහස් පද යළි සකසන සබැඳිය අප විසින් ඊ-තැපැල් කර ඇත!",
	"reset" => "ඔබගේ මුරපදය යළි සැකසිනි!",

];
