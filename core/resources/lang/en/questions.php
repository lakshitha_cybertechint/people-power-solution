<?php

return [
	'no_questions'		=> 'No questions available to answer at the moment!',
	'no_votes'			=> "You haven't voted for any question yet!",
	'no_votes_casted'	=> "Be the first one to Vote!",
	'fb_share'			=>	'Share on facebook',
	'tw_share'			=>	'Tweet your opinion',
	'yes'				=>	'YES',
	'no'				=>	'NO',
	'no_votes_casted'	=> "be the first one to vote!",
	'fb_share'			=> 'Share on facebook',
	'tw_share'			=> 'Tweet your opinion',
	'yes'				=> 'YES',
	'no'				=> 'NO',
	'published_on'		=> 'Published on',
	'answered'			=> 'Answered Questions',
];
