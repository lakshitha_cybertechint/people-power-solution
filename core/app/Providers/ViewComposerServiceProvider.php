<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use AdManage\Models\Location;
use AdManage\Models\Category;
use Request;

class ViewComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {

        view()->composer('layouts.front.master', function($view)
        {
            $view->with('location_select_options', Location::all());
        });

        view()->composer('layouts.front.master', function($view)
        {
            $view->with('category_select_options', Category::all());
        });

        view()->composer('layouts.front.master', function($view)
        {   
            if (Request::query('sortby')) {
                $sortby = Request::query('sortby');
                
            } else {
                $sortby = NULL;
            }
            $view->with('sortby', $sortby);
            
        });

        view()->composer('layouts.front.master', function($view)
        {   
            if (Request::query('keyword')) {
                $keyword = Request::query('keyword');
                
            } else {
                $keyword = NULL;
            }
            $view->with('keyword', $keyword);
            
        });

        view()->composer('layouts.front.master', function($view)
        {   
            if (Request::query('location')) {
                $location = Request::query('location');
                
            } else {
                $location = NULL;
            }
            $view->with('location', $location);
            
        });

        view()->composer('layouts.front.master', function($view)
        {   
            if (Request::query('category')) {
                $category = Request::query('category');
                
            } else {
                $category = NULL;
            }
            $view->with('category', $category);
            
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
