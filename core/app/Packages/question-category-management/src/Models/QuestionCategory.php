<?php
namespace QuestionCategoryManage\Models;

use Illuminate\Database\Eloquent\Model;
use QuestionManage\Models\Question;

class QuestionCategory extends Model{


	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name'];

	public function questions()
	{
	  return $this->hasMany(Question::class);
	}

}
