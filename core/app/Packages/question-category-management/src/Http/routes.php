<?php
/**
 * Blog MANAGEMENT ROUTES
 *
 * @version 1.0.0
 * @author Praveen Chameera <praveenchameera@gmail.com>
 * @copyright 2017 Praveen Chameera
 */

/**
 * USER AUTHENTICATION MIDDLEWARE
 */
Route::group(['middleware' => ['auth']], function()
{
    Route::group(['prefix' => 'admin/question-category', 'namespace' => 'QuestionCategoryManage\Http\Controllers'], function(){
      /**
       * GET Routes
       */
      Route::get('add', [
        'as' => 'question-category.add', 'uses' => 'QuestionCategoryController@addView'
      ]);


      Route::get('edit/{id}', [
        'as' => 'question-category.edit', 'uses' => 'QuestionCategoryController@editView'
      ]);

      Route::get('list', [
        'as' => 'question-category.list', 'uses' => 'QuestionCategoryController@listView'
      ]);

      Route::get('json/list', [
        'as' => 'question-category.list', 'uses' => 'QuestionCategoryController@jsonList'
      ]);

      Route::delete('image/delete', [
        'as' => 'question-category.image.delete', 'uses' => 'QuestionCategoryController@deleteImg'
      ]);



      /**
       * POST Routes
       */
      Route::post('add', [
        'as' => 'question-category.add', 'uses' => 'QuestionCategoryController@add'
      ]);

      Route::post('edit/{id}', [
        'as' => 'question-category.edit', 'uses' => 'QuestionCategoryController@edit'
      ]);


      Route::post('delete', [
        'as' => 'question-category.delete', 'uses' => 'QuestionCategoryController@delete'
      ]);


    });
});
