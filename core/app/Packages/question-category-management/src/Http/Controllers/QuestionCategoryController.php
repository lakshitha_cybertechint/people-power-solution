<?php

namespace QuestionCategoryManage\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Permissions\Models\Permission;
use Sentinel;
use QuestionCategoryManage\Models\QuestionCategory;

class QuestionCategoryController extends Controller {

    /**
     * Show the Branch add screen to the user.
     *
     * @return Response
     */
    public function addView() {
        return view('QuestionCategoryManage::add');
    }

    /**
     * Add new Branch data to database
     *
     * @return Redirect to Brach add
     */
    public function add(Request $request) {

      $this->validate($request,[
        'name' => 'required|max:255',
      ]);

      $requestData = $request->all();

      QuestionCategory::create($requestData);


      return redirect('admin/question-category/list')->with(['success' => true,
        'success.message' => 'QuestionCategory Created successfully!',
        'success.title' => 'Well Done!']);
    }

    /**
     * View Branch List View
     *
     * @return Response
     */
    public function listView() {
        return view('QuestionCategoryManage::list');
    }

    /**
     * Device list
     *
     * @return Response
     */
    public function jsonList(Request $request) {
        if ($request->ajax()) {
            $data = QuestionCategory::get();
            $jsonList = array();
            $i = 1;
            foreach ($data as $key => $questionCategory) {

                $dd = array();
                array_push($dd, $i);
                array_push($dd, $questionCategory->name);
                $permissions = Permission::whereIn('name', ['question.category.edit', 'admin'])->where('status', '=', 1)->lists('name');
                if (Sentinel::hasAnyAccess($permissions)) {
                    array_push($dd, '<center><a href="#" class="blue" onclick="window.location.href=\'' . url('admin/question-category/edit/' . $questionCategory->id) . '\'" data-toggle="tooltip" data-placement="top" title="Edit product"><i class="fa fa-pencil"></i></a></center>');
                } else {
                    array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Edit Disabled"><i class="fa fa-pencil"></i></a>');
                }

                $permissions = Permission::whereIn('name', ['question.category.delete', 'admin'])->where('status', '=', 1)->lists('name');
                if (Sentinel::hasAnyAccess($permissions)) {
                    array_push($dd, '<center><a href="#" class="product-delete" data-id="' . $questionCategory->id . '" data-toggle="tooltip" data-placement="top" title="Delete Blog"><i class="fa fa-trash-o"></i></a></center>');
                } else {
                    array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Delete Disabled"><i class="fa fa-trash-o"></i></a>');
                }

                array_push($jsonList, $dd);
                $i++;
            }
            return response()->json(array('data' => $jsonList));
        } else {
            return response()->json(array('data' => []));
        }
    }


    /**
     * Delete a device
     * @param  Request $request branch id
     * @return Json           	json object with status of success or failure
     */
    public function delete(Request $request) {
      $this->validate($request,['id' => 'required|exists:question_categories,id']);
      $id = $request->input('id');
      $questionCategory = QuestionCategory::with('questions')->find($id);

      if (count($questionCategory->questions)) {
        return response()->json(['status' => 'fail'],405);
      }

      $questionCategory->delete();
      return response()->json(['status' => 'success']);
    }

    /**
     * Show the devcie edit screen to the devcie.
     *
     * @return Response
     */
    public function editView($id) {
      $questionCategory = QuestionCategory::findOrFail($id);
      return view('QuestionCategoryManage::edit',compact('questionCategory'));
    }

    /**
     * Add new device data to database
     *
     * @return Redirect to Branch add
     */
    public function edit(Request $request, $id) {

      $this->validate($request,[
        'name' => 'required|max:255',
      ]);

      $questionCategory = QuestionCategory::findOrFail($id);

      $requestData = $request->all();

      $questionCategory->update($requestData);


      return redirect('admin/question-category/edit/' . $id)->with(['success' => true,
        'success.message' => 'QuestionCategory updated successfully!',
        'success.title' => 'Good Job!']);
    }


}
