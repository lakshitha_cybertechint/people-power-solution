@extends('layouts.back.master') @section('current_title','Update Question Category')
@section('css')

<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-3.5.2/select2.css')}}" />
<link rel="stylesheet" href="{{asset('assets/back/vendor/bootstrap-datepicker-master/dist/css/bootstrap-datepicker3.min.css')}}" />
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-bootstrap/select2-bootstrap.css')}}" />

@stop
@section('current_path')
<div id="hbreadcrumb">
    <ol class="hbreadcrumb breadcrumb">
        <li><a href="{{url('admin/event/list')}}">Question Category Management</a></li>

        <li class="active">
            <span>Update Question Category</span>
        </li>
    </ol>
</div>
@stop
@section('content')

<div class="row">
    <div class="col-lg-12">
        <div class="hpanel">
            <div class="panel-body">
              @if ($errors->any())
                <div class="alert alert-danger">
                  <ul class="">
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
                </div>
              @endif
              {!! Form::model($questionCategory, [
                  'method' => 'POST',
                  'url' => ['admin/question-category/edit', $questionCategory->id],
                  'class' => 'form-horizontal',
                  'files' => true
              ]) !!}

              <div class="form-group">
                  <div class="{{ $errors->has('name') ? 'has-error' : ''}}">
                    {!! Form::label('NAME', 'NAME', ['class' => 'col-md-2 control-label']) !!}
                    <div class="col-md-10">
                        {!! Form::text('name',null, ['class' => 'form-control col-sm-10', 'required' => 'required']) !!}
                        {!! $errors->first('name', '<p class="help is-danger">:message</p>') !!}
                    </div>
                  </div>
              </div>


              <div class="form-group">
                  <div class="col-sm-8 col-sm-offset-2">
                      <button class="btn btn-default" type="button" onclick="location.reload();">Cancel</button>
                      <button class="btn btn-primary" type="submit">Save Changes</button>
                  </div>
              </div>

              {!! Form::close() !!}

        </div>
    </div>
</div>
@stop
@section('js')
  <script src="{{asset('assets/back/vendor/bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js')}}"></script>
  <script type="text/javascript">
    $('.datepicker').datepicker({
      format : "yyyy-mm-dd"
    });
  </script>
@stop
