@extends('layouts.back.master') @section('current_title','New Question')
@section('css')

<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-3.5.2/select2.css')}}" />
<link rel="stylesheet" href="{{asset('assets/back/vendor/bootstrap-datepicker-master/dist/css/bootstrap-datepicker3.min.css')}}" />
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-bootstrap/select2-bootstrap.css')}}" />

@stop
@section('current_path')
<div id="hbreadcrumb">
<ol class="hbreadcrumb breadcrumb">
    <li><a href="{{url('admin/question/list')}}">Question Management</a></li>

    <li class="active">
        <span>New Question</span>
    </li>
</ol>
</div>
@stop
@section('content')

<div class="row">
<div class="col-lg-12">
    <div class="hpanel">
        <div class="panel-body">
          @if ($errors->any())
            <div class="alert alert-danger">
              <ul class="">
                  @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                  @endforeach
              </ul>
            </div>
          @endif
          <form  class="form-horizontal" id="form" method="post" enctype="multipart/form-data">
              {!!Form::token()!!}
              <div class="form-group">
                  <div class="{{ $errors->has('en_description') ? 'has-error' : ''}}">
                    {!! Form::label('en_description', 'English', ['class' => 'col-md-2 control-label']) !!}
                    <div class="col-md-10">
                        {!! Form::textarea('en_description',null, ['class' => 'form-control col-sm-10', 'required' => 'required']) !!}
                        {!! $errors->first('en_description', '<p class="help is-danger">:message</p>') !!}
                    </div>
                  </div>
              </div>

              <div class="form-group">
                  <div class="{{ $errors->has('si_description') ? 'has-error' : ''}}">
                    {!! Form::label('si_description', 'Sinhala', ['class' => 'col-md-2 control-label']) !!}
                    <div class="col-md-10">
                        {!! Form::textarea('si_description',null, ['class' => 'form-control col-sm-10', 'required' => 'required']) !!}
                        {!! $errors->first('si_description', '<p class="help is-danger">:message</p>') !!}
                    </div>
                  </div>
              </div>



              <div class="form-group">
                  <div class="{{ $errors->has('ta_description') ? 'has-error' : ''}}">
                    {!! Form::label('ta_description', 'Tamil', ['class' => 'col-md-2 control-label']) !!}
                    <div class="col-md-10">
                        {!! Form::textarea('ta_description',null, ['class' => 'form-control col-sm-10', 'required' => 'required']) !!}
                        {!! $errors->first('ta_description', '<p class="help is-danger">:message</p>') !!}
                    </div>
                  </div>
              </div>

              <div class="form-group">
                  <div class="{{ $errors->has('expire_date') ? 'has-error' : ''}}">
                    {!! Form::label('expire_date', 'EXPIRE DATE', ['class' => 'col-md-2 control-label']) !!}
                    <div class="col-md-10">
                        {!! Form::text('expire_date',null, ['class' => 'form-control col-sm-10 datepicker', 'required' => 'required']) !!}
                        {!! $errors->first('expire_date', '<p class="help is-danger">:message</p>') !!}
                    </div>
                  </div>
              </div>

              <div class="form-group">
                  <div class="{{ $errors->has('publish_date') ? 'has-error' : ''}}">
                    {!! Form::label('publish_date', 'PUBLISH DATE', ['class' => 'col-md-2 control-label']) !!}
                    <div class="col-md-10">
                        {!! Form::text('publish_date',date('Y-m-d'), ['class' => 'form-control col-sm-10 datepicker', 'required' => 'required']) !!}
                        {!! $errors->first('publish_date', '<p class="help is-danger">:message</p>') !!}
                    </div>
                  </div>
              </div>

              <div class="form-group">
                  <div class="{{ $errors->has('event_id') ? 'has-error' : ''}}">
                    {!! Form::label('event_id', 'Topic', ['class' => 'col-md-2 control-label']) !!}
                    <div class="col-md-10">
                        {!! Form::select('event_id', $events, null, ['class' => 'form-control', 'required' => 'required']) !!}
                        {!! $errors->first('event_id', '<p class="help is-danger">:message</p>') !!}
                    </div>
                  </div>
              </div>
              <div class="form-group">
                  <div class="{{ $errors->has('question_category_id') ? 'has-error' : ''}}">
                    {!! Form::label('question_category_id', 'Category', ['class' => 'col-md-2 control-label']) !!}
                    <div class="col-md-10">
                        {!! Form::select('question_category_id', $categories, null, ['class' => 'form-control', 'required' => 'required']) !!}
                        {!! $errors->first('question_category_id', '<p class="help is-danger">:message</p>') !!}
                    </div>
                  </div>
              </div>


              <div class="hr-line-dashed"></div>
              <div class="form-group">
                  <div class="col-sm-8 col-sm-offset-2">
                      <button class="btn btn-default" type="button" onclick="location.reload();">Cancel</button>
                      <button class="btn btn-primary" type="submit">Done</button>
                  </div>
              </div>

          </form>
        </div>
    </div>
</div>
@stop
@section('js')
  <script src="{{asset('assets/back/vendor/bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js')}}"></script>
  <script type="text/javascript">
    $('.datepicker').datepicker({
      format : "yyyy-mm-dd"
    });
  </script>
@stop
