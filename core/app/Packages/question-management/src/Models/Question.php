<?php
namespace QuestionManage\Models;

use Illuminate\Database\Eloquent\Model;
use UserManage\Models\User;
use EventManage\Models\Event;
use QuestionCategoryManage\Models\QuestionCategory;

class Question extends Model{


	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['si_description','expire_date', 'publish_date', 'en_description','ta_description','', 'user_id','slug', 'question_category_id','event_id'];

	public function user()
	{
	  return $this->belongsTo(User::class);
	}

	public function event()
	{
	  return $this->belongsTo(Event::class);
	}

	public function questionCategory()
	{
	  return $this->belongsTo(QuestionCategory::class);
	}

}
