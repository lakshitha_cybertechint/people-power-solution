<?php

namespace QuestionManage\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Permissions\Models\Permission;
use Sentinel;
use QuestionManage\Models\Question;
use EventManage\Models\Event;
use QuestionCategoryManage\Models\QuestionCategory;

class QuestionController extends Controller {

    /**
     * Show the Branch add screen to the user.
     *
     * @return Response
     */
    public function addView() {
        $categories = QuestionCategory::lists('name','id');
        $events = Event::lists('name','id');
        return view('QuestionManage::add',compact('categories','events'));
    }

    /**
     * Add new Branch data to database
     *
     * @return Redirect to Brach add
     */
    public function add(Request $request) {

      $this->validate($request,[
        'si_description' => 'required',
        'en_description' => 'required',
        'ta_description' => 'required',
        'question_category_id' => 'required|exists:question_categories,id',
        'event_id' => 'required|exists:events,id',
        'expire_date' => 'required|date|date_format:Y-m-d',
      ]);

      $requestData = $request->all();
      $requestData['slug'] = str_slug($request->title, '-');
      $requestData['user_id'] = Sentinel::getUser()->id;

      Question::create($requestData);


      return redirect('admin/question/list')->with(['success' => true,
        'success.message' => 'Question Created successfully!',
        'success.title' => 'Well Done!']);
    }

    /**
     * View Branch List View
     *
     * @return Response
     */
    public function listView() {
        return view('QuestionManage::list');
    }

    /**
     * Device list
     *
     * @return Response
     */
    public function jsonList(Request $request) {
        if ($request->ajax()) {
            $data = Question::get();
            $jsonList = array();
            $i = 1;
            foreach ($data as $key => $question) {

                $dd = array();
                array_push($dd, $question->id);
                array_push($dd, sprintf($question->en_description) );
                array_push($dd, $question->event->name);
                array_push($dd, $question->questionCategory->name);
                array_push($dd, $question->user->first_name);
                array_push($dd, $question->publish_date);
                array_push($dd, $question->expire_date);
                if ($question->status) {
                  array_push($dd, "Active");
                }else {
                  array_push($dd, "Deactive");
                }

                $permissions = Permission::whereIn('name', ['question.status', 'admin'])->where('status', '=', 1)->lists('name');
                if (Sentinel::hasAnyAccess($permissions)) {
                    array_push($dd, '<center><a href="'.url("admin/question/status/".$question->id).'" data-toggle="tooltip" data-placement="top" title="Chanage Status"><i class="fa fa-flag"></i></a></center>');
                } else {
                    array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Delete Disabled"><i class="fa fa-trash-o"></i></a>');
                }



                $permissions = Permission::whereIn('name', ['question.edit', 'admin'])->where('status', '=', 1)->lists('name');
                if (Sentinel::hasAnyAccess($permissions)) {
                    $edit = '<a href="#" class="blue" onclick="window.location.href=\'' . url('admin/question/edit/' . $question->id) . '\'" data-toggle="tooltip" data-placement="top" title="Edit product"><i class="fa fa-pencil"></i></a>';
                } else {
                      $edit = '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Edit Disabled"><i class="fa fa-pencil"></i></a>';
                }

                $permissions = Permission::whereIn('name', ['question.delete', 'admin'])->where('status', '=', 1)->lists('name');
                if (Sentinel::hasAnyAccess($permissions)) {
                    $delete = '<a href="#" class="product-delete" data-id="' . $question->id . '" data-toggle="tooltip" data-placement="top" title="Delete Status"><i class="fa fa-trash-o"></i></a>';
                } else {
                    $delete =  '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Delete Disabled"><i class="fa fa-trash-o"></i></a>';
                }

                array_push($dd, $edit."&nbsp;&nbsp; ".$delete);
                array_push($dd, $question->yes);
                array_push($dd, $question->no);

                array_push($jsonList, $dd);
                $i++;
            }
            return response()->json(array('data' => $jsonList));
        } else {
            return response()->json(array('data' => []));
        }
    }


    /**
     * Delete a device
     * @param  Request $request branch id
     * @return Json           	json object with status of success or failure
     */
    public function delete(Request $request) {
      $this->validate($request,['id' => 'required|exists:questions,id']);
      $id = $request->input('id');
      $question = Question::with('questions')->find($id);

      if (count($question->questions)) {
        return response()->json(['status' => 'fail'],405);
      }

      $question->delete();
      return response()->json(['status' => 'success']);
    }

    /**
     * Show the devcie edit screen to the devcie.
     *
     * @return Response
     */
    public function editView($id) {
      $categories = QuestionCategory::lists('name','id');
      $events = Event::lists('name','id');
      $question = Question::findOrFail($id);
      return view('QuestionManage::edit',compact('question','categories','events'));
    }

    /**
     * Add new device data to database
     *
     * @return Redirect to Branch add
     */
    public function edit(Request $request, $id) {

      $this->validate($request,[
        'si_description' => 'required',
        'en_description' => 'required',
        'ta_description' => 'required',
        'question_category_id' => 'required|exists:question_categories,id',
        'event_id' => 'required|exists:events,id',
        'expire_date' => 'required|date|date_format:Y-m-d',
      ]);

      $question = Question::findOrFail($id);

      $requestData = $request->all();
      $requestData['user_id'] = Sentinel::getUser()->id;

      $question->update($requestData);


      return redirect('admin/question/edit/' . $id)->with(['success' => true,
        'success.message' => 'Question updated successfully!',
        'success.title' => 'Good Job!']);
    }

    public function changeStatus($id)
    {
      $question = Question::findOrFail($id);
      $question->status = !$question->status;
      $question->save();

      return back()->with(['success' => true,
        'success.message' => 'Question Status updated successfully!',
        'success.title' => 'Good Job!']);
    }


}
