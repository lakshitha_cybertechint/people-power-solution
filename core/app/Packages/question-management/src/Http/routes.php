<?php
/**
 * Blog MANAGEMENT ROUTES
 *
 * @version 1.0.0
 * @author Praveen Chameera <praveenchameera@gmail.com>
 * @copyright 2017 Praveen Chameera
 */

/**
 * USER AUTHENTICATION MIDDLEWARE
 */
Route::group(['middleware' => ['auth']], function()
{
    Route::group(['prefix' => 'admin/question', 'namespace' => 'QuestionManage\Http\Controllers'], function(){
      /**
       * GET Routes
       */
      Route::get('add', [
        'as' => 'question.add', 'uses' => 'QuestionController@addView'
      ]);


      Route::get('edit/{id}', [
        'as' => 'question.edit', 'uses' => 'QuestionController@editView'
      ]);

      Route::get('list', [
        'as' => 'question.list', 'uses' => 'QuestionController@listView'
      ]);

      Route::get('json/list', [
        'as' => 'question.list', 'uses' => 'QuestionController@jsonList'
      ]);

      Route::get('status/{id}', [
        'as' => 'question.status', 'uses' => 'QuestionController@changeStatus'
      ]);

      Route::delete('image/delete', [
        'as' => 'question.image.delete', 'uses' => 'QuestionController@deleteImg'
      ]);



      /**
       * POST Routes
       */
      Route::post('add', [
        'as' => 'question.add', 'uses' => 'QuestionController@add'
      ]);

      Route::post('edit/{id}', [
        'as' => 'question.edit', 'uses' => 'QuestionController@edit'
      ]);


      Route::post('delete', [
        'as' => 'question.delete', 'uses' => 'QuestionController@delete'
      ]);


    });
});
