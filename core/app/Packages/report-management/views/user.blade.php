@extends('layouts.back.master') @section('current_title','Report')
@section('css')
<link rel="stylesheet" href="{{asset('assets/back/vendor/bootstrap-datepicker-master/dist/css/bootstrap-datepicker3.min.css')}}" />

@stop
@section('current_path')
<div id="hbreadcrumb">
    <ol class="hbreadcrumb breadcrumb">
        <li class="active">
            <span>User Report</span>
        </li>
    </ol>
</div>
@stop
@section('content')
<div class="row">
  <div class="col-md-12">

  </div>
    <div class="col-lg-12">
        <div class="hpanel">
            <div class="panel-body">
             	<table id="example1" class="table table-striped table-bordered table-hover" width="100%">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Mobile</th>
                        <th>Username</th>
                        <th>Email</th>
                    </tr>
                    </thead>
                </table>
        	</div>
    	</div>
	</div>
</div>
@stop
@section('js')
  <script src="{{asset('assets/back/vendor/bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js')}}"></script>
<script type="text/javascript">

  $(document).ready(function(){
    var table;
    table = $('#example1').dataTable( {
        ajax: {
          url : '{{url('admin/report/user/data')}}',
          // data: function (d){
          //   d.cat = $('#questionCatId').val()
          //   d.event = $('#eventId').val()
          //   d.user = $('#userId').val()
          //   d.date = $('#expireDate').val()
          // }
        },
        dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
        lengthMenu: [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
        buttons: [
            {extend: 'copy',className: 'btn-sm'},
            {extend: 'csv',title: 'Device List', className: 'btn-sm'},
            {extend: 'pdf', title: 'Device List', className: 'btn-sm'},
            {extend: 'print',className: 'btn-sm'}
        ],
         "autoWidth": false
    });

    // $('.fliter').change(function(event) {
    //   table.fnReloadAjax()
    // });
    //
    // $('.datepicker').change(function(event) {
    //   table.fnReloadAjax()
    // });
    //
    // $('#clearFilter').click(function(event) {
    //   $('.fliter').val('0')
    //   $('.datepicker').val('')
    //   table.fnReloadAjax()
    // });
    //
    // $('.datepicker').datepicker({
    //   format : "yyyy-mm-dd",
    // });
  });




</script>
@stop
