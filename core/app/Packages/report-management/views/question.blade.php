@extends('layouts.back.master') @section('current_title','Report')
@section('css')
<link rel="stylesheet" href="{{asset('assets/back/vendor/bootstrap-datepicker-master/dist/css/bootstrap-datepicker3.min.css')}}" />

@stop
@section('current_path')
<div id="hbreadcrumb">
    <ol class="hbreadcrumb breadcrumb">
        <li class="active">
            <span>Question Report</span>
        </li>
    </ol>
</div>
@stop
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">

        <h3 class="panel-title">Filters</h3>

      </div>
      <div class="panel-body">
        <div class="col-md-2">
          <select class="form-control fliter" id="userId">
            <option value="-1" selected disabled>--Select Agent--</option>
            @foreach ($users as $key => $value)
              <option value="{{ $value->id }}">{{ $value->first_name." ".$value->last_name }}</option>
            @endforeach
          </select>
        </div>
        <div class="col-md-2">
          <select class="form-control fliter" id="eventId">
            <option value="-1" selected disabled>--Select Topic--</option>
            @foreach ($events as $key => $value)
              <option value="{{ $value->id }}">{{ $value->name }}</option>
            @endforeach
          </select>
        </div>
        <div class="col-md-2">
          <select class="form-control fliter" id="questionCatId">
            <option value="-1" selected disabled>--Select Category--</option>
            @foreach ($cat as $key => $value)
              <option value="{{ $value->id }}">{{ $value->name }}</option>
            @endforeach
          </select>
        </div>
        <div class="col-md-2">
          <select class="form-control fliter" id="status">
            <option value="-1" selected disabled>--Select Status--</option>
            <option value="1">Active</option>
            <option value="0">Deactive</option>
          </select>
        </div>
        <div class="col-md-2  ">
          <input type="text" class="form-control datepicker"  placeholder="Expire date"  id="expireDate">
        </div>
        <div class="col-md-2  ">
          <input type="text" class="form-control datepicker"  placeholder="Publish date"  id="publishDate">
        </div>
        <div class="col-md-12">
          <button type="button" class="btn btn-default"id="clearFilter">
            CLEAR FLITER
          </button>
        </div>
      </div>
    </div>
  </div>
    <div class="col-lg-12">
        <div class="hpanel">
            <div class="panel-body">
             	<table id="example1" class="table table-striped table-bordered table-hover" width="100%">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Question</th>
                        <th>Added By</th>
                        <th>Category</th>
                        <th>Topic</th>
                        <th>Expire Date</th>
                        <th>Publish Date</th>
                        <th>Created Date</th>
                        <th>Yes</th>
                        <th>No</th>
                        <th>Status</th>
                    </tr>
                    </thead>
                </table>
        	</div>
    	</div>
	</div>
</div>
@stop
@section('js')
  <script src="{{asset('assets/back/vendor/bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js')}}"></script>
<script type="text/javascript">

  $(document).ready(function(){
    var table;
    table = $('#example1').dataTable( {
        ajax: {
          url : '{{url('admin/report/questions/data')}}',
          data: function (d){
            d.cat = $('#questionCatId').val()
            d.event = $('#eventId').val()
            d.user = $('#userId').val()
            d.date = $('#expireDate').val()
            d.status = $('#status').val()
            d.publish_date = $('#publishDate').val()
          }
        },
        dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
        lengthMenu: [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
        buttons: [
            {extend: 'copy',className: 'btn-sm'},
            {extend: 'csv',title: 'Device List', className: 'btn-sm'},
            {extend: 'pdf', title: 'Device List', className: 'btn-sm'},
            {extend: 'print',className: 'btn-sm'}
        ],
         "autoWidth": false
    });

    $('.fliter').change(function(event) {
      table.fnReloadAjax()
    });

    $('.datepicker').change(function(event) {
      table.fnReloadAjax()
    });

    $('#clearFilter').click(function(event) {
      $('.fliter').val('-1')
      $('.datepicker').val('')
      table.fnReloadAjax()
    });

    $('.datepicker').datepicker({
      format : "yyyy-mm-dd",
    });
  });




</script>
@stop
