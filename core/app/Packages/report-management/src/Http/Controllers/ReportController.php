<?php

namespace ReportManage\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Permissions\Models\Permission;
use Sentinel;
use EventManage\Models\Event;
use QuestionCategoryManage\Models\QuestionCategory;
use QuestionManage\Models\Question;
use UserManage\Models\User;
use App\Models\Frontend\Member;

class ReportController extends Controller {

    public function questionReport()
    {
      $users = User::get();
      $cat = QuestionCategory::get();
      $events = Event::get();

      return view('ReportManage::question',compact('cat','users','events'));
    }

    public function questionData(Request $request) {
      $data = Question::with('user')->where(function ($q) use ($request) {
        if ($request->has('cat')) {
          $q->where('question_category_id', $request->cat);
        }
        if ($request->has('event')) {
          $q->where('event_id', $request->event);
        }
        if ($request->has('user')) {
          $q->where('user_id', $request->user);
        }

        if ($request->has('status')) {
          $q->where('status', $request->status);
        }

        if ($request->has('date')) {
          $date = $request->date;
          $q->whereHas('event', function ($q) use ($date){
            $q->whereDate('expire_date', '=',$date );
          });
        }

        if ($request->has('publish_date')) {
          $q->whereDate('publish_date','=', $request->publish_date);
        }

        $auth = Sentinel::getUser()->roles->where('name', 'AGENT');
  			if (count($auth)) {
  				$q->whereUserId(Sentinel::getUser()->id);
  			}
      })
      ->get();

      $jsonList = array();
      foreach ($data as $key => $question) {
          $dd = array();
          array_push($dd, $question->id);
          array_push($dd, $question->en_description);
          array_push($dd, $question->user->first_name);
          array_push($dd, $question->questionCategory->name );
          array_push($dd, $question->event->name );
          array_push($dd, $question->event->expire_date );
          array_push($dd, $question->publish_date );
          array_push($dd, $question->created_at->format('Y-m-d') );
          array_push($dd, $question->yes );
          array_push($dd, $question->no );
          if ($question->status) {
            array_push($dd, "Active");
          }else {
            array_push($dd, "Deactive");
          }
          array_push($jsonList, $dd);
      }
      return response()->json(array('data' => $jsonList));

    }

    public function userReport()
    {
      return view('ReportManage::user');
    }

    public function userData(Request $request) {
      $data = Member::where(function ($q) use ($request) {

      })
      ->get();

      $jsonList = array();
      foreach ($data as $key => $question) {
          $dd = array();
          array_push($dd, $question->id);
          array_push($dd, $question->name);
          array_push($dd, $question->mobile);
          array_push($dd, $question->user_name );
          array_push($dd, $question->email );
          array_push($jsonList, $dd);
      }
      return response()->json(array('data' => $jsonList));

    }



}
