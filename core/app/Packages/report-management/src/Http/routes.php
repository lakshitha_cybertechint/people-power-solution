<?php
/**
 * Blog MANAGEMENT ROUTES
 *
 * @version 1.0.0
 * @author Praveen Chameera <praveenchameera@gmail.com>
 * @copyright 2017 Praveen Chameera
 */

/**
 * USER AUTHENTICATION MIDDLEWARE
 */
Route::group(['middleware' => ['auth']], function()
{
    Route::group(['prefix' => 'admin/report', 'namespace' => 'ReportManage\Http\Controllers'], function(){

      Route::get('questions', [
        'as' => 'report.questions', 'uses' => 'ReportController@questionReport'
      ]);

      Route::get('questions/data', [
        'as' => 'report.questions', 'uses' => 'ReportController@questionData'
      ]);

      Route::get('user', [
        'as' => 'report.user', 'uses' => 'ReportController@userReport'
      ]);

      Route::get('user/data', [
        'as' => 'report.user', 'uses' => 'ReportController@userData'
      ]);

    });
});
