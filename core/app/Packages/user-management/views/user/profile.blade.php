@extends('layouts.back.master') @section('current_title','All User')
@section('css')

@stop
@section('current_path')
<div id="hbreadcrumb">
    <ol class="hbreadcrumb breadcrumb">
        <li><a href="{{url('user/list')}}">User Management</a></li>

        <li class="active">
            <span>Profile</span>
        </li>
    </ol>
</div>
@stop
@section('content')
<div class="row">
  <div class="col-md-12">
    <ul class="nav nav-tabs">
      <li class="{{ $errors->first('old_password') || $errors->first('password') ? '' : 'active' }}"><a data-toggle="tab" href="#home">Profile</a></li>
      <li class="{{ $errors->first('old_password') || $errors->first('password') ? 'active' : '' }}"><a data-toggle="tab" href="#menu1" >Password</a></li>
    </ul>

    <div class="tab-content">
      <div id="home" class="tab-pane fade {{ $errors->first('old_password') || $errors->first('password') ? '' : 'active in' }}">
        <div class="col-md-12" style="margin-top: 20px;">
          {!! Form::open(['method' => 'POST', 'url' => 'user/admin/profile', 'class' => 'form-horizontal']) !!}
            <div class="form-group">
                <div class="{{ $errors->has('first_name') ? 'has-error' : ''}}">
                  {!! Form::label('FIRST NAME', 'FIRST NAME', ['class' => 'col-md-2 control-label']) !!}
                  <div class="col-md-10">
                      {!! Form::text('first_name',$user->first_name, ['class' => 'form-control col-sm-10', 'required' => 'required']) !!}
                      {!! $errors->first('first_name', '<p class="help is-danger">:message</p>') !!}
                  </div>
                </div>
            </div>
            <div class="form-group">
                <div class="{{ $errors->has('last_name') ? 'has-error' : ''}}">
                  {!! Form::label('LAST NAME', 'LAST NAME', ['class' => 'col-md-2 control-label']) !!}
                  <div class="col-md-10">
                      {!! Form::text('last_name',$user->last_name, ['class' => 'form-control col-sm-10', 'required' => 'required']) !!}
                      {!! $errors->first('last_name', '<p class="help is-danger">:message</p>') !!}
                  </div>
                </div>
            </div>
            <div class="form-group">
                <div class="{{ $errors->has('email') ? 'has-error' : ''}}">
                  {!! Form::label('EMAIL', 'EMAIL', ['class' => 'col-md-2 control-label']) !!}
                  <div class="col-md-10">
                      {!! Form::email('email',$user->email, ['class' => 'form-control col-sm-10', 'required' => 'required']) !!}
                      {!! $errors->first('email', '<p class="help is-danger">:message</p>') !!}
                  </div>
                </div>
            </div>
            <div class="form-group">
                <div class="{{ $errors->has('username') ? 'has-error' : ''}}">
                  {!! Form::label('USERNAME', 'USERNAME', ['class' => 'col-md-2 control-label']) !!}
                  <div class="col-md-10">
                      {!! Form::text('username',$user->username, ['class' => 'form-control col-sm-10', 'required' => 'required']) !!}
                      {!! $errors->first('username', '<p class="help is-danger">:message</p>') !!}
                  </div>
                </div>
            </div>
            <div class="form-group">
                <div class="{{ $errors->has('mobile') ? 'has-error' : ''}}">
                  {!! Form::label('MOBILE', 'MOBILE', ['class' => 'col-md-2 control-label']) !!}
                  <div class="col-md-10">
                      {!! Form::number('mobile',$user->mobile, ['class' => 'form-control col-sm-10', 'required' => 'required']) !!}
                      {!! $errors->first('mobile', '<p class="help is-danger">:message</p>') !!}
                  </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-8 col-sm-offset-2">
                    <button class="btn btn-primary" type="submit">Done</button>
                </div>
            </div>
          {!! Form::close() !!}
        </div>
      </div>
      <div id="menu1" class="tab-pane fade {{ $errors->first('old_password') || $errors->first('password') ? 'in active' : '' }}">
        <div class="col-md-12" style="margin-top: 20px;">
          {!! Form::open(['method' => 'POST', 'url' => 'user/admin/password', 'class' => 'form-horizontal']) !!}
            <div class="form-group">
                <div class="{{ $errors->has('old_password') ? 'has-error' : ''}}">
                  {!! Form::label('OLD PASSWORD', 'OLD PASSWORD', ['class' => 'col-md-2 control-label']) !!}
                  <div class="col-md-10">
                      {!! Form::password('old_password', ['class' => 'form-control col-sm-10', 'required' => 'required']) !!}
                      {!! $errors->first('old_password', '<p class="help is-danger">:message</p>') !!}
                  </div>
                </div>
            </div>
            <div class="form-group">
                <div class="{{ $errors->has('password') ? 'has-error' : ''}}">
                  {!! Form::label('NEW PASSWORD', 'NEW PASSWORD', ['class' => 'col-md-2 control-label']) !!}
                  <div class="col-md-10">
                      {!! Form::password('password', ['class' => 'form-control col-sm-10', 'required' => 'required']) !!}
                      {!! $errors->first('password', '<p class="help is-danger">:message</p>') !!}
                  </div>
                </div>
            </div>
            <div class="form-group">
                <div class="{{ $errors->has('password_confirmation') ? 'has-error' : ''}}">
                  {!! Form::label('RE ENTER PASSWORD', 'RE ENTER PASSWORD', ['class' => 'col-md-2 control-label']) !!}
                  <div class="col-md-10">
                      {!! Form::password('password_confirmation', ['class' => 'form-control col-sm-10', 'required' => 'required']) !!}
                      {!! $errors->first('password_confirmation', '<p class="help is-danger">:message</p>') !!}
                  </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-8 col-sm-offset-2">
                    <button class="btn btn-primary" type="submit">Done</button>
                </div>
            </div>


          {!! Form::close() !!}
        </div>
      </div>
    </div>
  </div>
</div>
@stop
