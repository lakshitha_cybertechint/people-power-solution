<?php
namespace EventManage\Models;

use Illuminate\Database\Eloquent\Model;
use UserManage\Models\User;
use QuestionManage\Models\Question;

class Event extends Model{


	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name','expire_date', 'description', 'user_id'];

	public function user()
	{
	  return $this->belongsTo(User::class);
	}

	public function questions()
	{
	  return $this->hasMany(Question::class);
	}

}
