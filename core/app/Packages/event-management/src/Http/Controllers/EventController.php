<?php

namespace EventManage\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Permissions\Models\Permission;
use Sentinel;
use EventManage\Models\Event;

class EventController extends Controller {

    /**
     * Show the Branch add screen to the user.
     *
     * @return Response
     */
    public function addView() {
        return view('EventManage::add');
    }

    /**
     * Add new Branch data to database
     *
     * @return Redirect to Brach add
     */
    public function add(Request $request) {

      $this->validate($request,[
        'name' => 'required|max:255',
      ]);

      $requestData = $request->all();
      $requestData['user_id'] = Sentinel::getUser()->id;

      Event::create($requestData);


      return redirect('admin/event/list')->with(['success' => true,
        'success.message' => 'Topic Created successfully!',
        'success.title' => 'Well Done!']);
    }

    /**
     * View Branch List View
     *
     * @return Response
     */
    public function listView() {
        return view('EventManage::list');
    }

    /**
     * Device list
     *
     * @return Response
     */
    public function jsonList(Request $request) {
        if ($request->ajax()) {
            $data = Event::get();
            $jsonList = array();
            $i = 1;
            foreach ($data as $key => $event) {

                $dd = array();
                array_push($dd, $i);
                array_push($dd, $event->name);
                array_push($dd, $event->user->first_name);
                array_push($dd, substr(strip_tags($event->description), 0 , 32) );
                $permissions = Permission::whereIn('name', ['event.edit', 'admin'])->where('status', '=', 1)->lists('name');
                if (Sentinel::hasAnyAccess($permissions)) {
                    array_push($dd, '<center><a href="#" class="blue" onclick="window.location.href=\'' . url('admin/event/edit/' . $event->id) . '\'" data-toggle="tooltip" data-placement="top" title="Edit product"><i class="fa fa-pencil"></i></a></center>');
                } else {
                    array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Edit Disabled"><i class="fa fa-pencil"></i></a>');
                }

                $permissions = Permission::whereIn('name', ['event.delete', 'admin'])->where('status', '=', 1)->lists('name');
                if (Sentinel::hasAnyAccess($permissions)) {
                    array_push($dd, '<center><a href="#" class="product-delete" data-id="' . $event->id . '" data-toggle="tooltip" data-placement="top" title="Delete Blog"><i class="fa fa-trash-o"></i></a></center>');
                } else {
                    array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Delete Disabled"><i class="fa fa-trash-o"></i></a>');
                }

                array_push($jsonList, $dd);
                $i++;
            }
            return response()->json(array('data' => $jsonList));
        } else {
            return response()->json(array('data' => []));
        }
    }


    /**
     * Delete a device
     * @param  Request $request branch id
     * @return Json           	json object with status of success or failure
     */
    public function delete(Request $request) {
      $this->validate($request,['id' => 'required|exists:events,id']);
      $id = $request->input('id');
      $event = Event::with('questions')->find($id);

      if (count($event->questions)) {
        return response()->json(['status' => 'fail'],405);
      }

      $event->delete();
      return response()->json(['status' => 'success']);
    }

    /**
     * Show the devcie edit screen to the devcie.
     *
     * @return Response
     */
    public function editView($id) {
      $event = Event::findOrFail($id);
      return view('EventManage::edit',compact('event'));
    }

    /**
     * Add new device data to database
     *
     * @return Redirect to Branch add
     */
    public function edit(Request $request, $id) {

      $this->validate($request,[
        'name' => 'required|max:255',
      ]);

      $event = Event::findOrFail($id);

      $requestData = $request->all();
      $requestData['user_id'] = Sentinel::getUser()->id;

      $event->update($requestData);


      return redirect('admin/event/edit/' . $id)->with(['success' => true,
        'success.message' => 'Topic updated successfully!',
        'success.title' => 'Good Job!']);
    }


}
