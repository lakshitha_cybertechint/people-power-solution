<?php
/**
 * Blog MANAGEMENT ROUTES
 *
 * @version 1.0.0
 * @author Praveen Chameera <praveenchameera@gmail.com>
 * @copyright 2017 Praveen Chameera
 */

/**
 * USER AUTHENTICATION MIDDLEWARE
 */
Route::group(['middleware' => ['auth']], function()
{
    Route::group(['prefix' => 'admin/event', 'namespace' => 'EventManage\Http\Controllers'], function(){
      /**
       * GET Routes
       */
      Route::get('add', [
        'as' => 'event.add', 'uses' => 'EventController@addView'
      ]);


      Route::get('edit/{id}', [
        'as' => 'event.edit', 'uses' => 'EventController@editView'
      ]);

      Route::get('list', [
        'as' => 'event.list', 'uses' => 'EventController@listView'
      ]);

      Route::get('json/list', [
        'as' => 'event.list', 'uses' => 'EventController@jsonList'
      ]);


      /**
       * POST Routes
       */
      Route::post('add', [
        'as' => 'event.add', 'uses' => 'EventController@add'
      ]);

      Route::post('edit/{id}', [
        'as' => 'event.edit', 'uses' => 'EventController@edit'
      ]);


      Route::post('delete', [
        'as' => 'event.delete', 'uses' => 'EventController@delete'
      ]);


    });
});
