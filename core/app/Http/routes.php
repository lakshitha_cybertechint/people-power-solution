<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/**
 * USER AUTHENTICATION MIDDLEWARE
 */

/*DONT USE THIS ROUTE FOR OTHER USAGE ----ONLY FOR THIS */
Route::group(['middleware' => ['auth']], function()
{
    Route::get('admin', [
      'as' => 'dashboard', 'uses' => 'WelcomeController@index'
  ]);   

});

/*
* Frontend routes --guest
*/
Route::get('version', [
    'as' => 'frontend.index', 'uses' => 'WebController@version'
]);

#### Public/Common routes ####
// index route

Route::get('/', [
    'as' => 'frontend.index', 'uses' => 'Frontend\MainController@index'
]);

// privacy policy route
Route::get('/privacy-policy', [
    'as' => 'frontend.privacy', 'uses' => 'Frontend\MainController@privacy'
]);


// language routes
Route::get('/language', [
    'as' => 'frontend.language', 'uses' => 'Frontend\MainController@language'
]);
Route::get('/language/{lang}', [
    'as' => 'frontend.language.set', 'uses' => 'Frontend\MainController@languageSet'
]);

// social media login routes
Route::get('/login/{provider}', [
    'as' => 'frontend.auth.socal', 'uses' => 'Frontend\AuthController@redirectToProvider']);
Route::get('/login/{provider}/callback', [
    'as' => 'frontend.auth.socal.callback', 'uses' => 'Frontend\AuthController@handleProviderCallback']);

// questions view route
Route::get('/question/{question}/{language}', [
    'as' => 'frontend.question.view', 'uses' => 'Frontend\QuestionController@view'
]);


Route::group(['middleware' => ['guest']], function() {
    /*
    *auth routes --guest
    */
    // select route
    Route::get('/select', [
        'as' => 'frontend.select', 'uses' => 'Frontend\MainController@select'
    ]);
    Route::get('/login', [
        'as' => 'frontend.auth.login', 'uses' => 'Frontend\AuthController@loginGet'
    ]);
    Route::post('/login', [
        'as' => 'frontend.auth.login.post', 'uses' => 'Frontend\AuthController@authenticate'
    ]);
    Route::get('/register', [
        'as' => 'frontend.auth.register', 'uses' => 'Frontend\AuthController@register'
    ]);
    Route::post('/register/new', [
        'as' => 'frontend.user.create', 'uses' => 'Frontend\AuthController@registerPost'
    ]);
    Route::get('/register/confirm', [
        'as' => 'frontend.user.confirm', 'uses' => 'Frontend\AuthController@registerConfirm'
    ]);
    Route::post('/register/confirm/activate', [
        'as' => 'frontend.user.confirm.activate', 'uses' => 'Frontend\AuthController@registerConfirmActivate'
    ]);
    Route::get('/register/confirm/resend', [
        'as' => 'frontend.user.confirm.resend', 'uses' => 'Frontend\AuthController@registerConfirmResend'
    ]);
    Route::post('/register/confirm/resend/post', [
        'as' => 'frontend.user.confirm.resend.post', 'uses' => 'Frontend\AuthController@registerConfirmResendPost'
    ]);
    // forget password route
    Route::get('/forget-password', [
        'as' => 'frontend.forget.password', 'uses' => 'Frontend\AuthController@forgetPassword'
    ]);
    // forget password post route 
    Route::post('/forget-password/post', [
        'as' => 'frontend.forget.password.post', 'uses' => 'Frontend\AuthController@forgetPasswordPost'
    ]);
    // password reset route
    Route::get('/password-reset', [
        'as' => 'frontend.password.reset', 'uses' => 'Frontend\AuthController@passwordReset'
    ]);
    // password reset post route
    Route::post('/password-reset/post', [
        'as' => 'frontend.password.reset.post', 'uses' => 'Frontend\AuthController@passwordResetPost'
    ]);
    // password new route
    // Route::get('/password-new', [
    //     'as' => 'frontend.password.new', 'uses' => 'Frontend\AuthController@passwordNew'
    // ]);
    Route::get('/password-new', [
        'as' => 'frontend.password.new', 'uses' => 'Frontend\PasswordController@passwordNew'
    ]);
    // password new post route
    Route::post('/password-new/post', [
        'as' => 'frontend.password.new.post', 'uses' => 'Frontend\AuthController@passwordNewPost'
    ]);

    // email confirm route
    Route::get('/register/verify/{code}', [
        'as' => 'frontend.email.confirm', 'uses' => 'Frontend\AuthController@emailConfirm'
    ]);
    // email resend route
    Route::get('/register/email/resend', [
        'as' => 'frontend.email.resend', 'uses' => 'Frontend\AuthController@emailResend'
    ]);
    // email resend post route
    Route::post('/register/email/resend/post', [
        'as' => 'frontend.email.resend.post', 'uses' => 'Frontend\AuthController@emailResendPost'
    ]);
    // Password reset link request routes - Email version
    Route::get('password/email', 'Frontend\PasswordController@getEmail')->name('frontend.password.email');
    Route::post('password/email', 'Frontend\PasswordController@postEmail')->name('frontend.password.email.post');
    // Password reset routes - Email version
    Route::get('password/reset/{token}', 'Frontend\PasswordController@getReset')->name('frontend.password.reset');
    Route::post('password/reset/post', 'Frontend\PasswordController@postReset')->name('frontend.password.reset.post');
});
/**
* Frontend protected routes --auth.member
**/

Route::group(['middleware' => ['auth.member:member']], function() {

    ####### Questions routes #######
    // index route
    Route::get('/questions', [
        'as' => 'frontend.questions', 'uses' => 'Frontend\QuestionController@index'
    ]);
    // answer post route
    Route::post('/questions/answer', [
        'as' => 'frontend.question.answer', 'uses' => 'Frontend\QuestionController@answerPost'
    ]);
    // update route
    Route::get('/questions/update', [
        'as' => 'frontend.questions.update', 'uses' => 'Frontend\QuestionController@update'
    ]);
    // answer update post route
    Route::post('/questions/answer/update', [
        'as' => 'frontend.question.answer.update', 'uses' => 'Frontend\QuestionController@answerUpdatePost'
    ]);

    // answer update post route
    Route::post('/questions/chart/image', [
        'as' => 'frontend.question.chart.image', 'uses' => 'Frontend\QuestionController@saveImage'
    ]);
    


    ###### other routes #######
    // settings route
    Route::get('/settings', [
        'as' => 'frontend.settings', 'uses' => 'Frontend\MainController@settings'
    ]);
    // settings update route
    Route::post('/member/update', [
        'as' => 'frontend.member.update', 'uses' => 'Frontend\AuthController@updateMember'
    ]);
    // password change route
    Route::post('/member/change/password', [
        'as' => 'frontend.member.change.password', 'uses' => 'Frontend\AuthController@changePassword'
    ]);
    Route::get('/logout', [
        'as' => 'frontend.auth.logout', 'uses' => 'Frontend\AuthController@logout'
    ]);
    // profile route
    Route::get('/profile', [
        'as' => 'frontend.profile', 'uses' => 'Frontend\MainController@profile'
    ]);
    // user change language route
    Route::get('/language/change/{lang}', [
        'as' => 'frontend.language.change', 'uses' => 'Frontend\MainController@changeLanguage'
    ]);
});



/**
 * start retrieved from chamara abesekara's 14.04.2018 commit
 */

Route::get('user/login', [
  'as' => 'user.login', 'uses' => 'AuthController@loginView'
]);
Route::post('user/login', [
  'as' => 'user.login', 'uses' => 'AuthController@login'
]);

Route::get('user/logout', [
  'as' => 'user.logout', 'uses' => 'AuthController@logout'
]);

Route::get('user/register', [
  'as' => 'user.register', 'uses' => 'AuthController@userRegisterView'
]);
Route::post('user/register', [
  'as' => 'user.register', 'uses' => 'AuthController@userRegister'
]);

Route::get('user/profile', [
  'as' => 'user.profile', 'uses' => 'WebController@profileView'
]);

/**
 * end retrieved from chamara abesekara's 14.04.2018 commit
 */