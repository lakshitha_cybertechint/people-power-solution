<?php

namespace App\Http\Requests\Frontend;

use Auth;
use App\Http\Requests\Request;

class ChangePasswordRequest extends Request
{
    public function __construct()
    {
        $this->member = Auth::user();     
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if(($this->member->social == null && $this->member->password !== null) || ($this->member->social !== null && $this->member->password !== null)){
            return [
                'old_password' => 'required',
                'password'     => 'required|min:6|confirmed',
            ];
            
        }
        else{
            return [
                'password'     => 'required|min:6|confirmed',
            ];
        }
        
    }
}
