<?php

namespace App\Http\Requests\Frontend;

use App\Http\Requests\Request;

class RegisterRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $rules = [
            'name'      => 'required|max:255|',
            'user_name' => 'required|min:5|max:12|alpha_num|unique:members,user_name',
            'email'     => 'required|unique:members,email',
            'mobile'    => 'required|min:10|unique:members,mobile',
            'password'  => 'required|confirmed|min:6'
        ];
    }

    public function messages()
    {
        return [
            // 'mobile.required' => 'The mobile number must be an integer.',
            // 'mobile.min'        => 'The mobile number must be 10 numbers.',
            // 'mobile.max'        => 'The mobile number may not be greater than 10',
            // 'user_name.unique'  => 'Unfortunately the user name has already been taken.',
        ];
    }
}