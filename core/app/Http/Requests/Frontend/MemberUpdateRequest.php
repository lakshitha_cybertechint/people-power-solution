<?php

namespace App\Http\Requests\Frontend;

use Auth;
use App\Http\Requests\Request;

class MemberUpdateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $member = Auth::user();

        return $rules = [
            'name' => 'required|max:255|',
            'user_name' => 'required|min:5|max:10|unique:members,user_name,'. $member->id,
            'mobile' => 'required|min:10'
        ];
    }

    public function messages()
    {
        return [
            'mobile.required' => 'The mobile number must be an integer.',
            'mobile.min' => 'The mobile number must be at least 10.',
            'mobile.max' => 'The mobile number may not be greater than 10',
            'user_name.unique' => 'Unfortunately the user name has already been taken.',
        ];
    }
}
