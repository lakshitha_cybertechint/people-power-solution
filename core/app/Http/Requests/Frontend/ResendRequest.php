<?php

namespace App\Http\Requests\Frontend;

use App\Http\Requests\Request;

class ResendRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $rules = [
            'mobile' => 'required|min:10'
        ];
    }

    public function messages()
    {
        return [
            // 'mobile.integer' => 'The mobile number must be an integer.',
            'mobile.min' => 'The mobile number must be 10 numbers.',
            'mobile.max' => 'The mobile number may not be greater than 10',
        ];
    }
}
