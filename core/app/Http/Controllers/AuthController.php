<?php namespace App\Http\Controllers;

use Input;
use Sentinel;
use Session;
use Socialite;
use DB;

use UserManage\Models\User;
use UserRoles\Models\UserRole;
class AuthController extends Controller {

	/*
		|--------------------------------------------------------------------------
		| Welcome Controller
		|--------------------------------------------------------------------------
		|
		| This controller renders the "marketing page" for the application and
		| is configured to only allow guests. Like most of the other sample
		| controllers, you are free to modify or remove it as you desire.
		|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
		//$this->middleware('auth');
	}
	
    
	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function loginView() {
		try {
			if (!Sentinel::check()) {				
				return view('layouts.back.login');
			} else {
				$redirect = Session::get('loginRedirect', '');
				Session::forget('loginRedirect');
				return redirect($redirect);
			}
		} catch (\Exception $e) {
			return view('layouts.back.login')->withErrors(['login' => $e->getMessage()]);
		}
	}

	
	public function login() {

		
		$credentials = array(
			'username' => Input::get('username'),
			'password' => Input::get('password'),
		);

		if (Input::get('remember')) {
			$remember = true;
		} else {
			$remember = false;
		}


		try {

			$user = Sentinel::authenticate($credentials, $remember);
			if ($user) {				
	           if($user->hasAnyAccess(['ad.user'])){
					$redirect = Session::get('loginRedirect', '/');
				}else{
					$redirect = Session::get('loginRedirect', 'admin');
				}
				Session::forget('loginRedirect');

				return redirect($redirect);

			} else {
				return redirect('user/login')->with([ 'error' => true,
				'error.message'=> 'Incorrect Username OR Password !',
				'error.title' => 'Try Again!']);
			}
		} catch (\Exception $e) {
			return $msg = $e->getMessage();
		}
		
		
	}
	/*
		*	@method logout()
		*	@description Logging out the logged in user
		*	@return URL redirection
	*/
	public function logout() {
		Sentinel::logout();		
		return redirect()->to('/');

	}


     /**
     * Redirect the user to the FACEBOOK authentication page.
     *
     * @return Response
     */
    public function redirectToFacebook()
    {
        return Socialite::driver('facebook')->redirect();
    }
 	/**
     * Redirect the user to the Google authentication page.
     *
     * @return Response
     */
    public function redirectToGoogle()
    {
        return Socialite::driver('google')->redirect();
    }

    /**
     * Obtain the user information from FACEBOOK.
     *
     * @return Response
     */
    public function handleFacebookCallback()
    {
        $user = Socialite::driver('facebook')->user();
        $user_details=$user->user;        
        if(User::where('fb_id',$user_details['id'])->exists()){
        	$fbloged_user=User::where('fb_id',$user_details['id'])->get();
        	$fbloged_user=$fbloged_user[0];        	
        	$user_login = Sentinel::findById($fbloged_user->id);
			Sentinel::login($user_login);
        	return redirect('/');
        }else{
        	try {
				$registed_user=DB::transaction(function ()  use ($user_details){

					$user = Sentinel::registerAndActivate([								
						'email' =>$user_details['id'].'@fbmail.com',
						'username' => $user_details['name'],
						'password' => $user_details['id'],
						'fb_id' => $user_details['id']
					]);

					if (!$user) {
						throw new TransactionException('', 100);
					}

					$user->makeRoot();

					$role = Sentinel::findRoleById(1);
					$role->users()->attach($user);

					User::rebuild();
					return $user;
					

				});
				Sentinel::login($registed_user);
        		return redirect('/');

			} catch (TransactionException $e) {
				if ($e->getCode() == 100) {
					Log::info("Could not register user");

					return redirect('user/register')->with(['error' => true,
						'error.message' => "Could not register user",
						'error.title' => 'Ops!']);
				}
			} catch (Exception $e) {

			}
        }
        

        
       
       

        // $user->token;
    }/**
     * Obtain the user information from Google.
     *
     * @return Response
     */
    public function handleGoogleCallback()
    {	
	     $user = Socialite::driver('google')->user();
	     $user_details=$user->user;
	     // return $user_details['emails'][0]['value'];

       if(User::where('g_id',$user_details['id'])->exists()){

	    	$fbloged_user=User::where('g_id',$user_details['id'])->get();
	    	$fbloged_user=$fbloged_user[0];        	
	    	$user_login = Sentinel::findById($fbloged_user->id);
			Sentinel::login($user_login);
	    	return redirect('/');
        }else{

        	try {
				$registed_user=DB::transaction(function ()  use ($user_details){

					$user = Sentinel::registerAndActivate([								
						'email' =>$user_details['emails'][0]['value'],
						'username' => $user_details['emails'][0]['value'],
						'password' => $user_details['id'],
						'g_id' => $user_details['id']
					]);

					if (!$user) {
						throw new TransactionException('', 100);
					}

					$user->makeRoot();

					$role = Sentinel::findRoleById(1);
					$role->users()->attach($user);

					User::rebuild();
					return $user;
					

				});
				Sentinel::login($registed_user);
        		return redirect('/');

			} catch (TransactionException $e) {
				if ($e->getCode() == 100) {
					Log::info("Could not register user");

					return redirect('user/register')->with(['error' => true,
						'error.message' => "Could not register user",
						'error.title' => 'Ops!']);
				}
			} catch (Exception $e) {

			}
        }
       
    }
}
