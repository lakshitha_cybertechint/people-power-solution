<?php

namespace App\Http\Controllers;


use App\Http\Requests\ReCaptchataTestFormRequest;
use Illuminate\Http\Request;

/*USAGE LIBRARY*/
use File;
use Response;
use Input;
use DB;
use Session;
use Mail;
use Sentinel;
use PDF;
use View;
use Hash;
use Carbon\Carbon;
use Permissions\Models\Permission;
use App\Models\config;


class WebController extends Controller {

    /*
        |--------------------------------------------------------------------------
        | Web Controller
        |--------------------------------------------------------------------------
        |
        | This controller renders the "marketing page" for the application and
        | is configured to only allow guests. Like most of the other sample
        | controllers, you are free to modify or remove it as you desire.
        |
    */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        //$this->middleware('guest');
    }



    public function index() {

        return view('front.index');
    }
    public function version()
    {
         $config=config::get()->first();
         if ($config) {
            return Response::json(array('ID'=>200,'DATA'=>$config->version));
         }else{
            return Response::json(array('ID'=>400,'DATA'=>'Bad Request'));
         }
      
        
    }

    // public function adDetails() {

    //     return view('front.ad-details');
    // }

    // public function addProductMember() {

    //     return view('front.addProductMember');
    // }

    // public function addStockMember() {

    //     return view('front.addStockMember');
    // }

    // public function allAds() {

    //     return view('front.all-ads');
    // }

    // public function businessReg() {

    //     return view('front.businessReg');
    // }

    // public function customizeLayout() {

    //     return view('front.customizeLayout');
    // }

    // public function custompageLayout1() {

    //     return view('front.custompageLayout1');
    // }

    // public function custompageLayout2() {

    //     return view('front.custompageLayout2');
    // }

    // public function custompageLayout3() {

    //     return view('front.custompageLayout3');
    // }

    // public function postAd() {

    //     return view('front.post-ad');
    // }

    // public function profileMember() {

    //     return view('front.profileMember');
    // }

    // public function support() {

    //     return view('front.support');
    // }

    // public function termsOfService() {

    //     return view('front.termsOfService');
    // }

}
