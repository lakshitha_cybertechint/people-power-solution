<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;

use DB;
use App;
use Auth;
use File;
use Carbon\Carbon;
use App\Http\Requests;
use GuzzleHttp\Client;
use App\Models\Frontend\Event;
use App\Models\Frontend\Voting;
use App\Models\Frontend\Question;
use App\Http\Controllers\Controller;
use App\Models\Frontend\QuestionCategory;
use GuzzleHttp\Exception\GuzzleException;


class QuestionController extends Controller
{
    public function __construct()
    {
        $this->member = Auth::user();
        if(Auth::check()){
            if($this->member->language == 'si'){
                session()->put('locale', 'si');
            }
            elseif($this->member->language == 'ta'){
                session()->put('locale', 'ta');
            }
            else{
                session()->put('locale', 'en');
            }
        } 
    }

    /**
     *
     * @return \Illuminate\Http\Response
     */

    
    public function index()
    {
        return view('front.questions.index')->with([
            'questions' => $this->getNewQuestions()['questions'],
            'meta_percent' => $this->getNewQuestions()['meta_percent'],
            'language' => $this->member->language,
            'answered_questions' => $this->getAnsweredQuestions()['questions'],
            'answered_meta_percent' => $this->getAnsweredQuestions()['meta_percent']
        ]);
    }


    public function answerPost(Request $request)
    {
        $voting = new Voting();
        $voting->member_id = $this->member->id;
        $voting->question_id = $request->input('q_id');
        $voting->answer = $request->input('value');
        $voting->save();

        $no_count = Voting::select(DB::raw('COUNT(answer) as answers'))->where('answer', -1)->where('question_id', $request->input('q_id'))->first();
        $yes_count = Voting::select(DB::raw('COUNT(answer) as answers'))->where('answer', 1)->where('question_id', $request->input('q_id'))->first();

        $question = Question::find($request->input('q_id'))->update(['no' => $no_count['answers'], 'yes' => $yes_count['answers']]);

        return response()->json(['yes' => $yes_count['answers'], 'no' => $no_count['answers'], 'question' => $question]); 
    }

    public function update()
    {
        return view('front.questions.update')->with(['questions' => $this->getAnsweredQuestions()['questions'], 'meta_percent' => $this->getAnsweredQuestions()['meta_percent'], 'language' => $this->member->language]);
    }



    public function answerUpdatePost(Request $request)
    {
        $create_graph = false;

        $voting_check = Voting::where('question_id', $request->input('q_id'))->where('member_id', $this->member->id)->where('answer', $request->input('value'))->first();

        if($voting_check == !null){
            $create_graph = true;
        }

        $old_question = Question::findorFail($request->input('q_id'));

        $voting = Voting::where('question_id', $request->input('q_id'))->where('member_id', $this->member->id)->update(['answer' => $request->input('value')]);

        $no_count = Voting::select(DB::raw('COUNT(answer) as answers'))->where('answer', -1)->where('question_id', $request->input('q_id'))->first();
        $yes_count = Voting::select(DB::raw('COUNT(answer) as answers'))->where('answer', 1)->where('question_id', $request->input('q_id'))->first();

        $question = Question::find($request->input('q_id'))->update(['no' => $no_count['answers'], 'yes' => $yes_count['answers']]);

        return response()->json(['yes' => $yes_count['answers'], 'no' => $no_count['answers'], 'question' => $question, 'create_graph' => $create_graph, 'old_question' => $old_question]); 
    }



    public function view(Request $request, Question $question, $language)
    {
        if($language == 'si'){
            $locale = $language . '_LK.utf8';
        }
        elseif($language == 'ta'){
            $locale = $language . '_IN.utf8';
        }
        else{
            $locale = $language . '_US.utf8';
        }

        setlocale(LC_TIME, $locale);
        $dt = Carbon::today();
        setlocale(LC_TIME, $locale);
        $date = $dt->formatLocalized('%B %d, %Y');

        // Set question percent%
        $total_votes = $question['yes'] + $question['no'];
        if($total_votes  !== 0){
            if($question['no'] == 0){
                $percent = 0;
            }
            else{
                $percent = $question['no'] * 100 / $total_votes;
            }                        
        }
        else{
            $percent = null;
        } 

        // Set meat description percent%
        $meta_percent = [];
        $meta_votes = $question['yes'] + $question['no'];
        if($meta_votes  !== 0){
            if($question['no'] == 0 && $question['yes'] == 0){
                $meta_percent['no_percent'] = 0;
                $meta_percent['yes_percent'] = 0;
            }
            elseif($question['no'] !== 0 && $question['yes'] !== 0){
                $meta_percent['yes_percent'] = $question['yes'] * 100 / $total_votes;
                $meta_percent['no_percent'] = $question['no'] * 100 / $total_votes;
            }
            elseif($question['no'] !== 0){
                $meta_percent['no_percent'] = $question['no'] * 100 / $total_votes;
                $meta_percent['yes_percent'] = 0;
            }          
            elseif($question['yes'] !== 0){     
                $meta_percent['yes_percent'] = $question['yes'] * 100 / $total_votes;
                $meta_percent['no_percent'] = 0;
            }         
        }

        return view('front.questions.view')->with(['question' => $question, 'percent' => $percent, 'language' => $language, 'date' => $date, 'url' => $request->fullUrl(), 'meta_percent' => $meta_percent, 'yes' => $question['yes'], 'no' => $question['no']]);
    }

    public function saveImage(Request $request)
    {
        // $question = Question::where('id', $request->input('q_id'))->first();

        // if(File::exists(str_replace('http://', '', $request->input('db_image')))) {
        //     File::delete(str_replace('http://', '', $request->input('db_image')));
        // }        

        $url = $request->input('image');
        $img = public_path() . '/core/storage/uploads/votes/' . Carbon::now()->timestamp . '-' . $request->input('q_id') . '.png';
        $db_img = url('/') . '/core/storage/uploads/votes/' . Carbon::now()->timestamp . '-' . $request->input('q_id') . '.png';
        file_put_contents($img, file_get_contents($url));

        $question = Question::where('id', $request->input('q_id'))->update(['image' => $db_img]);

        return response()->json('success');         
    }

    public function getNewQuestions()
    {
        $filtered_questions['publish_date'] = [];
        $filtered_questions['count'] = [];
        $filtered_questions['data'] = [];
        $voting_ids = [];
        $meta_percent = [];

        $lang = $this->member->language;
        if($lang == 'si'){
            $locale = $lang . '_LK.utf8';
        }
        elseif($lang == 'ta'){
            $locale = $lang . '_IN.utf8';
        }
        else{
            $locale = $lang . '_US.utf8';
        }

        $new_date = Carbon::today();
        $new_d = $new_date->toDateString();

        $voting_ids = [];

        $votings = Voting::distinct()->where('member_id', $this->member->id)->get();

        if($votings !== null || !$votings->isEmpty){
            foreach ($votings as $key => $voting) {
                $voting_ids[] = $voting->question_id;
            }            
        }
        
        $event_questions = [];
        $questions = [];
        if($lang == 'si'){
            $event_questions = Question::select(['id', 'si_description', 'yes', 'no', 'publish_date'])->whereNotIn('id', $voting_ids)->where('publish_date', '<=', $new_d)->where('expire_date', '>=', Carbon::today()->toDateString())->where('status', 1)->orderBy('publish_date', 'DESC')->get()->toArray();
        }
        elseif($lang == 'ta'){
            $event_questions = Question::select(['id', 'ta_description', 'yes', 'no', 'publish_date'])->whereNotIn('id', $voting_ids)->where('publish_date', '<=', $new_d)->where('expire_date', '>=', Carbon::today()->toDateString())->where('status', 1)->orderBy('publish_date', 'DESC')->get()->toArray();
        }
        else{
            $event_questions = Question::select(['id', 'en_description', 'yes', 'no', 'publish_date'])->whereNotIn('id', $voting_ids)->where('publish_date', '<=', $new_d)->where('expire_date', '>=', Carbon::today()->toDateString())->where('status', 1)->orderBy('publish_date', 'DESC')->get()->toArray();
        }

        $questions = $event_questions;
        if($lang == 'si'){
            if($questions !== []){
                foreach ($questions as $key => $question) {
                    // set date format and locale
                    setlocale(LC_TIME, $locale);
                    $dt = Carbon::parse($question['publish_date']);
                    setlocale(LC_TIME, $locale);
                    $publish_date = $dt->formatLocalized('%B %d, %Y');

                    $filtered_questions['count'][] = $key;
                    $filtered_questions['publish_date'][] = $publish_date;
                    $filtered_questions['data'][$key]['id'] = $question['id'];
                    $filtered_questions['data'][$key]['question'] = $question['si_description'];
                    $filtered_questions['data'][$key]['url'] = route('frontend.question.view', ['question' => $question['id'], 'language' => 'si']);

                    // Set total %
                    $total_votes = $question['yes'] + $question['no'];
                    if($total_votes  !== 0){

                        if($question['no'] == 0 && $question['yes'] == 0){
                            $filtered_questions['data'][$key]['no_percent'] = 0;
                            $filtered_questions['data'][$key]['yes_percent'] = 0;
                        }
                        elseif($question['no'] !== 0 && $question['yes'] !== 0){
                            $filtered_questions['data'][$key]['yes_percent'] = $question['yes'] * 100 / $total_votes;
                            $filtered_questions['data'][$key]['no_percent'] = $question['no'] * 100 / $total_votes;
                        }
                        elseif($question['no'] !== 0){
                            $filtered_questions['data'][$key]['no_percent'] = $question['no'] * 100 / $total_votes;
                            $filtered_questions['data'][$key]['yes_percent'] = 0;
                        }          
                        elseif($question['yes'] !== 0){     
                            $filtered_questions['data'][$key]['yes_percent'] = $question['yes'] * 100 / $total_votes;
                            $filtered_questions['data'][$key]['no_percent'] = 0;
                        }                         
                    }
                    else{
                        $filtered_questions['data'][$key]['yes_percent'] = 0;
                        $filtered_questions['data'][$key]['no_percent'] = 0;
                    }  

                    // Set meat description percent%
                    $meta_percent = [];
                    $meta_votes = $question['yes'] + $question['no'];
                    if($meta_votes  !== 0){
                        if($question['no'] == 0 && $question['yes'] == 0){
                            $meta_percent['no_percent'] = 0;
                            $meta_percent['yes_percent'] = 0;
                        }
                        elseif($question['no'] !== 0 && $question['yes'] !== 0){
                            $meta_percent['yes_percent'] = $question['yes'] * 100 / $total_votes;
                            $meta_percent['no_percent'] = $question['no'] * 100 / $total_votes;
                        }
                        elseif($question['no'] !== 0){
                            $meta_percent['no_percent'] = $question['no'] * 100 / $total_votes;
                            $meta_percent['yes_percent'] = 0;
                        }          
                        elseif($question['yes'] !== 0){     
                            $meta_percent['yes_percent'] = $question['yes'] * 100 / $total_votes;
                            $meta_percent['no_percent'] = 0;
                        }         
                    }
                }
            }            
        }
        elseif($lang == 'ta'){
            if($questions !== []){
                foreach ($questions as $key => $question) {
                    // set date format and locale
                    setlocale(LC_TIME, $locale);
                    $dt = Carbon::parse($question['publish_date']);
                    setlocale(LC_TIME, $locale);
                    $publish_date = $dt->formatLocalized('%B %d, %Y');

                    $filtered_questions['count'][] = $key;
                    $filtered_questions['publish_date'][] = $publish_date;
                    $filtered_questions['data'][$key]['id'] = $question['id'];
                    $filtered_questions['data'][$key]['question'] = $question['ta_description'];
                    $filtered_questions['data'][$key]['url'] = route('frontend.question.view', ['question' => $question['id'], 'language' => 'ta']);

                    // Set total %
                    $total_votes = $question['yes'] + $question['no'];
                    if($total_votes  !== 0){

                        if($question['no'] == 0 && $question['yes'] == 0){
                            $filtered_questions['data'][$key]['no_percent'] = 0;
                            $filtered_questions['data'][$key]['yes_percent'] = 0;
                        }
                        elseif($question['no'] !== 0 && $question['yes'] !== 0){
                            $filtered_questions['data'][$key]['yes_percent'] = $question['yes'] * 100 / $total_votes;
                            $filtered_questions['data'][$key]['no_percent'] = $question['no'] * 100 / $total_votes;
                        }
                        elseif($question['no'] !== 0){
                            $filtered_questions['data'][$key]['no_percent'] = $question['no'] * 100 / $total_votes;
                            $filtered_questions['data'][$key]['yes_percent'] = 0;
                        }          
                        elseif($question['yes'] !== 0){     
                            $filtered_questions['data'][$key]['yes_percent'] = $question['yes'] * 100 / $total_votes;
                            $filtered_questions['data'][$key]['no_percent'] = 0;
                        }                         
                    }
                    else{
                        $filtered_questions['data'][$key]['yes_percent'] = 0;
                        $filtered_questions['data'][$key]['no_percent'] = 0;
                    }  

                    // Set meat description percent%
                    $meta_percent = [];
                    $meta_votes = $question['yes'] + $question['no'];
                    if($meta_votes  !== 0){
                        if($question['no'] == 0 && $question['yes'] == 0){
                            $meta_percent['no_percent'] = 0;
                            $meta_percent['yes_percent'] = 0;
                        }
                        elseif($question['no'] !== 0 && $question['yes'] !== 0){
                            $meta_percent['yes_percent'] = $question['yes'] * 100 / $total_votes;
                            $meta_percent['no_percent'] = $question['no'] * 100 / $total_votes;
                        }
                        elseif($question['no'] !== 0){
                            $meta_percent['no_percent'] = $question['no'] * 100 / $total_votes;
                            $meta_percent['yes_percent'] = 0;
                        }          
                        elseif($question['yes'] !== 0){     
                            $meta_percent['yes_percent'] = $question['yes'] * 100 / $total_votes;
                            $meta_percent['no_percent'] = 0;
                        }         
                    }
                }
            }
        }
        else{
            if($questions !== []){
                foreach ($questions as $key => $question) {
                    // set date format and locale
                    setlocale(LC_TIME, $locale);
                    $dt = Carbon::parse($question['publish_date']);
                    setlocale(LC_TIME, $locale);
                    $publish_date = $dt->formatLocalized('%B %d, %Y');

                    $filtered_questions['count'][] = $key;
                    $filtered_questions['publish_date'][] = $publish_date;
                    $filtered_questions['data'][$key]['id'] = $question['id'];
                    $filtered_questions['data'][$key]['question'] = $question['en_description'];
                    $filtered_questions['data'][$key]['url'] = route('frontend.question.view', ['question' => $question['id'], 'language' => 'en']);

                    // Set total %
                    $total_votes = $question['yes'] + $question['no'];
                    if($total_votes  !== 0){

                        if($question['no'] == 0 && $question['yes'] == 0){
                            $filtered_questions['data'][$key]['no_percent'] = 0;
                            $filtered_questions['data'][$key]['yes_percent'] = 0;
                        }
                        elseif($question['no'] !== 0 && $question['yes'] !== 0){
                            $filtered_questions['data'][$key]['yes_percent'] = $question['yes'] * 100 / $total_votes;
                            $filtered_questions['data'][$key]['no_percent'] = $question['no'] * 100 / $total_votes;
                        }
                        elseif($question['no'] !== 0){
                            $filtered_questions['data'][$key]['no_percent'] = $question['no'] * 100 / $total_votes;
                            $filtered_questions['data'][$key]['yes_percent'] = 0;
                        }          
                        elseif($question['yes'] !== 0){     
                            $filtered_questions['data'][$key]['yes_percent'] = $question['yes'] * 100 / $total_votes;
                            $filtered_questions['data'][$key]['no_percent'] = 0;
                        }                         
                    }
                    else{
                        $filtered_questions['data'][$key]['yes_percent'] = 0;
                        $filtered_questions['data'][$key]['no_percent'] = 0;
                    }


                    // Set meat description percent%
                    $meta_percent = [];
                    $meta_votes = $question['yes'] + $question['no'];
                    if($meta_votes  !== 0){
                        if($question['no'] == 0 && $question['yes'] == 0){
                            $meta_percent['no_percent'] = 0;
                            $meta_percent['yes_percent'] = 0;
                        }
                        elseif($question['no'] !== 0 && $question['yes'] !== 0){
                            $meta_percent['yes_percent'] = $question['yes'] * 100 / $total_votes;
                            $meta_percent['no_percent'] = $question['no'] * 100 / $total_votes;
                        }
                        elseif($question['no'] !== 0){
                            $meta_percent['no_percent'] = $question['no'] * 100 / $total_votes;
                            $meta_percent['yes_percent'] = 0;
                        }          
                        elseif($question['yes'] !== 0){     
                            $meta_percent['yes_percent'] = $question['yes'] * 100 / $total_votes;
                            $meta_percent['no_percent'] = 0;
                        }         
                    }  
                }
            }
        }


        // dd($filtered_questions);
        $unique_questions['publish_date'] = array_unique($filtered_questions['publish_date']);
        $unique_questions['count'] = $filtered_questions['count'];
        $unique_questions['data'] = array_values($filtered_questions['data']);

        return $all = array(
            'questions' => $unique_questions,
            'meta_percent' => $meta_percent,
        );
    }

    public function getAnsweredQuestions()
    {
        $filtered_questions['publish_date'] = [];
        $filtered_questions['count'] = [];
        $filtered_questions['data'] = [];
        $voting_ids = [];
        $meta_percent = [];

        $lang = $this->member->language;
        if($lang == 'si'){
            $locale = $lang . '_LK.utf8';
        }
        elseif($lang == 'ta'){
            $locale = $lang . '_IN.utf8';
        }
        else{
            $locale = $lang . '_US.utf8';
        }     

        $voting_ids = [];
        $votings = Voting::where('member_id', $this->member->id)->get();

        if($votings !== null || $votings->isEmpty()){
            foreach($votings as $voting){
                $voting_ids[] = $voting->question_id;
            }
        }

        $event_questions = [];
        $questions = [];
        // $events = Event::where('expire_date', '>=', Carbon::today()->toDateString())->get();
        // if(!$events->isEmpty()){
        //     foreach($events as $key => $event){
        //         if($event->questions !== null){
        //             foreach ($event->questions as $key => $question) {
        if($lang == 'si'){
            $event_questions = Question::select(['id', 'si_description', 'yes', 'no', 'publish_date'])->whereIn('id', $voting_ids)->where('status', 1)->orderBy('publish_date', 'DESC')->get()->toArray();
        }
        elseif($lang == 'ta'){
            $event_questions = Question::select(['id', 'ta_description', 'yes', 'no', 'publish_date'])->whereIn('id', $voting_ids)->where('status', 1)->orderBy('publish_date', 'DESC')->get()->toArray();
        }
        else{
            $event_questions = Question::select(['id', 'en_description', 'yes', 'no', 'publish_date'])->whereIn('id', $voting_ids)->where('status', 1)->orderBy('publish_date', 'DESC')->get()->toArray();
        }
        
        $questions = $event_questions;

        if($lang == 'si'){

            if($questions !== []){
                foreach ($questions as $key => $question) {
                    // set date format and locale
                    setlocale(LC_TIME, $locale);
                    $dt = Carbon::parse($question['publish_date']);
                    setlocale(LC_TIME, $locale);
                    $publish_date = $dt->formatLocalized('%B %d, %Y');

                    $filtered_questions['count'][] = $key;
                    $filtered_questions['publish_date'][] = $publish_date;
                    $filtered_questions['data'][$key]['id'] = $question['id'];
                    $filtered_questions['data'][$key]['question'] = $question['si_description'];
                    $filtered_questions['data'][$key]['url'] = route('frontend.question.view', ['question' => $question['id'], 'language' => 'si']);

                    // Set total %
                    $total_votes = $question['yes'] + $question['no'];
                    if($total_votes  !== 0){
                        if($question['no'] == 0){
                            $filtered_questions['data'][$key]['percent'] = 0;
                        }
                        else{
                            $filtered_questions['data'][$key]['percent'] = $question['no'] * 100 / $total_votes;
                        }                        
                    }
                    else{
                        $filtered_questions['data'][$key]['percent'] = null;
                    }

                    // Set meat description percent%
                    $meta_percent = [];
                    $meta_votes = $question['yes'] + $question['no'];
                    if($meta_votes  !== 0){
                        if($question['no'] == 0 && $question['yes'] == 0){
                            $meta_percent['no_percent'] = 0;
                            $meta_percent['yes_percent'] = 0;
                        }
                        elseif($question['no'] !== 0 && $question['yes'] !== 0){
                            $meta_percent['yes_percent'] = $question['yes'] * 100 / $total_votes;
                            $meta_percent['no_percent'] = $question['no'] * 100 / $total_votes;
                        }
                        elseif($question['no'] !== 0){
                            $meta_percent['no_percent'] = $question['no'] * 100 / $total_votes;
                            $meta_percent['yes_percent'] = 0;
                        }          
                        elseif($question['yes'] !== 0){     
                            $meta_percent['yes_percent'] = $question['yes'] * 100 / $total_votes;
                            $meta_percent['no_percent'] = 0;
                        }         
                    }
                }
            }
        }
        elseif($lang == 'ta'){
            if($questions !== []){
                foreach ($questions as $key => $question) {
                    // set date format and locale
                    setlocale(LC_TIME, $locale);
                    $dt = Carbon::parse($question['publish_date']);
                    setlocale(LC_TIME, $locale);
                    $publish_date = $dt->formatLocalized('%B %d, %Y');

                    $filtered_questions['count'][] = $key;
                    $filtered_questions['publish_date'][] = $publish_date;
                    $filtered_questions['data'][$key]['id'] = $question['id'];
                    $filtered_questions['data'][$key]['question'] = $question['ta_description'];
                    $filtered_questions['data'][$key]['url'] = route('frontend.question.view', ['question' => $question['id'], 'language' => 'ta']);

                    // Set total %
                    $total_votes = $question['yes'] + $question['no'];
                    if($total_votes  !== 0){
                        if($question['no'] == 0){
                            $filtered_questions['data'][$key]['percent'] = 0;
                        }
                        else{
                            $filtered_questions['data'][$key]['percent'] = $question['no'] * 100 / $total_votes;
                        }                        
                    }
                    else{
                        $filtered_questions['data'][$key]['percent'] = null;
                    } 

                    // Set meat description percent%
                    $meta_percent = [];
                    $meta_votes = $question['yes'] + $question['no'];
                    if($meta_votes  !== 0){
                        if($question['no'] == 0 && $question['yes'] == 0){
                            $meta_percent['no_percent'] = 0;
                            $meta_percent['yes_percent'] = 0;
                        }
                        elseif($question['no'] !== 0 && $question['yes'] !== 0){
                            $meta_percent['yes_percent'] = $question['yes'] * 100 / $total_votes;
                            $meta_percent['no_percent'] = $question['no'] * 100 / $total_votes;
                        }
                        elseif($question['no'] !== 0){
                            $meta_percent['no_percent'] = $question['no'] * 100 / $total_votes;
                            $meta_percent['yes_percent'] = 0;
                        }          
                        elseif($question['yes'] !== 0){     
                            $meta_percent['yes_percent'] = $question['yes'] * 100 / $total_votes;
                            $meta_percent['no_percent'] = 0;
                        }         
                    }
                }
            }
        }
        else{
            if($questions !== []){
                foreach ($questions as $key => $question) {
                    // set date format and locale
                    setlocale(LC_TIME, $locale);
                    $dt = Carbon::parse($question['publish_date']);
                    setlocale(LC_TIME, $locale);
                    $publish_date = $dt->formatLocalized('%B %d, %Y');

                    $filtered_questions['count'][] = $key;
                    $filtered_questions['publish_date'][] = $publish_date;
                    $filtered_questions['data'][$key]['id'] = $question['id'];
                    $filtered_questions['data'][$key]['question'] = $question['en_description'];
                    $filtered_questions['data'][$key]['url'] = route('frontend.question.view', ['question' => $question['id'], 'language' => 'en']);

                    // Set total %
                    $total_votes = $question['yes'] + $question['no'];
                    if($total_votes  !== 0){
                        if($question['no'] == 0){
                            $filtered_questions['data'][$key]['percent'] = 0;
                        }
                        else{
                            $filtered_questions['data'][$key]['percent'] = $question['no'] * 100 / $total_votes;
                        }                        
                    }
                    else{
                        $filtered_questions['data'][$key]['percent'] = null;
                    }  


                    // Set meat description percent%
                    $meta_percent = [];
                    $meta_votes = $question['yes'] + $question['no'];
                    if($meta_votes  !== 0){
                        if($question['no'] == 0 && $question['yes'] == 0){
                            $meta_percent['no_percent'] = 0;
                            $meta_percent['yes_percent'] = 0;
                        }
                        elseif($question['no'] !== 0 && $question['yes'] !== 0){
                            $meta_percent['yes_percent'] = $question['yes'] * 100 / $total_votes;
                            $meta_percent['no_percent'] = $question['no'] * 100 / $total_votes;
                        }
                        elseif($question['no'] !== 0){
                            $meta_percent['no_percent'] = $question['no'] * 100 / $total_votes;
                            $meta_percent['yes_percent'] = 0;
                        }          
                        elseif($question['yes'] !== 0){     
                            $meta_percent['yes_percent'] = $question['yes'] * 100 / $total_votes;
                            $meta_percent['no_percent'] = 0;
                        }         
                    }
                }
            }
        }

        $unique_questions['publish_date'] = array_unique($filtered_questions['publish_date']);
        $unique_questions['count'] = $filtered_questions['count'];
        $unique_questions['data'] = $filtered_questions['data'];

        return $all = array(
            'questions' => $unique_questions,
            'meta_percent' => $meta_percent
        );
    }
}