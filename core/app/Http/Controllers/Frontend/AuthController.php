<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;

use URL;
use App;
use Auth;
use Mail;
use Socialite;
use App\Http\Requests;
use App\Models\Frontend\Member;
use App\Models\Frontend\SocialLogin;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\Frontend\ResendRequest;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use App\Http\Requests\Frontend\RegisterRequest;
use App\Http\Controllers\Frontend\SmsController;
use App\Http\Requests\Frontend\NewPasswordRequest;
use App\Http\Requests\Frontend\MemberUpdateRequest;
use App\Http\Requests\Frontend\ChangePasswordRequest;
use App\Http\Controllers\Frontend\MobileApiController;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{

    protected $redirectPath = '/';
    protected $loginPath = '/login';
    public $member;

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    ###### User Registraton and Login Controller######
    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->member = Auth::user();

        if(Auth::check()){
            if($this->member->language == 'si'){
                session()->put('locale', 'si');
            }
            elseif($this->member->language == 'ta'){
                session()->put('locale', 'ta');
            }
            else{
                session()->put('locale', 'en');
            }
        }   
    }

    /**
     * login page view
     *
     * @return Response
     */
    public function loginGet()
    {
        return view('front.auth.login');
    }

    /**
     * logout 
     *
     * @return Response
     */
    public function logout()
    {
        Auth::logout();

        return redirect()->route('frontend.index');
    }

    /**
     * register page view
     *
     * @return Response
     */
    public function register()
    {
        $mobile_api = new MobileApiController();
        
        // dd($mobile_api->getNumber());
        return view('front.auth.register');
    }

    /**
     * account confirmation resend sms view
     *
     * @return Response
     */
    public function registerConfirmResend()
    {
        return view('front.auth.resend');
    }
    
    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return Member
     */
    protected function create(array $data, $code, $language)
    {
        return Member::create([
            'name' => $data['name'],
            'user_name' => $data['user_name'],
            'email' => $data['email'],
            'language' => $language,
            'mobile' => $data['mobile'],
            'password' => bcrypt($data['password']),
            'confirmation_code' => $code
        ]);
    }

    /**
     * user registeration
     *
     * @return Response
     */
    public function registerPost(RegisterRequest $request)
    {
        // Email verification
        $code = md5(uniqid(mt_rand(), true));
        $url = route('frontend.email.confirm', $code);
        // dd($url);
       /* return Mail::send('front.emails.confirm', ['url' => $url], function($message) {
            $message->to(Input::get('email'), Input::get('name'))
            ->subject('Verify your email address');
        });*/


        $code = md5(uniqid(mt_rand(), true));
        $url = route('frontend.email.confirm', $code);
        
        $user = Input::get('email');
        Mail::send('front.emails.confirm', ['url' => $url], function($message) use ($user) {
            $message->to($user, '')->subject('Verify Email Address');
            $message->cc('lakshitha.infomail@gmail.com', '')->subject('Verify Email Address');
        });
        
        
        // Sms verification
        // $code = rand(10000, 99999);

        $sms = new SmsController();

        $message = trans('alerts.hi') . " " . $request->input('user_name') . ",  " . trans('alerts.confirm_account') . $code;

        $this->create($request->all(), $code, App::getLocale());

        // $sms->getDetails($request->input('user_name'), $request->input('mobile'), $code, $message);      

        Cache::forever('user_name', $request->input('user_name'));
        
        // return redirect()->route('frontend.user.confirm')->with(['flash_success' => trans('alerts.register_success'), 'user_name' => $request->input('user_name')]);

        $alert = trans('alerts.register_success') . " " . trans('alerts.check_inbox');
        $link = "<a href='". route('frontend.email.resend') ."'>Click Here</a>";
        $alert .= " " . trans('alerts.email_not_receive', ['link' => $link]);
        
        // dd('done');
        return redirect()->back()->with(['flash_success' => $alert]);
    }

    /**
     * user registeration - confirm
     *
     * @return Response
     */
    public function emailConfirm($code)
    {
        $member = Member::where('confirmation_code', $code)->first();

        if($member !== null){
            if($member->confirmed !== 1){
                $member->confirmed = 1;
                $member->save();

                Auth::loginUsingId($member->id);

                return redirect()->route('frontend.questions');
            }
            else{
                return redirect()->route('frontend.auth.login')->with(['flash_success' => trans('alerts.account_already_activated')]);
            }
            
        }
        else{
            return redirect()->route('frontend.auth.register')->with(['flash_success' => 'Invalid confirmation Url!']);
        }        
    }

    public function emailResend()
    {
        return view('front.auth.email-resend');
    }

    public function emailResendPost(Request $request)
    {
        $member = Member::where('email', $request->input('email'))->first();

        if(!$member){
            return redirect()->back()->with(['flash_success' => trans('alerts.email_not_found')]);
        }
        else{
            // Email resend verification
            $code = md5(uniqid(mt_rand(), true));
            $url = route('frontend.email.confirm', $code);
            $member->update(['confirmation_code' => $code]);
            $user = Input::get('email');
            Mail::send('front.emails.confirm', ['url' => $url], function($message) use ($user) {
                $message->to($user, '')->subject('Verify Email Address');
                $message->cc('lakshitha.infomail@gmail.com', '')->subject('Verify Email Address');
            });
            return redirect()->back()->with(['flash_success' => trans('alerts.email_resend_success')]);
        }
    }

    /**
     * user registeration - confirm
     *
     * @return Response
     */
    public function registerConfirm()
    {
        return view('front.auth.confirm');
        
    }

    /**
     * user registeration - account activation
     *
     * @return Response
     */
    public function registerConfirmActivate(Request $request)
    {
        $member = Member::where('user_name', Cache::get('user_name'))->first();

        if($member['confirmation_code'] == $request->input('code')){

            $member = Member::where('user_name', Cache::get('user_name'))->first();

            $member->update(['confirmed' => 1]);

            Auth::loginUsingId($member->id);

            return redirect()->route('frontend.questions');
        }
        else{
            return redirect()->back()->with(['flash_success' => trans('alerts.invalid_code')]);
        }
    }

    /**
     * user registeration - confirm resend sms
     *
     * @return Response
     */
    public function registerConfirmResendPost(ResendRequest $request)
    {
        $member = Member::where('user_name', Cache::get('user_name'))->first();

        if($member !== null){
            if($member['confirmed'] == 0){
                $code = rand(10000, 99999);

                $sms = new SmsController();

                $message = trans('alerts.hi') . " " . $request->input('user_name') . ",  " . trans('alerts.confirm_account') . $code;

                $sms->getDetails($request->input('user_name'), $request->input('mobile'), $code, $message);

                Member::where('user_name', Cache::get('user_name'))->update(['mobile' => $request->input('mobile'), 'confirmation_code' => $code]);

                return redirect()->route('frontend.user.confirm');
            }
            else{
                return redirect()->route('frontend.auth.login')->with(['flash_success' => trans('alerts.account_already_activated')]);
            }
            
        }
        else{
            return redirect()->route('frontend.user.confirm.resend')->with(['flash_success' => trans('alerts.invalid_username')]);
        }
    }

    /**
     * user login
     *
     * @return Response
     */
    public function authenticate(Request $request)
    {
        $member = Member::where('user_name', $request->input('user_name'))->first();

        if($member !== null){

            if (Auth::attempt(['user_name' => $request->input('user_name'), 'password' => $request->input('password'), 'confirmed' => 1])) {
                return redirect()->intended(route('frontend.questions'));
            }
            else{
                if($member->social && $member['password'] !== null){
                    Auth::loginUsingId($member->id);
                    return redirect()->intended(route('frontend.questions'));
                }
                elseif($member->social && $member['password'] == null && $member['confirmed'] == 1){
                    Auth::loginUsingId($member->id);
                    return redirect()->intended(route('frontend.questions'));
                }
                else{
                    if($member['confirmed'] == 1){
                        return view('front.auth.login')->with(['flash_success' => trans('alerts.login_failed')]);
                    }
                    else{
                        // Email resend verification
                        $code = md5(uniqid(mt_rand(), true));
                        $url = route('frontend.email.confirm', $code);
                        $member->update(['confirmation_code' => $code]);
                        $user = $member['email'];
                        Mail::send('front.emails.confirm', ['url' => $url], function($message) use ($user) {
                            $message->to($user, '')->subject('Verify Email Address');
                            // $message->cc('lakshitha.infomail@gmail.com', '')->subject('Verify Email Address');
                        });

                        return redirect()->route('frontend.email.resend')->with(['user_name' => $request->input('user_name'), 'flash_success' => trans('alerts.email_confirm_account')]);
                    }
                }
            }
        }
        else{
            return redirect()->back()->with(['flash_success' => trans('alerts.invalid_username')]);
        }
        
    }

    ###### Social media login ######
    /**
     * Redirect the user to the Social authentication page. 
     * @return Response
     */
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    /**
     * Get or Register user.
     * @return Response
     */
    public function createOrGetUser($social_user, $provider)
    {
        $new_member = null;

        // Check user's exist
        $social_account = SocialLogin::where('provider', $provider)
        ->where('provider_id', $social_user->getId())
        ->first();

        // return existing user
        if ($social_account) {
            // Update the social user's information, token and avatar can be updated.
            $social_account->update([
                'token'       => $social_user->token,
                'avatar'      => $social_user->avatar,
            ]);

            return $social_account->member;
        } 

        // creating a new user
        else {
            // checking nickname exist and set nickname as user name or set name as nickname
            if($social_user->nickname){
                $member = Member::where('user_name', $social_user->getNickname())->first();
                //skip if user is already in the db
                if(!$member){
                    $new_member = Member::create([
                        'name' => $social_user->getName(),
                        'user_name' => $social_user->getNickname(),
                        'language' => App::getLocale()
                    ]);
                }
            }
            else{
                $member = Member::where('user_name', str_replace(' ', '', $social_user->getName()))->first();
                //skip if user is already in the db
                if(!$member){
                    $new_member = Member::create([
                        'name' => $social_user->getName(),
                        'user_name' => str_replace(' ', '', $social_user->getName()),
                        'language' => App::getLocale()
                    ]);
                }

            }
            if($new_member){
                $new_member->save();

                // creating a social user account
                $social_account = new SocialLogin([
                    'member_id'   => $new_member->id,
                    'provider'    => $provider,
                    'provider_id' => $social_user->id,
                    'token'       => $social_user->token,
                    'avatar'      => $social_user->avatar,
                ]);

                $social_account->save();
            }              

            return $new_member;
        }
    }

    /**
     * Obtain the user information.
     *
     * @return Response
     */
    public function handleProviderCallback($provider)
    {
        $user = Socialite::driver($provider)->user();

        $auth_user =  $this->createOrGetUser($user, $provider);
        
        auth()->login($auth_user);

        return redirect()->intended(route('frontend.questions'));
    }

    ###### update member settings ######
    /**
     * @return \Illuminate\Http\Response
     */
    public function updateMember(MemberUpdateRequest $request)
    {
        Member::find($this->member->id)->update(['name' => $request->input('name'), 'user_name' => $request->input('user_name'), 'email' => $request->input('email'), 'mobile' => $request->input('mobile')]);

        return redirect()->route('frontend.settings')->with(['flash_success' => trans('alerts.account_settings_update')]);
    }

    ###### change password ######
    /**
     * @return \Illuminate\Http\Response
     */
    public function changePassword(ChangePasswordRequest $request)
    {
        if(($this->member->social == null && $this->member->password !== null) || ($this->member->social !== null && $this->member->password !== null)){
            if (Hash::check($request->input('old_password'), $this->member->password)) {

                Member::find($this->member->id)->update(['password' => bcrypt($request->input('password'))]);

                return redirect()->route('frontend.settings')->with(['flash_success' => trans('alerts.password_update')]);
            }
            else{
                return redirect()->route('frontend.settings')->with(['flash_success' => trans('alerts.old_password_fail')]);
            }
        }
        else{
            Member::find($this->member->id)->update(['password' => bcrypt($request->input('password'))]);

            return redirect()->route('frontend.settings')->with(['flash_success' => trans('alerts.password_update')]);
        }
    }

    ###### password reset ######
    /**
     * @return \Illuminate\Http\Response
     */
    public function forgetPassword()
    {
        return view('front.auth.forget-password');
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function forgetPasswordPost(Request $request)
    {
        $member = Member::where('user_name', $request->input('user_name'))->first();
        if($member == null){
            return redirect()->back()->with(['flash_success' => trans('alerts.invalid_username')]);
        }
        else{
            $code = rand(10000, 99999);

            $sms = new SmsController();

            $message = trans('alerts.hi') . " " . $request->input('user_name') . ",  " . trans('alerts.password_reset_code') . " " . $code;

            $sms->getDetails($request->input('user_name'), $request->input('mobile'), $code, $message);

            Member::where('user_name', $request->input('user_name'))->update(['mobile' => $request->input('mobile'), 'confirmation_code' => $code]);

            Cache::forever('user_name', $request->input('user_name'));

            return redirect()->route('frontend.password.reset')->with(['user_name' => $request->input('user_name')]);
        }
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function passwordReset()
    {
        return view('front.auth.password-reset')->with(['user_name' => Cache::get('user_name')]);
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function passwordResetPost(Request $request)
    {
        $member = Member::where('user_name', Cache::get('user_name'))->first();

        if($member['confirmation_code'] == $request->input('code')){
            return redirect()->route('frontend.password.new')->with(['flash_success' => trans('alerts.mobile_confirmed')]);
        }
        else{
            return redirect()->back()->with(['flash_success' => trans('alerts.invalid_code'), 'user_name' => Cache::get('user_name')]);
        }
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function passwordNew()
    {
        return view('front.auth.password-new')->with(['user_name' => Cache::get('user_name')]);
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function passwordNewPost(NewPasswordRequest $request)
    {
        Member::where('user_name', Cache::get('user_name'))->update(['password' => bcrypt($request->input('password'))]);

        Cache::forget('user_name');

        return redirect()->route('frontend.auth.login')->with(['flash_success' => trans('alerts.password_reset_success')]);
    }
}