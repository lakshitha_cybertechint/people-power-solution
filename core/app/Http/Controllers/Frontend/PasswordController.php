<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;

use Auth;
use Mail;
use Carbon\Carbon;
use App\Models\Frontend\Member;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use App\Models\Frontend\PasswordReset;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use App\Http\Requests\Frontend\NewPasswordRequest;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class PasswordController extends Controller
{
    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Create a new password controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Display the form to request a password reset link.
     *
     * @return \Illuminate\Http\Response
     */
    public function getEmail()
    {
        return view('front.auth.emails.password');
    }

    /**
     * Send a reset link to the given user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postEmail(Request $request)
    {
        $this->validate($request, ['email' => 'required|email']);

        $member = Member::where('email', $request->input('email'))->first();
        if(!$member){
            return redirect()->back()->with('flash_success', trans('alerts.invalid_email'));
        }
        else{
            $PasswordReset = PasswordReset::create(['email' => $request->input('email'), 'token' => md5(uniqid(mt_rand(), true)), 'created_at' => Carbon::now()]);

            $user = $request->input('email');
            $url = route('frontend.password.reset', $PasswordReset->token);

            Mail::send('front.auth.emails.reset-email', ['url' => $url], function($message) use ($user) {
                $message->to($user, '')->subject(trans('auth.reset'));
            });

            return redirect()->back()->with(['flash_success' => trans('alerts.password_reset_confirm')]);
        }
    }

    public function getReset($token)
    {
        $PasswordReset = PasswordReset::where('token', $token)->first();

        if(!$PasswordReset){
            return redirect()->route('frontend.password.email')->with(['flash_success' => trans('alerts.invalid_password_reset')]);
        }
        else{
            if($PasswordReset['created_at'] <= Carbon::now()->subHour(1)){
                return redirect()->route('frontend.password.email')->with(['flash_success' => trans('alerts.expired_password_reset')]);
            }
            else{
                Cache::forever('token', $token);
                return redirect()->route('frontend.password.new');
            }
        }
    }

    public function passwordNew()
    {
        return view('front.auth.password-new')->with(['token' => Cache::get('token')]);
    }

    public function postReset(NewPasswordRequest $request)
    {
        $PasswordReset = PasswordReset::where('token', $request->input('token'))->first();

        $member = Member::where('email', $PasswordReset['email'])->first();

        if(!$member){
            return redirect()->route('frontend.password.email')->with(['flash_success' => trans('alerts.invalid_password_reset')]);
        }
        else{
            $member->update(['password' => bcrypt($request->input('password'))]);

            Auth::loginUsingId($member['id']);

            PasswordReset::where('token', $request->input('token'))->delete();
            
            return redirect()->route('frontend.questions');
        }
    }
}
