<?php

namespace App\Http\Controllers\Frontend;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;

use App\Http\Controllers\Controller;

class SmsController extends Controller
{
    private $password = '14181869886758';
    private $url = 'https://cpsolutions.dialog.lk/index.php/cbs/sms/send?';

    /**
     * get user mobile to send sms
     *
     * @return true
     */
    public function getDetails($name, $mobile, $code, $message)
    {
        $this->sendSms($mobile, $message);

        return true;
    }

    /**
     * sending sms
     *
     * @return Response
     */
    public function sendSms($mobile, $message)
    {
        $client = new Client();

        $response = $client->post($this->url, [
            'verify'    =>  false,
            'form_params' => [
                'destination' => $mobile,
                'q' => $this->password,
                'message' => $message,
                'from' => ''               
            ]
        ]);

        $response = json_decode($response->getBody(), true);
    }
}