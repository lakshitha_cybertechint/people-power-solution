<?php

namespace App\Http\Controllers\Frontend;

use App;
use Auth;
use Gr8Shivam\SmsApi;
use App\Http\Requests;
use App\Models\Frontend\Member;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MainController extends Controller
{  
    private $member;

    /**
     * @param $member
     */
    public function __construct()
    {
        $this->member = Auth::user();

        if(Auth::check()){
            if($this->member->language == 'si'){
                session()->put('locale', 'si');
            }
            elseif($this->member->language == 'ta'){
                session()->put('locale', 'ta');
            }
            else{
                session()->put('locale', 'en');
            }
        }        
    }

    ####### static routes ########
    /**
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('front.index');
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function select()
    {
        return view('front.select');
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function privacy()
    {
        return view('front.privacy');
    }
    
    
    
    ####### auth routes --guest ####### 

    /**
     * @return \Illuminate\Http\Response
     */
    public function login()
    {
        return view('front.auth.login');
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function register()
    {
        return view('front.auth.register');
    }

    
    ####### auth routes --auth.member ####### 
    /**
     * @return \Illuminate\Http\Response
     */
    public function settings()
    {

        return view('front.users.account-setting')->with(['member' => $this->member]);
    }    

    /**
     * @return \Illuminate\Http\Response
     */
    public function questions()
    {
        return view('front.questions.index');
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function language()
    {
        return view('front.lang.index');
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function languageSet($lang)
    {
        if($lang == 'sinhala'){
            session()->put('locale', 'si');
        }
        elseif($lang == 'tamil'){
            session()->put('locale', 'ta');
        }
        else{
            session()->put('locale', 'en');
        }

        return redirect()->route('frontend.auth.register');
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function profile()
    {
        return view('front.users.profile');
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function changeLanguage($lang)
    {

        if($lang == 'sinhala'){
            session()->put('locale', 'si');
            Member::find($this->member->id)->update(['language' => 'si']);
        }
        elseif($lang == 'tamil'){
            session()->put('locale', 'ta');
            Member::find($this->member->id)->update(['language' => 'ta']);
        }
        else{
            session()->put('locale', 'en');
            Member::find($this->member->id)->update(['language' => 'en']);
        }

        $alert = 'Language change to ' . $lang . '!';


        return redirect()->route('frontend.profile')->with(['flash_success' => $alert]);
    }


}
