<?php namespace App\Http\Controllers;

use App\Models\Menu;
use Sentinel;
use QuestionManage\Models\Question;


class WelcomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//$this->middleware('guest');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$user = Sentinel::getUser();

		if (count($user->roles->where('name', 'AGENT'))) {
			$question = Question::where('user_id', $user->id)->get();
			$questions = Question::where('user_id', $user->id)->orderBy('created_at','desc')->limit(10)->get();
			$expire = Question::where('user_id', $user->id)->whereHas('event', function ($q) {
				$q->whereDate('expire_date','=',date('Y-m-d'));
			})->get();
			$yes = $question->sum('yes');
			$no = $question->sum('no');

			return view('dashboard-agent',compact('questions','question','expire', 'yes' , 'no'));
		}

		$question = Question::get();
		$questions = Question::orderBy('created_at','desc')->limit(10)->get();
		$expire = Question::whereHas('event', function ($q) {
			$q->whereDate('expire_date','=',date('Y-m-d'));
		})->get();

		$yes = $question->sum('yes');
		$no = $question->sum('no');

		return view('dashboard',compact('questions','question','expire', 'yes' , 'no'));

	}




}
