<?php

namespace App\Models\Frontend;

use Illuminate\Database\Eloquent\Model;

class Voting extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'votings';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['member_id', 'question_id', 'answer'];

}
