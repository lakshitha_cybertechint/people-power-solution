<?php

namespace App\Models\Frontend;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $table = 'events';

    public function questions()
    {
    	return $this->hasMany('App\Models\Frontend\Question', 'event_id', 'id');
    }
}
