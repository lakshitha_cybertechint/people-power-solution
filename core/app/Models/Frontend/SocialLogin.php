<?php

namespace App\Models\Frontend;

use Illuminate\Database\Eloquent\Model;

class SocialLogin extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'social_logins';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['member_id', 'provider', 'provider_id', 'token', 'avatar'];

    public function member()
    {
        return $this->belongsTo('App\Models\Frontend\Member');
    }
}
